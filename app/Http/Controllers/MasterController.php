<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Ticket;
use App\Models\City;
use App\Models\Category;
use App\Models\SubCategory;
use App\Models\TicketConversation;
use Auth;
use Notification;
use Session;
use DB;
use Redirect,Response;
class MasterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function login()
    {
    return view('user.login');
    }
    public function adminlogin()
    {
    return view('login');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function loginpost(Request $request)
    {
        $data['driving_licence'] = 'required';
        
        $validation = $this->validateform($request, $data);
        if ($validation) return $validation;
        $customer = User::where('driving_licence',$request->driving_licence)
                        ->where('role','customer')->get();
        if(count($customer) !=0 ){
            $customer = User::where('driving_licence',$request->driving_licence)->first();
            if(Auth::attempt(['email'=>$customer->email, 'password'=>'123456' ])){ 
                return redirect()->route('dashborad')->with('message','success|Login successfully');
            }
        }
        
        else{
            return redirect()->back()->with('message','danger|Invalid número de licencia de conducir no válido');
        }
    }
    public function updatepassword(Request $request){
        
        $data['new_password']='required';
        $data['confirm_password']='required|same:new_password';
        $validation = $this->validateform($request, $data);
        if ($validation) return $validation;
        $params = $request->all();
        unset($params['_token']);
      
        if($params['new_password']==$params['confirm_password']){
            User::whereId(Auth::user()->id)->update(['password'=>bcrypt($params['new_password'])]);
            Auth::logout();
            return redirect(route('login'))->with('message', 'success|Update Password Successfully');
        }
           else{
            return redirect()->back()->with('message','danger|New password and Confirm Password Does Not Match');

           } 
       
    }
    public function adminloginpost(Request $request)
    {
        $data['email'] = 'required';
        $data['password'] = 'required';
        $validation = $this->validateform($request, $data);
        if ($validation) return $validation;
            if(Auth::attempt(['email'=>$request->email, 'password'=>$request->password ])){ 
                return redirect()->route('dashborad')->with('message','success|Login successfully');
            }
        
        else{
            return redirect(route('adminlogin'))->with('message','danger|Invalid credentials');
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function dashborad(Request $request)
    {
        $user =Auth::user();
        if($user->role == "technician"){
            $data['ticket'] = Ticket::where('assign_to',$user->id)->where('assign_to',$user->id)->orderBy('id','desc')->paginate(10);
            $data['open'] = Ticket::where('status','open')->where('assign_to',$user->id)->count();
            $data['waiting'] = Ticket::where('status','waiting')->where('assign_to',$user->id)->count();
            $data['visit'] = Ticket::where('status','visit')->where('assign_to',$user->id)->count();
            $data['revisit'] = Ticket::where('status','revisit')->where('assign_to',$user->id)->count();
            $data['close'] = Ticket::where('status','closed')->where('assign_to',$user->id)->count();
        }
        if($user->role == "admin"){
            $data['ticket'] = Ticket::orderBy('id','desc')->paginate(10);
            $data['open'] = Ticket::where('status','open')->count();
            $data['waiting'] = Ticket::where('status','waiting')->count();
            $data['visit'] = Ticket::where('status','visit')->count();
            $data['revisit'] = Ticket::where('status','revisit')->count();
            $data['close'] = Ticket::where('status','closed')->count();
            $data['notification'] = DB::table('notifications')->count();

        }
        if($user->role == "customer"){
            $data['ticket'] = Ticket::where('user_id',$user->id)->orderBy('id','desc')->paginate(10);
            $data['open'] = Ticket::where('status','open')->where('user_id',$user->id)->count();
            $data['waiting'] = Ticket::where('status','waiting')->where('user_id',$user->id)->count();
            $data['visit'] = Ticket::where('status','visit')->where('user_id',$user->id)->count();
            $data['revisit'] = Ticket::where('status','revisit')->where('user_id',$user->id)->count();
            $data['close'] = Ticket::where('status','closed')->where('user_id',$user->id)->count();
        }
       
       
        return view('user.dashborad',$data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $user = Auth::user()->role;
        Auth::logout();
        Session::flush();
        if($user == 'admin' || $user == 'technician'){
            return redirect(route('adminlogin'))->with('message','success|Logout successfully');

        }
        else{
            return redirect(route('login'))->with('message','success|Logout successfully');

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view()
    {
        return view('user.visitcalendra');
    }
    public function index1(Request $request)
    {
        $user =Auth::user();
        if($user->role == "technician"){
        $events = array();
        $bookings = Ticket::where('assign_to',$user->id)->where('status','like', '%'.'visit'.'%')->get();
        foreach($bookings as $key => $booking) {
            $color = null;
            if($booking->title == 'Test') {
                $color = '#924ACE';
            }
            if($booking->title == 'Test 1') {
                $color = '#68B01A';
            }
            $events[] = [
                'id'   => $booking->id,
                'title' => $booking->title,
                'start' => $booking->visit_date.'T'.$booking->visit_time,
                'end' => $booking->visit_date,
                'visittime' => $booking->visit_time,
                'assign_to' => $booking->assign_to,
                'status'=>$booking->status,
            ];
        }
        return view('user.visit',['events' => $events]);
        }
        if($user->role == "admin"){
            $user1 = User::where('role','technician')->get();
            $events = array();
            $bookings = Ticket::where('status','like', '%'.'visit'.'%')->get();
            foreach($bookings as $key => $booking) {
                $color = null;
                if($booking->title == 'Test') {
                    $color = '#924ACE';
                }
                if($booking->title == 'Test 1') {
                    $color = '#68B01A';
                }
                $events[] = [
                    'id'   => $booking->id,
                    'title' => $booking->title,
                    'start' => $booking->visit_date.'T'.$booking->visit_time,
                    'end' => $booking->visit_date,
                    'visittime' => $booking->visit_time,
                    'assign_to' => $booking->assign_to,
                    'status'=>$booking->status,
                ];
            }
        return view('user.visit',['events' => $events, 'users'=>$user1]);
        }
    }
    public function visitFilter(Request $request)
    {
        $user =Auth::user();
        
        if($user->role == "admin"){
            $events = array();
            $user1 = User::where('role','technician')->get();
            $bookings = Ticket::where('assign_to',$request->id)->where('status','like', '%'.'visit'.'%')->get();
            foreach($bookings as $key => $booking) {
                
                $events[] = [
                    'id'   => $booking->id,
                    'title' => $booking->title,
                    'start' => $booking->visit_date.'T'.$booking->visit_time,
                    'end' => $booking->visit_date.'T'.$booking->visit_time,
                    'visittime' => $booking->visit_time,
                    'assign_to' => $booking->assign_to,
                    'status'=>$booking->status,

                ];
            }
        return view('user.visit',['events' => $events,'users'=>$user1]);
        }
    }
    public function notification()
    {
        $data['notification'] = DB::table('notifications')->get('data');
        return view('notification',$data);
    }
    public function fetchState(Request $request)
    {
        $data['states'] = Subcategory::where("category_id",$request->category_id)->get(["name", "id"]);
        return response()->json($data);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function chart(Request $request)
    {
        $user =Auth::user();
        $cities=City::where('status','active')->get();
        $category =Category::where('status','active')->get();
        $subcategory =SubCategory::where('status','active')->get();
        
        $users = User::where('role','technician')->get();
        if($user->role == "technician"){
            $data['ticket'] = Ticket::where('assign_to',$user->id)->orderBy('id','desc')->get();
            $data['waiting'] = Ticket::where('status','waiting')->where('assign_to',$user->id)->count();
            $data['visit'] = Ticket::where('status','visit')->where('assign_to',$user->id)->count();
            $data['revisit'] = Ticket::where('status','revisit')->where('assign_to',$user->id)->count();
            $data['close'] = Ticket::where('status','closed')->where('assign_to',$user->id)->count();
         
        }
        $data2=[];

        if($user->role == "admin"){
            foreach($users as $user){

            $data['ticket'] = Ticket::orderBy('id','desc')->get();
            $data['openmonday'] = Ticket::where('assign_to',$user->id)->where('update_day','Monday')->where('status','open')->count();
            $data['opentuesday'] = Ticket::where('assign_to',$user->id)->where('update_day','Tuesday')->where('status','open')->count();
            $data['openwedday'] = Ticket::where('assign_to',$user->id)->where('update_day','Wednesday')->where('status','open')->count();
            $data['openthurday'] = Ticket::where('assign_to',$user->id)->where('update_day','Thursday')->where('status','open')->count();
            $data['openfriday'] = Ticket::where('assign_to',$user->id)->where('update_day','Friday')->where('status','open')->count();
            $data['opensatday'] = Ticket::where('assign_to',$user->id)->where('update_day','Saturday')->where('status','open')->count();
            $data['opensunday'] = Ticket::where('assign_to',$user->id)->where('update_day','Sunday')->where('status','open')->count();
            $data['open'] = Ticket::where('status','open')->count();


            $data['waiting'] = Ticket::where('status','waiting')->count();
            $data['waitingmonday'] = Ticket::where('assign_to',$user->id)->where('update_day','Monday')->where('status','waiting')->count();
            $data['waitingtuesday'] = Ticket::where('assign_to',$user->id)->where('update_day','Tuesday')->where('status','waiting')->count();
            $data['waitingwedday'] = Ticket::where('assign_to',$user->id)->where('update_day','Wednesday')->where('status','waiting')->count();
            $data['waitingthurday'] = Ticket::where('assign_to',$user->id)->where('update_day','Thursday')->where('status','waiting')->count();
            $data['waitingfriday'] = Ticket::where('assign_to',$user->id)->where('update_day','Friday')->where('status','waiting')->count();
            $data['waitingsatday'] = Ticket::where('assign_to',$user->id)->where('update_day','Saturday')->where('status','waiting')->count();
            $data['waitingsunday'] = Ticket::where('assign_to',$user->id)->where('update_day','Sunday')->where('status','waiting')->count();
            
            $data['visit'] = Ticket::where('status','visit')->count();
            $data['visitmonday'] = Ticket::where('assign_to',$user->id)->where('visit_day','Monday')->where('status','visit')->count();
            $data['visittuesday'] = Ticket::where('assign_to',$user->id)->where('visit_day','Tuesday')->where('status','visit')->count();
            $data['visitwedday'] = Ticket::where('assign_to',$user->id)->where('visit_day','Wednesday')->where('status','visit')->count();
            $data['visitthurday'] = Ticket::where('assign_to',$user->id)->where('visit_day','Thursday')->where('status','visit')->count();
            $data['visitfriday'] = Ticket::where('assign_to',$user->id)->where('visit_day','Friday')->where('status','visit')->count();
            $data['visitsatday'] = Ticket::where('assign_to',$user->id)->where('visit_day','Saturday')->where('status','visit')->count();
            $data['visitsunday'] = Ticket::where('assign_to',$user->id)->where('visit_day','Sunday')->where('status','visit')->count();

            $data['revisit'] = Ticket::where('status','revisit')->count();
            $data['revisitmonday'] = Ticket::where('assign_to',$user->id)->where('visit_day','Monday')->where('status','revisit')->count();
            $data['revisittuesday'] = Ticket::where('assign_to',$user->id)->where('visit_day','Tuesday')->where('status','revisit')->count();
            $data['revisitwedday'] = Ticket::where('assign_to',$user->id)->where('visit_day','Wednesday')->where('status','revisit')->count();
            $data['revisitthurday'] = Ticket::where('assign_to',$user->id)->where('visit_day','Thursday')->where('status','revisit')->count();
            $data['revisitfriday'] = Ticket::where('assign_to',$user->id)->where('visit_day','Friday')->where('status','revisit')->count();
            $data['revisitsatday'] = Ticket::where('assign_to',$user->id)->where('visit_day','Saturday')->where('status','revisit')->count();
            $data['revisitsunday'] = Ticket::where('assign_to',$user->id)->where('visit_day','Sunday')->where('status','revisit')->count();


            $data['close'] = Ticket::where('status','closed')->count();
            $data['closemonday'] = Ticket::where('assign_to',$user->id)->where('update_day','Monday')->where('status','closed')->count();
            $data['closetuesday'] = Ticket::where('assign_to',$user->id)->where('update_day','Tuesday')->where('status','closed')->count();
            $data['closewedday'] = Ticket::where('assign_to',$user->id)->where('update_day','Wednesday')->where('status','closed')->count();
            $data['closethurday'] = Ticket::where('assign_to',$user->id)->where('update_day','Thursday')->where('status','closed')->count();
            $data['closefriday'] = Ticket::where('assign_to',$user->id)->where('update_day','Friday')->where('status','closed')->count();
            $data['closesatday'] = Ticket::where('assign_to',$user->id)->where('update_day','Saturday')->where('status','closed')->count();
            $data['closesunday'] = Ticket::where('assign_to',$user->id)->where('update_day','Sunday')->where('status','closed')->count();

            $data['reopen'] = Ticket::where('status','reopen')->count();
            $data['reopenmonday'] = Ticket::where('assign_to',$user->id)->where('update_day','Monday')->where('status','reopen')->count();
            $data['reopentuesday'] = Ticket::where('assign_to',$user->id)->where('update_day','Tuesday')->where('status','reopen')->count();
            $data['reopenwedday'] = Ticket::where('assign_to',$user->id)->where('update_day','Wednesday')->where('status','reopen')->count();
            $data['reopenthurday'] = Ticket::where('assign_to',$user->id)->where('update_day','Thursday')->where('status','reopen')->count();
            $data['reopenfriday'] = Ticket::where('assign_to',$user->id)->where('update_day','Friday')->where('status','reopen')->count();
            $data['reopensatday'] = Ticket::where('assign_to',$user->id)->where('update_day','Saturday')->where('status','reopen')->count();
            $data['reopensunday'] = Ticket::where('assign_to',$user->id)->where('update_day','Sunday')->where('status','reopen')->count();

            $data['notification'] = DB::table('notifications')->count();
            array_push($data2,$data);
            }
        }
        if($user->role == "customer"){
            $data['ticket'] = Ticket::where('user_id',$user->id)->orderBy('id','desc')->get();
            $data['open'] = Ticket::where('status','open')->where('user_id',$user->id)->count();
            $data['waiting'] = Ticket::where('status','waiting')->where('user_id',$user->id)->count();
            $data['visit'] = Ticket::where('status','visit')->where('user_id',$user->id)->count();
            $data['revisit'] = Ticket::where('status','revisit')->where('user_id',$user->id)->count();
            $data['close'] = Ticket::where('status','closed')->where('user_id',$user->id)->count();
        }
       
       
        return view('admin.chart',compact('cities','category','subcategory','data2','users'));
    }
    public function chartFilter(Request $request)
    {
        $user =Auth::user();
        $cities =City::where('status','active')->get();
        $category =Category::where('status','active')->get();
        $subcategory =SubCategory::where('status','active')->get();
        $users = User::where('role','technician')->get();
        $data2=[];
         if($user->role == "admin"){
            foreach($users as $user){
                if($request->from_date && $request->to_date && !$request->city && !$request->category_id && !$request->subcategory_id){
                    $data['openmonday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', '>=', $request->from_date)
                                                ->whereDate('update_date', '<=', $request->to_date)
                                                ->where('update_day','Monday')
                                                ->where('status','open')
                                                ->count();
                    $data['opentuesday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', '>=', $request->from_date)
                                                ->whereDate('update_date', '<=', $request->to_date)
                                                ->where('update_day','Tuesday')
                                                ->where('status','open')
                                                ->count();
                    $data['openwedday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', '>=', $request->from_date)
                                                ->whereDate('update_date', '<=', $request->to_date)
                                                ->where('update_day','Wednesday')->where('status','open')->count();
                    $data['openthurday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', '>=', $request->from_date)
                                                ->whereDate('update_date', '<=', $request->to_date)
                                                ->where('update_day','Thursday')->where('status','open')->count();
                    $data['openfriday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', '>=', $request->from_date)
                                                ->whereDate('update_date', '<=', $request->to_date)
                                                ->where('update_day','Friday')->where('status','open')->count();
                    $data['opensatday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', '>=', $request->from_date)
                                                ->whereDate('update_date', '<=', $request->to_date)
                                                ->where('update_day','Saturday')->where('status','open')->count();
                    $data['opensunday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', '>=', $request->from_date)
                                                ->whereDate('update_date', '<=', $request->to_date)
                                                ->where('update_day','Sunday')->where('status','open')->count();
                    $data['open'] = Ticket::where('assign_to',$user->id)->where('status','open')->
                                            whereDate('update_date', '>=', $request->from_date)
                                        ->whereDate('update_date', '<=', $request->to_date)->count();


                    $data['waiting'] = Ticket::where('status','waiting')->
                                whereDate('update_date', '>=', $request->from_date)
                            ->whereDate('update_date', '<=', $request->to_date)->count(); 
                    $data['waitingmonday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', '>=', $request->from_date)
                                                ->whereDate('update_date', '<=', $request->to_date)
                                                ->where('update_day','Monday')->where('status','waiting')->count();
                    $data['waitingtuesday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', '>=', $request->from_date)
                                                ->whereDate('update_date', '<=', $request->to_date)
                                                ->where('update_day','Tuesday')->where('status','waiting')->count();
                    $data['waitingwedday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', '>=', $request->from_date)
                                                ->whereDate('update_date', '<=', $request->to_date)
                                                ->where('update_day','Wednesday')->where('status','waiting')->count();
                    $data['waitingthurday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', '>=', $request->from_date)
                                                ->whereDate('update_date', '<=', $request->to_date)
                                                ->where('update_day','Thursday')->where('status','waiting')->count();
                    $data['waitingfriday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', '>=', $request->from_date)
                                                ->whereDate('update_date', '<=', $request->to_date)
                                                ->where('update_day','Friday')->where('status','waiting')->count();
                    $data['waitingsatday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', '>=', $request->from_date)
                                                ->whereDate('update_date', '<=', $request->to_date)
                                                ->where('update_day','Saturday')->where('status','waiting')->count();
                    $data['waitingsunday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', '>=', $request->from_date)
                                                ->whereDate('update_date', '<=', $request->to_date)
                                                ->where('update_day','Sunday')->where('status','waiting')->count();
                    
                    $data['visit'] = Ticket::where('status','visit')->
                                            whereDate('visit_date', '>=', $request->from_date)->whereDate('visit_date', '<=', $request->to_date)->count();
                    $data['visitmonday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', '>=', $request->from_date)
                                                ->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Monday')->where('status','visit')->count();
                    $data['visittuesday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', '>=', $request->from_date)
                                                ->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Tuesday')->where('status','visit')->count();
                    $data['visitwedday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', '>=', $request->from_date)
                                                ->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Wednesday')->where('status','visit')->count();
                    $data['visitthurday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', '>=', $request->from_date)
                                                ->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Thursday')->where('status','visit')->count();
                    $data['visitfriday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', '>=', $request->from_date)
                                                ->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Friday')->where('status','visit')->count();
                    $data['visitsatday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', '>=', $request->from_date)
                                                ->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Saturday')->where('status','visit')->count();
                    $data['visitsunday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', '>=', $request->from_date)
                                                ->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Sunday')->where('status','visit')->count();
                    $data['revisit'] = Ticket::where('status','revisit')->whereDate('visit_date', '>=', $request->from_date)->whereDate('visit_date', '<=', $request->to_date)->count();
                    $data['revisitmonday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', '>=', $request->from_date)
                                                ->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Monday')->where('status','revisit')->count();
                    $data['revisittuesday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', '>=', $request->from_date)
                                                ->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Tuesday')->where('status','revisit')->count();
                    $data['revisitwedday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', '>=', $request->from_date)
                                                ->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Wednesday')->where('status','revisit')->count();
                    $data['revisitthurday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', '>=', $request->from_date)
                                                ->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Thursday')->where('status','revisit')->count();
                    $data['revisitfriday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', '>=', $request->from_date)
                                                ->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Friday')->where('status','revisit')->count();
                    $data['revisitsatday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', '>=', $request->from_date)
                                                ->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Saturday')->where('status','revisit')->count();
                    $data['revisitsunday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', '>=', $request->from_date)
                                                ->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Sunday')->where('status','revisit')->count();


                    $data['close'] = Ticket::where('status','closed')->whereDate('update_date', '>=', $request->from_date)->whereDate('update_date', '<=', $request->to_date)->count();
                    $data['closemonday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', '>=', $request->from_date)
                                                ->whereDate('update_date', '<=', $request->to_date)
                                                ->where('update_day','Monday')->where('status','closed')->count();
                    $data['closetuesday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', '>=', $request->from_date)
                                                ->whereDate('update_date', '<=', $request->to_date)
                                                ->where('update_day','Tuesday')->where('status','closed')->count();
                    $data['closewedday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', '>=', $request->from_date)
                                                ->whereDate('update_date', '<=', $request->to_date)
                                                ->where('update_day','Wednesday')->where('status','closed')->count();
                    $data['closethurday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', '>=', $request->from_date)
                                                ->whereDate('update_date', '<=', $request->to_date)
                                                ->where('update_day','Thursday')->where('status','closed')->count();
                    $data['closefriday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', '>=', $request->from_date)
                                                ->whereDate('update_date', '<=', $request->to_date)
                                                ->where('update_day','Friday')->where('status','closed')->count();
                    $data['closesatday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', '>=', $request->from_date)
                                                ->whereDate('update_date', '<=', $request->to_date)
                                                ->where('update_day','Saturday')->where('status','closed')->count();
                    $data['closesunday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', '>=', $request->from_date)
                                                ->whereDate('update_date', '<=', $request->to_date)
                                                ->where('update_day','Sunday')->where('status','closed')->count();

                    $data['reopen'] = Ticket::where('status','reopen')->whereDate('update_date', '>=', $request->from_date)->whereDate('update_date', '<=', $request->to_date)->count();
                    $data['reopenmonday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', '>=', $request->from_date)
                                                ->whereDate('update_date', '<=', $request->to_date)
                                                ->where('update_day','Monday')->where('status','reopen')->count();
                    $data['reopentuesday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', '>=', $request->from_date)
                                                ->whereDate('update_date', '<=', $request->to_date)
                                                ->where('update_day','Tuesday')->where('status','reopen')->count();
                    $data['reopenwedday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', '>=', $request->from_date)
                                                ->whereDate('update_date', '<=', $request->to_date)
                                                ->where('update_day','Wednesday')->where('status','reopen')->count();
                    $data['reopenthurday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', '>=', $request->from_date)
                                                ->whereDate('update_date', '<=', $request->to_date)
                                                ->where('update_day','Thursday')->where('status','reopen')->count();
                    $data['reopenfriday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', '>=', $request->from_date)
                                                ->whereDate('update_date', '<=', $request->to_date)
                                                ->where('update_day','Friday')->where('status','reopen')->count();
                    $data['reopensatday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', '>=', $request->from_date)
                                                ->whereDate('update_date', '<=', $request->to_date)
                                                ->where('update_day','Saturday')->where('status','reopen')->count();
                    $data['reopensunday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', '>=', $request->from_date)
                                                ->whereDate('update_date', '<=', $request->to_date)
                                                ->where('update_day','Sunday')->where('status','reopen')->count();

                }
                if($request->from_date && $request->to_date && $request->city && !$request->category_id && !$request->subcategory_id){
                    $data['ticket'] = Ticket::orderBy('id','desc')->get();
                    $data['open'] = Ticket::where('status','open')->whereDate('update_date', '>=', $request->from_date)->whereDate('update_date', '<=', $request->to_date)->where('city',$request->city)->count();
                    $data['waiting'] = Ticket::where('status','waiting')->whereDate('update_date', '>=', $request->from_date)->whereDate('update_date', '<=', $request->to_date)->where('city',$request->city)->count();
                    $data['visit'] = Ticket::where('status','visit')->whereDate('visit_date', '>=', $request->from_date)->whereDate('update_date', '<=', $request->to_date)->where('city',$request->city)->count();
                    $data['revisit'] = Ticket::where('status','revisit')->whereDate('visit_date', '>=', $request->from_date)->whereDate('update_date', '<=', $request->to_date)->where('city',$request->city)->count();
                    $data['close'] = Ticket::where('status','closed')->whereDate('update_date', '>=', $request->from_date)->whereDate('update_date', '<=', $request->to_date)->where('city',$request->city)->count();
                    $data['reopen'] = Ticket::where('status','reopen')->whereDate('update_date', '>=', $request->from_date)->whereDate('update_date', '<=', $request->to_date)->where('city',$request->city)->count();
                    $data['openmonday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', '>=', $request->from_date)
                                                ->whereDate('update_date', '<=', $request->to_date)
                                                ->where('city',  $request->city)
                                                ->where('update_day','Monday')
                                                ->where('status','open')
                                                ->count();
                    $data['opentuesday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', '>=', $request->from_date)
                                                ->whereDate('update_date', '<=', $request->to_date)
                                                ->where('city',  $request->city)
                                                ->where('update_day','Tuesday')
                                                ->where('status','open')
                                                ->count();
                    $data['openwedday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', '>=', $request->from_date)
                                                ->whereDate('update_date', '<=', $request->to_date)
                                                ->where('update_day','Wednesday')
                                                ->where('city',  $request->city)
                                                ->where('status','open')->count();
                    $data['openthurday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', '>=', $request->from_date)
                                                ->whereDate('update_date', '<=', $request->to_date)
                                                ->where('update_day','Thursday')
                                                ->where('city',  $request->city)
                                                ->where('status','open')->count();
                    $data['openfriday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', '>=', $request->from_date)
                                                ->whereDate('update_date', '<=', $request->to_date)
                                                ->where('update_day','Friday')
                                                ->where('city',  $request->city)
                                                ->where('status','open')->count();
                    $data['opensatday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', '>=', $request->from_date)
                                                ->whereDate('update_date', '<=', $request->to_date)
                                                ->where('update_day','Saturday')
                                                ->where('city',  $request->city)
                                                ->where('status','open')->count();
                    $data['opensunday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', '>=', $request->from_date)
                                                ->whereDate('update_date', '<=', $request->to_date)
                                                ->where('update_day','Sunday')
                                                ->where('city',  $request->city)
                                                ->where('status','open')->count();
        
        
                    $data['waitingmonday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', '>=', $request->from_date)
                                                ->whereDate('update_date', '<=', $request->to_date)
                                                ->where('update_day','Monday')
                                                ->where('city',  $request->city)
                                                ->where('status','waiting')->count();
                    $data['waitingtuesday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', '>=', $request->from_date)
                                                ->whereDate('update_date', '<=', $request->to_date)
                                                ->where('update_day','Tuesday')
                                                ->where('city',  $request->city)
                                                ->where('status','waiting')->count();
                    $data['waitingwedday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', '>=', $request->from_date)
                                                ->whereDate('update_date', '<=', $request->to_date)
                                                ->where('update_day','Wednesday')
                                                ->where('city',  $request->city)
                                                ->where('status','waiting')->count();
                    $data['waitingthurday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', '>=', $request->from_date)
                                                ->whereDate('update_date', '<=', $request->to_date)
                                                ->where('update_day','Thursday')
                                                ->where('city',  $request->city)
                                                ->where('status','waiting')->count();
                    $data['waitingfriday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', '>=', $request->from_date)
                                                ->whereDate('update_date', '<=', $request->to_date)
                                                ->where('update_day','Friday')
                                                ->where('city',  $request->city)
                                                ->where('status','waiting')->count();
                    $data['waitingsatday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', '>=', $request->from_date)
                                                ->whereDate('update_date', '<=', $request->to_date)
                                                ->where('update_day','Saturday')
                                                ->where('city',  $request->city)
                                                ->where('status','waiting')->count();
                    $data['waitingsunday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', '>=', $request->from_date)
                                                ->whereDate('update_date', '<=', $request->to_date)
                                                ->where('update_day','Sunday')
                                                ->where('city', '<=', $request->city)
                                                ->where('status','waiting')->count();
                    
                    $data['visitmonday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', '>=', $request->from_date)
                                                ->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Monday')
                                                ->where('city', $request->city)
                                                ->where('status','visit')->count();
                    $data['visittuesday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', '>=', $request->from_date)
                                                ->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Tuesday')
                                                ->where('city',  $request->city)
                                                ->where('status','visit')->count();
                    $data['visitwedday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', '>=', $request->from_date)
                                                ->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Wednesday')
                                                ->where('city', $request->city)
                                                ->where('status','visit')->count();
                    $data['visitthurday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', '>=', $request->from_date)
                                                ->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Thursday')
                                                ->where('city', $request->city)
                                                ->where('status','visit')->count();
                    $data['visitfriday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', '>=', $request->from_date)
                                                ->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Friday')
                                                ->where('city', $request->city)
                                                ->where('status','visit')->count();
                    $data['visitsatday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', '>=', $request->from_date)
                                                ->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Saturday')
                                                ->where('city', $request->city)
                                                ->where('status','visit')->count();
                    $data['visitsunday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', '>=', $request->from_date)
                                                ->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Sunday')
                                                ->where('city', $request->city)
                                                ->where('status','visit')->count();
        
                    $data['revisitmonday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', '>=', $request->from_date)
                                                ->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Monday')
                                                ->where('city', $request->city)
                                                ->where('status','revisit')->count();
                    $data['revisittuesday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', '>=', $request->from_date)
                                                ->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Tuesday')
                                                ->where('city', $request->city)
                                                ->where('status','revisit')->count();
                    $data['revisitwedday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', '>=', $request->from_date)
                                                ->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Wednesday')
                                                ->where('city', $request->city)
                                                ->where('status','revisit')->count();
                    $data['revisitthurday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', '>=', $request->from_date)
                                                ->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Thursday')
                                                ->where('city', $request->city)
                                                ->where('status','revisit')->count();
                    $data['revisitfriday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', '>=', $request->from_date)
                                                ->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Friday')
                                                ->where('city', $request->city)
                                                ->where('status','revisit')->count();
                    $data['revisitsatday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', '>=', $request->from_date)
                                                ->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Saturday')
                                                ->where('city', $request->city)
                                                ->where('status','revisit')->count();
                    $data['revisitsunday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', '>=', $request->from_date)
                                                ->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Sunday')
                                                ->where('city', $request->city)
                                                ->where('status','revisit')->count();
        
        
                    $data['closemonday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', '>=', $request->from_date)
                                                ->whereDate('update_date', '<=', $request->to_date)
                                                ->where('update_day','Monday')
                                                ->where('city', $request->city)
                                                ->where('status','closed')->count();
                    $data['closetuesday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', '>=', $request->from_date)
                                                ->whereDate('update_date', '<=', $request->to_date)
                                                ->where('update_day','Tuesday')
                                                ->where('city', $request->city)
                                                ->where('status','closed')->count();
                    $data['closewedday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', '>=', $request->from_date)
                                                ->whereDate('update_date', '<=', $request->to_date)
                                                ->where('update_day','Wednesday')
                                                ->where('city', $request->city)
                                                ->where('status','closed')->count();
                    $data['closethurday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', '>=', $request->from_date)
                                                ->whereDate('update_date', '<=', $request->to_date)
                                                ->where('update_day','Thursday')
                                                ->where('city', $request->city)
                                                ->where('status','closed')->count();
                    $data['closefriday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', '>=', $request->from_date)
                                                ->whereDate('update_date', '<=', $request->to_date)
                                                ->where('update_day','Friday')
                                                ->where('city', $request->city)
                                                ->where('status','closed')->count();
                    $data['closesatday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', '>=', $request->from_date)
                                                ->whereDate('update_date', '<=', $request->to_date)
                                                ->where('update_day','Saturday')
                                                ->where('city', $request->city)
                                                ->where('status','closed')->count();
                    $data['closesunday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', '>=', $request->from_date)
                                                ->whereDate('update_date', '<=', $request->to_date)
                                                ->where('update_day','Sunday')
                                                ->where('city', $request->city)
                                                ->where('status','closed')->count();
        
                    $data['reopenmonday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', '>=', $request->from_date)
                                                ->whereDate('update_date', '<=', $request->to_date)
                                                ->where('update_day','Monday')
                                                ->where('city', $request->city)
                                                ->where('status','reopen')->count();
                    $data['reopentuesday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', '>=', $request->from_date)
                                                ->whereDate('update_date', '<=', $request->to_date)
                                                ->where('update_day','Tuesday')
                                                ->where('city', $request->city)
                                                ->where('status','reopen')->count();
                    $data['reopenwedday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', '>=', $request->from_date)
                                                ->whereDate('update_date', '<=', $request->to_date)
                                                ->where('update_day','Wednesday')
                                                ->where('city', $request->city)
                                                ->where('status','reopen')->count();
                    $data['reopenthurday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', '>=', $request->from_date)
                                                ->whereDate('update_date', '<=', $request->to_date)
                                                ->where('update_day','Thursday')
                                                ->where('city', $request->city)
                                                ->where('status','reopen')->count();
                    $data['reopenfriday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', '>=', $request->from_date)
                                                ->whereDate('update_date', '<=', $request->to_date)
                                                ->where('update_day','Friday')
                                                ->where('city', $request->city)
                                                ->where('status','reopen')->count();
                    $data['reopensatday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', '>=', $request->from_date)
                                                ->whereDate('update_date', '<=', $request->to_date)
                                                ->where('update_day','Saturday')
                                                ->where('city', $request->city)
                                                ->where('status','reopen')->count();
                    $data['reopensunday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', '>=', $request->from_date)
                                                ->whereDate('update_date', '<=', $request->to_date)
                                                ->where('update_day','Sunday')
                                                ->where('city', $request->city)
                                                ->where('status','reopen')->count();
        
                    $data['notification'] = DB::table('notifications')->count();
                }
                if($request->from_date && !$request->to_date && $request->city && !$request->category_id && !$request->subcategory_id){
                    $data['ticket'] = Ticket::orderBy('id','desc')->get();
                    $data['open'] = Ticket::where('status','open')->where('city',$request->city)->whereDate('update_date', '<=', $request->form_date)->count();
                    $data['waiting'] = Ticket::where('status','waiting')->where('city',$request->city)->whereDate('update_date', '<=', $request->form_date)->count();
                    $data['visit'] = Ticket::where('status','visit')->where('city',$request->city->whereDate('visit_date', '<=', $request->form_date))->count();
                    $data['revisit'] = Ticket::where('status','revisit')->where('city',$request->city)->whereDate('visit_date', '<=', $request->form_date)->count();
                    $data['close'] = Ticket::where('status','closed')->where('city',$request->city)->whereDate('update_date', '<=', $request->form_date)->count();
                    $data['reopen'] = Ticket::where('status','reopen')->where('city',$request->city)->whereDate('update_date', '<=', $request->form_date)->count();
                    $data['openmonday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->from_date)
                                                ->where('city',  $request->city)
                                                ->where('update_day','Monday')
                                                ->where('status','open')
                                                ->count();
                    $data['opentuesday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->from_date)
                                                ->where('city',  $request->city)
                                                ->where('update_day','Tuesday')
                                                ->where('status','open')
                                                ->count();
                    $data['openwedday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Wednesday')
                                                ->where('city',  $request->city)
                                                ->where('status','open')->count();
                    $data['openthurday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Thursday')
                                                ->where('city',  $request->city)
                                                ->where('status','open')->count();
                    $data['openfriday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Friday')
                                                ->where('city',  $request->city)
                                                ->where('status','open')->count();
                    $data['opensatday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Saturday')
                                                ->where('city',  $request->city)
                                                ->where('status','open')->count();
                    $data['opensunday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Sunday')
                                                ->where('city',  $request->city)
                                                ->where('status','open')->count();
        
        
                    $data['waitingmonday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Monday')
                                                ->where('city',  $request->city)
                                                ->where('status','waiting')->count();
                    $data['waitingtuesday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Tuesday')
                                                ->where('city',  $request->city)
                                                ->where('status','waiting')->count();
                    $data['waitingwedday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Wednesday')
                                                ->where('city',  $request->city)
                                                ->where('status','waiting')->count();
                    $data['waitingthurday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Thursday')
                                                ->where('city',  $request->city)
                                                ->where('status','waiting')->count();
                    $data['waitingfriday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Friday')
                                                ->where('city',  $request->city)
                                                ->where('status','waiting')->count();
                    $data['waitingsatday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Saturday')
                                                ->where('city',  $request->city)
                                                ->where('status','waiting')->count();
                    $data['waitingsunday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Sunday')
                                                ->where('city', '<=', $request->city)
                                                ->where('status','waiting')->count();
                    
                    $data['visitmonday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Monday')
                                                ->where('city', $request->city)
                                                ->where('status','visit')->count();
                    $data['visittuesday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Tuesday')
                                                ->where('city',  $request->city)
                                                ->where('status','visit')->count();
                    $data['visitwedday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Wednesday')
                                                ->where('city', $request->city)
                                                ->where('status','visit')->count();
                    $data['visitthurday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Thursday')
                                                ->where('city', $request->city)
                                                ->where('status','visit')->count();
                    $data['visitfriday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Friday')
                                                ->where('city', $request->city)
                                                ->where('status','visit')->count();
                    $data['visitsatday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Saturday')
                                                ->where('city', $request->city)
                                                ->where('status','visit')->count();
                    $data['visitsunday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Sunday')
                                                ->where('city', $request->city)
                                                ->where('status','visit')->count();
        
                    $data['revisitmonday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Monday')
                                                ->where('city', $request->city)
                                                ->where('status','revisit')->count();
                    $data['revisittuesday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Tuesday')
                                                ->where('city', $request->city)
                                                ->where('status','revisit')->count();
                    $data['revisitwedday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Wednesday')
                                                ->where('city', $request->city)
                                                ->where('status','revisit')->count();
                    $data['revisitthurday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Thursday')
                                                ->where('city', $request->city)
                                                ->where('status','revisit')->count();
                    $data['revisitfriday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Friday')
                                                ->where('city', $request->city)
                                                ->where('status','revisit')->count();
                    $data['revisitsatday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Saturday')
                                                ->where('city', $request->city)
                                                ->where('status','revisit')->count();
                    $data['revisitsunday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Sunday')
                                                ->where('city', $request->city)
                                                ->where('status','revisit')->count();
        
        
                    $data['closemonday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Monday')
                                                ->where('city', $request->city)
                                                ->where('status','closed')->count();
                    $data['closetuesday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Tuesday')
                                                ->where('city', $request->city)
                                                ->where('status','closed')->count();
                    $data['closewedday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Wednesday')
                                                ->where('city', $request->city)
                                                ->where('status','closed')->count();
                    $data['closethurday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Thursday')
                                                ->where('city', $request->city)
                                                ->where('status','closed')->count();
                    $data['closefriday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Friday')
                                                ->where('city', $request->city)
                                                ->where('status','closed')->count();
                    $data['closesatday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Saturday')
                                                ->where('city', $request->city)
                                                ->where('status','closed')->count();
                    $data['closesunday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Sunday')
                                                ->where('city', $request->city)
                                                ->where('status','closed')->count();
        
                    $data['reopenmonday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Monday')
                                                ->where('city', $request->city)
                                                ->where('status','reopen')->count();
                    $data['reopentuesday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Tuesday')
                                                ->where('city', $request->city)
                                                ->where('status','reopen')->count();
                    $data['reopenwedday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Wednesday')
                                                ->where('city', $request->city)
                                                ->where('status','reopen')->count();
                    $data['reopenthurday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Thursday')
                                                ->where('city', $request->city)
                                                ->where('status','reopen')->count();
                    $data['reopenfriday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Friday')
                                                ->where('city', $request->city)
                                                ->where('status','reopen')->count();
                    $data['reopensatday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Saturday')
                                                ->where('city', $request->city)
                                                ->where('status','reopen')->count();
                    $data['reopensunday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Sunday')
                                                ->where('city', $request->city)
                                                ->where('status','reopen')->count();
        
                    $data['notification'] = DB::table('notifications')->count();
                }
                if(!$request->from_date && $request->to_date && $request->city && !$request->category_id && !$request->subcategory_id){
                    $data['ticket'] = Ticket::orderBy('id','desc')->get();
                    $data['open'] = Ticket::where('status','open')->where('city',$request->city)->whereDate('update_date', '<=', $request->to_date)->count();
                    $data['waiting'] = Ticket::where('status','waiting')->where('city',$request->city)->whereDate('update_date', '<=', $request->to_date)->count();
                    $data['visit'] = Ticket::where('status','visit')->where('city',$request->city)->whereDate('visit_date', '<=', $request->to_date)->count();
                    $data['revisit'] = Ticket::where('status','revisit')->where('city',$request->city)->whereDate('visit_date', '<=', $request->to_date)->count();
                    $data['close'] = Ticket::where('status','closed')->where('city',$request->city)->whereDate('update_date', '<=', $request->to_date)->count();
                    $data['reopen'] = Ticket::where('status','reopen')->where('city',$request->city)->whereDate('update_date', '<=', $request->to_date)->count();
                    $data['openmonday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date',$request->to_date)
                                                ->where('city',  $request->city)
                                                ->where('update_day','Monday')
                                                ->where('status','open')
                                                ->count();
                    $data['opentuesday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date',$request->to_date)
                                                ->where('city',  $request->city)
                                                ->where('update_day','Tuesday')
                                                ->where('status','open')
                                                ->count();
                    $data['openwedday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date',$request->to_date)
                                                ->where('update_day','Wednesday')
                                                ->where('city',  $request->city)
                                                ->where('status','open')->count();
                    $data['openthurday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date',$request->to_date)
                                                ->where('update_day','Thursday')
                                                ->where('city',  $request->city)
                                                ->where('status','open')->count();
                    $data['openfriday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date',$request->to_date)
                                                ->where('update_day','Friday')
                                                ->where('city',  $request->city)
                                                ->where('status','open')->count();
                    $data['opensatday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date',$request->to_date)
                                                ->where('update_day','Saturday')
                                                ->where('city',  $request->city)
                                                ->where('status','open')->count();
                    $data['opensunday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date',$request->to_date)
                                                ->where('update_day','Sunday')
                                                ->where('city',  $request->city)
                                                ->where('status','open')->count();
        
        
                    $data['waitingmonday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date',$request->to_date)
                                                ->where('update_day','Monday')
                                                ->where('city',  $request->city)
                                                ->where('status','waiting')->count();
                    $data['waitingtuesday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date',$request->to_date)
                                                ->where('update_day','Tuesday')
                                                ->where('city',  $request->city)
                                                ->where('status','waiting')->count();
                    $data['waitingwedday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date',$request->to_date)
                                                ->where('update_day','Wednesday')
                                                ->where('city',  $request->city)
                                                ->where('status','waiting')->count();
                    $data['waitingthurday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date',$request->to_date)
                                                ->where('update_day','Thursday')
                                                ->where('city',  $request->city)
                                                ->where('status','waiting')->count();
                    $data['waitingfriday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date',$request->to_date)
                                                ->where('update_day','Friday')
                                                ->where('city',  $request->city)
                                                ->where('status','waiting')->count();
                    $data['waitingsatday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date',$request->to_date)
                                                ->where('update_day','Saturday')
                                                ->where('city',  $request->city)
                                                ->where('status','waiting')->count();
                    $data['waitingsunday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date',$request->to_date)
                                                ->where('update_day','Sunday')
                                                ->where('city', '<=', $request->city)
                                                ->where('status','waiting')->count();
                    
                    $data['visitmonday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date',$request->to_date)
                                                ->where('visit_day','Monday')
                                                ->where('city', $request->city)
                                                ->where('status','visit')->count();
                    $data['visittuesday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date',$request->to_date)
                                                ->where('visit_day','Tuesday')
                                                ->where('city',  $request->city)
                                                ->where('status','visit')->count();
                    $data['visitwedday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date',$request->to_date)
                                                ->where('visit_day','Wednesday')
                                                ->where('city', $request->city)
                                                ->where('status','visit')->count();
                    $data['visitthurday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date',$request->to_date)
                                                ->where('visit_day','Thursday')
                                                ->where('city', $request->city)
                                                ->where('status','visit')->count();
                    $data['visitfriday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date',$request->to_date)
                                                ->where('visit_day','Friday')
                                                ->where('city', $request->city)
                                                ->where('status','visit')->count();
                    $data['visitsatday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date',$request->to_date)
                                                ->where('visit_day','Saturday')
                                                ->where('city', $request->city)
                                                ->where('status','visit')->count();
                    $data['visitsunday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date',$request->to_date)
                                                ->where('visit_day','Sunday')
                                                ->where('city', $request->city)
                                                ->where('status','visit')->count();
        
                    $data['revisitmonday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date',$request->to_date)
                                                ->where('visit_day','Monday')
                                                ->where('city', $request->city)
                                                ->where('status','revisit')->count();
                    $data['revisittuesday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date',$request->to_date)
                                                ->where('visit_day','Tuesday')
                                                ->where('city', $request->city)
                                                ->where('status','revisit')->count();
                    $data['revisitwedday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date',$request->to_date)
                                                ->where('visit_day','Wednesday')
                                                ->where('city', $request->city)
                                                ->where('status','revisit')->count();
                    $data['revisitthurday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date',$request->to_date)
                                                ->where('visit_day','Thursday')
                                                ->where('city', $request->city)
                                                ->where('status','revisit')->count();
                    $data['revisitfriday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date',$request->to_date)
                                                ->where('visit_day','Friday')
                                                ->where('city', $request->city)
                                                ->where('status','revisit')->count();
                    $data['revisitsatday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date',$request->to_date)
                                                ->where('visit_day','Saturday')
                                                ->where('city', $request->city)
                                                ->where('status','revisit')->count();
                    $data['revisitsunday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date',$request->to_date)
                                                ->where('visit_day','Sunday')
                                                ->where('city', $request->city)
                                                ->where('status','revisit')->count();
        
        
                    $data['closemonday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date',$request->to_date)
                                                ->where('update_day','Monday')
                                                ->where('city', $request->city)
                                                ->where('status','closed')->count();
                    $data['closetuesday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date',$request->to_date)
                                                ->where('update_day','Tuesday')
                                                ->where('city', $request->city)
                                                ->where('status','closed')->count();
                    $data['closewedday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date',$request->to_date)
                                                ->where('update_day','Wednesday')
                                                ->where('city', $request->city)
                                                ->where('status','closed')->count();
                    $data['closethurday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date',$request->to_date)
                                                ->where('update_day','Thursday')
                                                ->where('city', $request->city)
                                                ->where('status','closed')->count();
                    $data['closefriday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date',$request->to_date)
                                                ->where('update_day','Friday')
                                                ->where('city', $request->city)
                                                ->where('status','closed')->count();
                    $data['closesatday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date',$request->to_date)
                                                ->where('update_day','Saturday')
                                                ->where('city', $request->city)
                                                ->where('status','closed')->count();
                    $data['closesunday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date',$request->to_date)
                                                ->where('update_day','Sunday')
                                                ->where('city', $request->city)
                                                ->where('status','closed')->count();
        
                    $data['reopenmonday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date',$request->to_date)
                                                ->where('update_day','Monday')
                                                ->where('city', $request->city)
                                                ->where('status','reopen')->count();
                    $data['reopentuesday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date',$request->to_date)
                                                ->where('update_day','Tuesday')
                                                ->where('city', $request->city)
                                                ->where('status','reopen')->count();
                    $data['reopenwedday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date',$request->to_date)
                                                ->where('update_day','Wednesday')
                                                ->where('city', $request->city)
                                                ->where('status','reopen')->count();
                    $data['reopenthurday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date',$request->to_date)
                                                ->where('update_day','Thursday')
                                                ->where('city', $request->city)
                                                ->where('status','reopen')->count();
                    $data['reopenfriday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date',$request->to_date)
                                                ->where('update_day','Friday')
                                                ->where('city', $request->city)
                                                ->where('status','reopen')->count();
                    $data['reopensatday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date',$request->to_date)
                                                ->where('update_day','Saturday')
                                                ->where('city', $request->city)
                                                ->where('status','reopen')->count();
                    $data['reopensunday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date',$request->to_date)
                                                ->where('update_day','Sunday')
                                                ->where('city', $request->city)
                                                ->where('status','reopen')->count();
                    $data['notification'] = DB::table('notifications')->count();
                }
                if(!$request->from_date && !$request->to_date && !$request->city && !$request->category_id && !$request->subcategory_id){
                    $data['ticket'] = Ticket::orderBy('id','desc')->get();
                    $data['openmonday'] = Ticket::where('assign_to',$user->id)->where('update_day','Monday')->where('status','open')->count();
                    $data['opentuesday'] = Ticket::where('assign_to',$user->id)->where('update_day','Tuesday')->where('status','open')->count();
                    $data['openwedday'] = Ticket::where('assign_to',$user->id)->where('update_day','Wednesday')->where('status','open')->count();
                    $data['openthurday'] = Ticket::where('assign_to',$user->id)->where('update_day','Thursday')->where('status','open')->count();
                    $data['openfriday'] = Ticket::where('assign_to',$user->id)->where('update_day','Friday')->where('status','open')->count();
                    $data['opensatday'] = Ticket::where('assign_to',$user->id)->where('update_day','Saturday')->where('status','open')->count();
                    $data['opensunday'] = Ticket::where('assign_to',$user->id)->where('update_day','Sunday')->where('status','open')->count();
                    $data['open'] = Ticket::where('status','open')->count();


                    $data['waiting'] = Ticket::where('status','waiting')->count();
                    $data['waitingmonday'] = Ticket::where('assign_to',$user->id)->where('update_day','Monday')->where('status','waiting')->count();
                    $data['waitingtuesday'] = Ticket::where('assign_to',$user->id)->where('update_day','Tuesday')->where('status','waiting')->count();
                    $data['waitingwedday'] = Ticket::where('assign_to',$user->id)->where('update_day','Wednesday')->where('status','waiting')->count();
                    $data['waitingthurday'] = Ticket::where('assign_to',$user->id)->where('update_day','Thursday')->where('status','waiting')->count();
                    $data['waitingfriday'] = Ticket::where('assign_to',$user->id)->where('update_day','Friday')->where('status','waiting')->count();
                    $data['waitingsatday'] = Ticket::where('assign_to',$user->id)->where('update_day','Saturday')->where('status','waiting')->count();
                    $data['waitingsunday'] = Ticket::where('assign_to',$user->id)->where('update_day','Sunday')->where('status','waiting')->count();
                    
                    $data['visit'] = Ticket::where('status','visit')->count();
                    $data['visitmonday'] = Ticket::where('assign_to',$user->id)->where('visit_day','Monday')->where('status','visit')->count();
                    $data['visittuesday'] = Ticket::where('assign_to',$user->id)->where('visit_day','Tuesday')->where('status','visit')->count();
                    $data['visitwedday'] = Ticket::where('assign_to',$user->id)->where('visit_day','Wednesday')->where('status','visit')->count();
                    $data['visitthurday'] = Ticket::where('assign_to',$user->id)->where('visit_day','Thursday')->where('status','visit')->count();
                    $data['visitfriday'] = Ticket::where('assign_to',$user->id)->where('visit_day','Friday')->where('status','visit')->count();
                    $data['visitsatday'] = Ticket::where('assign_to',$user->id)->where('visit_day','Saturday')->where('status','visit')->count();
                    $data['visitsunday'] = Ticket::where('assign_to',$user->id)->where('visit_day','Sunday')->where('status','visit')->count();

                    $data['revisit'] = Ticket::where('status','revisit')->count();
                    $data['revisitmonday'] = Ticket::where('assign_to',$user->id)->where('visit_day','Monday')->where('status','revisit')->count();
                    $data['revisittuesday'] = Ticket::where('assign_to',$user->id)->where('visit_day','Tuesday')->where('status','revisit')->count();
                    $data['revisitwedday'] = Ticket::where('assign_to',$user->id)->where('visit_day','Wednesday')->where('status','revisit')->count();
                    $data['revisitthurday'] = Ticket::where('assign_to',$user->id)->where('visit_day','Thursday')->where('status','revisit')->count();
                    $data['revisitfriday'] = Ticket::where('assign_to',$user->id)->where('visit_day','Friday')->where('status','revisit')->count();
                    $data['revisitsatday'] = Ticket::where('assign_to',$user->id)->where('visit_day','Saturday')->where('status','revisit')->count();
                    $data['revisitsunday'] = Ticket::where('assign_to',$user->id)->where('visit_day','Sunday')->where('status','revisit')->count();


                    $data['close'] = Ticket::where('status','closed')->count();
                    $data['closemonday'] = Ticket::where('assign_to',$user->id)->where('update_day','Monday')->where('status','closed')->count();
                    $data['closetuesday'] = Ticket::where('assign_to',$user->id)->where('update_day','Tuesday')->where('status','closed')->count();
                    $data['closewedday'] = Ticket::where('assign_to',$user->id)->where('update_day','Wednesday')->where('status','closed')->count();
                    $data['closethurday'] = Ticket::where('assign_to',$user->id)->where('update_day','Thursday')->where('status','closed')->count();
                    $data['closefriday'] = Ticket::where('assign_to',$user->id)->where('update_day','Friday')->where('status','closed')->count();
                    $data['closesatday'] = Ticket::where('assign_to',$user->id)->where('update_day','Saturday')->where('status','closed')->count();
                    $data['closesunday'] = Ticket::where('assign_to',$user->id)->where('update_day','Sunday')->where('status','closed')->count();

                    $data['reopen'] = Ticket::where('status','reopen')->count();
                    $data['reopenmonday'] = Ticket::where('assign_to',$user->id)->where('update_day','Monday')->where('status','reopen')->count();
                    $data['reopentuesday'] = Ticket::where('assign_to',$user->id)->where('update_day','Tuesday')->where('status','reopen')->count();
                    $data['reopenwedday'] = Ticket::where('assign_to',$user->id)->where('update_day','Wednesday')->where('status','reopen')->count();
                    $data['reopenthurday'] = Ticket::where('assign_to',$user->id)->where('update_day','Thursday')->where('status','reopen')->count();
                    $data['reopenfriday'] = Ticket::where('assign_to',$user->id)->where('update_day','Friday')->where('status','reopen')->count();
                    $data['reopensatday'] = Ticket::where('assign_to',$user->id)->where('update_day','Saturday')->where('status','reopen')->count();
                    $data['reopensunday'] = Ticket::where('assign_to',$user->id)->where('update_day','Sunday')->where('status','reopen')->count();

                    $data['notification'] = DB::table('notifications')->count();

                }
                if($request->from_date && !$request->to_date && !$request->city && !$request->category_id && !$request->subcategory_id){
                    $data['ticket'] = Ticket::orderBy('id','desc')->get();
                    $data['open'] = Ticket::where('status','open')->whereDate('update_date', $request->from_date)->count();
                    $data['waiting'] = Ticket::where('status','waiting')->whereDate('update_date', $request->from_date)->count();
                    $data['visit'] = Ticket::where('status','visit')->whereDate('visit_date', $request->from_date)->count();
                    $data['revisit'] = Ticket::where('status','revisit')->whereDate('visit_date', $request->from_date)->count();
                    $data['close'] = Ticket::where('status','closed')->whereDate('update_date', $request->from_date)->count();
                    $data['reopen'] = Ticket::where('status','reopen')->whereDate('update_date', $request->from_date)->count();
                    $data['openmonday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Monday')
                                                ->where('status','open')
                                                ->count();
                    $data['opentuesday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Tuesday')
                                                ->where('status','open')
                                                ->count();
                    $data['openwedday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Wednesday')
                                                ->where('status','open')->count();
                    $data['openthurday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Thursday')
                                                ->where('status','open')->count();
                    $data['openfriday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Friday')
                                                ->where('status','open')->count();
                    $data['opensatday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Saturday')
                                                ->where('status','open')->count();
                    $data['opensunday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Sunday')
                                                ->where('status','open')->count();
        
        
                    $data['waitingmonday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Monday')
                                                ->where('status','waiting')->count();
                    $data['waitingtuesday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Tuesday')
                                                ->where('status','waiting')->count();
                    $data['waitingwedday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Wednesday')
                                                ->where('status','waiting')->count();
                    $data['waitingthurday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Thursday')
                                                ->where('status','waiting')->count();
                    $data['waitingfriday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Friday')
                                                ->where('status','waiting')->count();
                    $data['waitingsatday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Saturday')
                                                ->where('status','waiting')->count();
                    $data['waitingsunday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Sunday')
                                                ->where('status','waiting')->count();
                    
                    $data['visitmonday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Monday')
                                                ->where('status','visit')->count();
                    $data['visittuesday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Tuesday')
                                                ->where('status','visit')->count();
                    $data['visitwedday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Wednesday')
                                                ->where('status','visit')->count();
                    $data['visitthurday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Thursday')
                                                ->where('status','visit')->count();
                    $data['visitfriday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Friday')
                                                ->where('status','visit')->count();
                    $data['visitsatday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Saturday')
                                                ->where('status','visit')->count();
                    $data['visitsunday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Sunday')
                                                ->where('status','visit')->count();
        
                    $data['revisitmonday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Monday')
                                                ->where('status','revisit')->count();
                    $data['revisittuesday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Tuesday')
                                                ->where('status','revisit')->count();
                    $data['revisitwedday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Wednesday')
                                                ->where('status','revisit')->count();
                    $data['revisitthurday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Thursday')
                                                ->where('status','revisit')->count();
                    $data['revisitfriday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Friday')
                                                ->where('status','revisit')->count();
                    $data['revisitsatday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Saturday')
                                                ->where('status','revisit')->count();
                    $data['revisitsunday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Sunday')
                                                ->where('status','revisit')->count();
        
        
                    $data['closemonday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Monday')
                                                ->where('status','closed')->count();
                    $data['closetuesday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Tuesday')
                                                ->where('status','closed')->count();
                    $data['closewedday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Wednesday')
                                                ->where('status','closed')->count();
                    $data['closethurday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Thursday')
                                                ->where('status','closed')->count();
                    $data['closefriday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Friday')
                                                ->where('status','closed')->count();
                    $data['closesatday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Saturday')
                                                ->where('status','closed')->count();
                    $data['closesunday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Sunday')
                                                ->where('status','closed')->count();
        
                    $data['reopenmonday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Monday')
                                                ->where('status','reopen')->count();
                    $data['reopentuesday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Tuesday')
                                                ->where('status','reopen')->count();
                    $data['reopenwedday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Wednesday')
                                                ->where('status','reopen')->count();
                    $data['reopenthurday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Thursday')
                                                ->where('status','reopen')->count();
                    $data['reopenfriday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Friday')
                                                ->where('status','reopen')->count();
                    $data['reopensatday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Saturday')
                                                ->where('status','reopen')->count();
                    $data['reopensunday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Sunday')
                                                ->where('status','reopen')->count();
        
                    $data['notification'] = DB::table('notifications')->count();
                }
                if(!$request->from_date && !$request->to_date && $request->city && !$request->category_id && !$request->subcategory_id){
                    $data['ticket'] = Ticket::orderBy('id','desc')->get();
                    $data['open'] = Ticket::where('status','open')->where('city',$request->city)->count();
                    $data['waiting'] = Ticket::where('status','waiting')->where('city',$request->city)->count();
                    $data['visit'] = Ticket::where('status','visit')->where('city',$request->city)->count();
                    $data['revisit'] = Ticket::where('status','revisit')->where('city',$request->city)->count();
                    $data['close'] = Ticket::where('status','closed')->where('city',$request->city)->count();
                    $data['reopen'] = Ticket::where('status','reopen')->where('city',$request->city)->count();
                    $data['openmonday'] = Ticket::where('assign_to',$user->id)->where('city',  $request->city)
                                                ->where('update_day','Monday')
                                                ->where('status','open')
                                                ->count();
                    $data['opentuesday'] = Ticket::where('assign_to',$user->id)->where('city',  $request->city)
                                                ->where('update_day','Tuesday')
                                                ->where('status','open')
                                                ->count();
                    $data['openwedday'] = Ticket::where('assign_to',$user->id)->where('update_day','Wednesday')
                                                ->where('city',  $request->city)
                                                ->where('status','open')->count();
                    $data['openthurday'] = Ticket::where('assign_to',$user->id)->where('update_day','Thursday')
                                                ->where('city',  $request->city)
                                                ->where('status','open')->count();
                    $data['openfriday'] = Ticket::where('assign_to',$user->id)->where('update_day','Friday')
                                                ->where('city',  $request->city)
                                                ->where('status','open')->count();
                    $data['opensatday'] = Ticket::where('assign_to',$user->id)->where('update_day','Saturday')
                                                ->where('city',  $request->city)
                                                ->where('status','open')->count();
                    $data['opensunday'] = Ticket::where('assign_to',$user->id)->where('update_day','Sunday')
                                                ->where('city',  $request->city)
                                                ->where('status','open')->count();
        
        
                    $data['waitingmonday'] = Ticket::where('assign_to',$user->id)->where('update_day','Monday')
                                                ->where('city',  $request->city)
                                                ->where('status','waiting')->count();
                    $data['waitingtuesday'] = Ticket::where('assign_to',$user->id)->where('update_day','Tuesday')
                                                ->where('city',  $request->city)
                                                ->where('status','waiting')->count();
                    $data['waitingwedday'] = Ticket::where('assign_to',$user->id)->where('update_day','Wednesday')
                                                ->where('city',  $request->city)
                                                ->where('status','waiting')->count();
                    $data['waitingthurday'] = Ticket::where('assign_to',$user->id)->where('update_day','Thursday')
                                                ->where('city',  $request->city)
                                                ->where('status','waiting')->count();
                    $data['waitingfriday'] = Ticket::where('assign_to',$user->id)->where('update_day','Friday')
                                                ->where('city',  $request->city)
                                                ->where('status','waiting')->count();
                    $data['waitingsatday'] = Ticket::where('assign_to',$user->id)->where('update_day','Saturday')
                                                ->where('city',  $request->city)
                                                ->where('status','waiting')->count();
                    $data['waitingsunday'] = Ticket::where('assign_to',$user->id)->where('update_day','Sunday')
                                                ->where('city', '<=', $request->city)
                                                ->where('status','waiting')->count();
                    
                    $data['visitmonday'] = Ticket::where('assign_to',$user->id)->where('visit_day','Monday')
                                                ->where('city', $request->city)
                                                ->where('status','visit')->count();
                    $data['visittuesday'] = Ticket::where('assign_to',$user->id)->where('visit_day','Tuesday')
                                                ->where('city',  $request->city)
                                                ->where('status','visit')->count();
                    $data['visitwedday'] = Ticket::where('assign_to',$user->id)->where('visit_day','Wednesday')
                                                ->where('city', $request->city)
                                                ->where('status','visit')->count();
                    $data['visitthurday'] = Ticket::where('assign_to',$user->id)->where('visit_day','Thursday')
                                                ->where('city', $request->city)
                                                ->where('status','visit')->count();
                    $data['visitfriday'] = Ticket::where('assign_to',$user->id)->where('visit_day','Friday')
                                                ->where('city', $request->city)
                                                ->where('status','visit')->count();
                    $data['visitsatday'] = Ticket::where('assign_to',$user->id)->where('visit_day','Saturday')
                                                ->where('city', $request->city)
                                                ->where('status','visit')->count();
                    $data['visitsunday'] = Ticket::where('assign_to',$user->id)->where('visit_day','Sunday')
                                                ->where('city', $request->city)
                                                ->where('status','visit')->count();
        
                    $data['revisitmonday'] = Ticket::where('assign_to',$user->id)->where('visit_day','Monday')
                                                ->where('city', $request->city)
                                                ->where('status','revisit')->count();
                    $data['revisittuesday'] = Ticket::where('assign_to',$user->id)->where('visit_day','Tuesday')
                                                ->where('city', $request->city)
                                                ->where('status','revisit')->count();
                    $data['revisitwedday'] = Ticket::where('assign_to',$user->id)->where('visit_day','Wednesday')
                                                ->where('city', $request->city)
                                                ->where('status','revisit')->count();
                    $data['revisitthurday'] = Ticket::where('assign_to',$user->id)->where('visit_day','Thursday')
                                                ->where('city', $request->city)
                                                ->where('status','revisit')->count();
                    $data['revisitfriday'] = Ticket::where('assign_to',$user->id)->where('visit_day','Friday')
                                                ->where('city', $request->city)
                                                ->where('status','revisit')->count();
                    $data['revisitsatday'] = Ticket::where('assign_to',$user->id)->where('visit_day','Saturday')
                                                ->where('city', $request->city)
                                                ->where('status','revisit')->count();
                    $data['revisitsunday'] = Ticket::where('assign_to',$user->id)->where('visit_day','Sunday')
                                                ->where('city', $request->city)
                                                ->where('status','revisit')->count();
        
        
                    $data['closemonday'] = Ticket::where('assign_to',$user->id)->where('update_day','Monday')
                                                ->where('city', $request->city)
                                                ->where('status','closed')->count();
                    $data['closetuesday'] = Ticket::where('assign_to',$user->id)->where('update_day','Tuesday')
                                                ->where('city', $request->city)
                                                ->where('status','closed')->count();
                    $data['closewedday'] = Ticket::where('assign_to',$user->id)->where('update_day','Wednesday')
                                                ->where('city', $request->city)
                                                ->where('status','closed')->count();
                    $data['closethurday'] = Ticket::where('assign_to',$user->id)->where('update_day','Thursday')
                                                ->where('city', $request->city)
                                                ->where('status','closed')->count();
                    $data['closefriday'] = Ticket::where('assign_to',$user->id)->where('update_day','Friday')
                                                ->where('city', $request->city)
                                                ->where('status','closed')->count();
                    $data['closesatday'] = Ticket::where('assign_to',$user->id)->where('update_day','Saturday')
                                                ->where('city', $request->city)
                                                ->where('status','closed')->count();
                    $data['closesunday'] = Ticket::where('assign_to',$user->id)->where('update_day','Sunday')
                                                ->where('city', $request->city)
                                                ->where('status','closed')->count();
        
                    $data['reopenmonday'] = Ticket::where('assign_to',$user->id)->where('update_day','Monday')
                                                ->where('city', $request->city)
                                                ->where('status','reopen')->count();
                    $data['reopentuesday'] = Ticket::where('assign_to',$user->id)->where('update_day','Tuesday')
                                                ->where('city', $request->city)
                                                ->where('status','reopen')->count();
                    $data['reopenwedday'] = Ticket::where('assign_to',$user->id)->where('update_day','Wednesday')
                                                ->where('city', $request->city)
                                                ->where('status','reopen')->count();
                    $data['reopenthurday'] = Ticket::where('assign_to',$user->id)->where('update_day','Thursday')
                                                ->where('city', $request->city)
                                                ->where('status','reopen')->count();
                    $data['reopenfriday'] = Ticket::where('assign_to',$user->id)->where('update_day','Friday')
                                                ->where('city', $request->city)
                                                ->where('status','reopen')->count();
                    $data['reopensatday'] = Ticket::where('assign_to',$user->id)->where('update_day','Saturday')
                                                ->where('city', $request->city)
                                                ->where('status','reopen')->count();
                    $data['reopensunday'] = Ticket::where('assign_to',$user->id)->where('update_day','Sunday')
                                                ->where('city', $request->city)
                                                ->where('status','reopen')->count();
                    $data['notification'] = DB::table('notifications')->count();
                }
                if(!$request->from_date && $request->to_date && !$request->city && !$request->category_id && !$request->subcategory_id){
                    $data['ticket'] = Ticket::orderBy('id','desc')->get();
                    $data['open'] = Ticket::where('status','open')->whereDate('update_date',  $request->to_date)->count();
                    $data['waiting'] = Ticket::where('status','waiting')->whereDate('update_date',  $request->to_date)->count();
                    $data['visit'] = Ticket::where('status','visit')->whereDate('visit_date',  $request->to_date)->count();
                    $data['revisit'] = Ticket::where('status','revisit')->whereDate('visit_date',  $request->to_date)->count();
                    $data['close'] = Ticket::where('status','closed')->whereDate('update_date',  $request->to_date)->count();
                    $data['reopen'] = Ticket::where('status','reopen')->whereDate('update_date',  $request->to_date)->count();
                    $data['openmonday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Monday')
                                                ->where('status','open')
                                                ->count();
                    $data['opentuesday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Tuesday')
                                                ->where('status','open')
                                                ->count();
                    $data['openwedday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Wednesday')
                                                ->where('status','open')->count();
                    $data['openthurday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Thursday')
                                                ->where('status','open')->count();
                    $data['openfriday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Friday')
                                                ->where('status','open')->count();
                    $data['opensatday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Saturday')
                                                ->where('status','open')->count();
                    $data['opensunday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Sunday')
                                                ->where('status','open')->count();
        
        
                    $data['waitingmonday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Monday')
                                                ->where('status','waiting')->count();
                    $data['waitingtuesday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Tuesday')
                                                ->where('status','waiting')->count();
                    $data['waitingwedday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Wednesday')
                                                ->where('status','waiting')->count();
                    $data['waitingthurday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Thursday')
                                                ->where('status','waiting')->count();
                    $data['waitingfriday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Friday')
                                                ->where('status','waiting')->count();
                    $data['waitingsatday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Saturday')
                                                ->where('status','waiting')->count();
                    $data['waitingsunday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Sunday')
                                                ->where('status','waiting')->count();
                    
                    $data['visitmonday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Monday')
                                                ->where('status','visit')->count();
                    $data['visittuesday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Tuesday')
                                                ->where('status','visit')->count();
                    $data['visitwedday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Wednesday')
                                                ->where('status','visit')->count();
                    $data['visitthurday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Thursday')
                                                ->where('status','visit')->count();
                    $data['visitfriday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Friday')
                                                ->where('status','visit')->count();
                    $data['visitsatday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Saturday')
                                                ->where('status','visit')->count();
                    $data['visitsunday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Sunday')
                                                ->where('status','visit')->count();
        
                    $data['revisitmonday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Monday')
                                                ->where('status','revisit')->count();
                    $data['revisittuesday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Tuesday')
                                                ->where('status','revisit')->count();
                    $data['revisitwedday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Wednesday')
                                                ->where('status','revisit')->count();
                    $data['revisitthurday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Thursday')
                                                ->where('status','revisit')->count();
                    $data['revisitfriday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Friday')
                                                ->where('status','revisit')->count();
                    $data['revisitsatday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Saturday')
                                                ->where('status','revisit')->count();
                    $data['revisitsunday'] = Ticket::where('assign_to',$user->id)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Sunday')
                                                ->where('status','revisit')->count();
        
        
                    $data['closemonday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Monday')
                                                ->where('status','closed')->count();
                    $data['closetuesday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Tuesday')
                                                ->where('status','closed')->count();
                    $data['closewedday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Wednesday')
                                                ->where('status','closed')->count();
                    $data['closethurday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Thursday')
                                                ->where('status','closed')->count();
                    $data['closefriday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Friday')
                                                ->where('status','closed')->count();
                    $data['closesatday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Saturday')
                                                ->where('status','closed')->count();
                    $data['closesunday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Sunday')
                                                ->where('status','closed')->count();
        
                    $data['reopenmonday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Monday')
                                                ->where('status','reopen')->count();
                    $data['reopentuesday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Tuesday')
                                                ->where('status','reopen')->count();
                    $data['reopenwedday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Wednesday')
                                                ->where('status','reopen')->count();
                    $data['reopenthurday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Thursday')
                                                ->where('status','reopen')->count();
                    $data['reopenfriday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Friday')
                                                ->where('status','reopen')->count();
                    $data['reopensatday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Saturday')
                                                ->where('status','reopen')->count();
                    $data['reopensunday'] = Ticket::where('assign_to',$user->id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Sunday')
                                                ->where('status','reopen')->count();
        
                    $data['notification'] = DB::table('notifications')->count();
                }
                if(!$request->from_date && !$request->to_date && !$request->city && $request->category_id && !$request->subcategory_id){
                    $data['ticket'] = Ticket::orderBy('id','desc')->get();
                    $data['open'] = Ticket::where('status','open')->where('category_id', $request->category_id)->count();
                    $data['waiting'] = Ticket::where('status','waiting')->where('category_id', $request->category_id)->count();
                    $data['visit'] = Ticket::where('status','visit')->where('category_id', $request->category_id)->count();
                    $data['revisit'] = Ticket::where('status','revisit')->where('category_id', $request->category_id)->count();
                    $data['close'] = Ticket::where('status','closed')->where('category_id', $request->category_id)->count();
                    $data['reopen'] = Ticket::where('status','reopen')->where('category_id', $request->category_id)->count();
                    $data['openmonday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)
                                                ->where('update_day','Monday')
                                                ->where('status','open')
                                                ->count();
                    $data['opentuesday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)
                                                ->where('update_day','Tuesday')
                                                ->where('status','open')
                                                ->count();
                    $data['openwedday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)
                                                ->where('update_day','Wednesday')
                                                ->where('status','open')->count();
                    $data['openthurday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)
                                                ->where('update_day','Thursday')
                                                ->where('status','open')->count();
                    $data['openfriday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)
                                                ->where('update_day','Friday')
                                                ->where('status','open')->count();
                    $data['opensatday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)
                                                ->where('update_day','Saturday')
                                                ->where('status','open')->count();
                    $data['opensunday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)
                                                ->where('update_day','Sunday')
                                                ->where('status','open')->count();
        
        
                    $data['waitingmonday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)
                                                ->where('update_day','Monday')
                                                ->where('status','waiting')->count();
                    $data['waitingtuesday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)
                                                ->where('update_day','Tuesday')
                                                ->where('status','waiting')->count();
                    $data['waitingwedday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)
                                                ->where('update_day','Wednesday')
                                                ->where('status','waiting')->count();
                    $data['waitingthurday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)
                                                ->where('update_day','Thursday')
                                                ->where('status','waiting')->count();
                    $data['waitingfriday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)
                                                ->where('update_day','Friday')
                                                ->where('status','waiting')->count();
                    $data['waitingsatday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)
                                                ->where('update_day','Saturday')
                                                ->where('status','waiting')->count();
                    $data['waitingsunday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)
                                                ->where('update_day','Sunday')
                                                ->where('status','waiting')->count();
                    
                    $data['visitmonday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)
                                                ->where('visit_day','Monday')
                                                ->where('status','visit')->count();
                    $data['visittuesday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)
                                                ->where('visit_day','Tuesday')
                                                ->where('status','visit')->count();
                    $data['visitwedday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)
                                                ->where('visit_day','Wednesday')
                                                ->where('status','visit')->count();
                    $data['visitthurday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)
                                                ->where('visit_day','Thursday')
                                                ->where('status','visit')->count();
                    $data['visitfriday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)
                                                ->where('visit_day','Friday')
                                                ->where('status','visit')->count();
                    $data['visitsatday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)
                                                ->where('visit_day','Saturday')
                                                ->where('status','visit')->count();
                    $data['visitsunday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)
                                                ->where('visit_day','Sunday')
                                                ->where('status','visit')->count();
        
                    $data['revisitmonday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)
                                                ->where('visit_day','Monday')
                                                ->where('status','revisit')->count();
                    $data['revisittuesday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)
                                                ->where('visit_day','Tuesday')
                                                ->where('status','revisit')->count();
                    $data['revisitwedday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)
                                                ->where('visit_day','Wednesday')
                                                ->where('status','revisit')->count();
                    $data['revisitthurday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)
                                                ->where('visit_day','Thursday')
                                                ->where('status','revisit')->count();
                    $data['revisitfriday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)
                                                ->where('visit_day','Friday')
                                                ->where('status','revisit')->count();
                    $data['revisitsatday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)
                                                ->where('visit_day','Saturday')
                                                ->where('status','revisit')->count();
                    $data['revisitsunday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)
                                                ->where('visit_day','Sunday')
                                                ->where('status','revisit')->count();
        
        
                    $data['closemonday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)
                                                ->where('update_day','Monday')
                                                ->where('status','closed')->count();
                    $data['closetuesday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)
                                                ->where('update_day','Tuesday')
                                                ->where('status','closed')->count();
                    $data['closewedday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)
                                                ->where('update_day','Wednesday')
                                                ->where('status','closed')->count();
                    $data['closethurday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)
                                                ->where('update_day','Thursday')
                                                ->where('status','closed')->count();
                    $data['closefriday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)
                                                ->where('update_day','Friday')
                                                ->where('status','closed')->count();
                    $data['closesatday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)
                                                ->where('update_day','Saturday')
                                                ->where('status','closed')->count();
                    $data['closesunday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)
                                                ->where('update_day','Sunday')
                                                ->where('status','closed')->count();
        
                    $data['reopenmonday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)
                                                ->where('update_day','Monday')
                                                ->where('status','reopen')->count();
                    $data['reopentuesday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)
                                                ->where('update_day','Tuesday')
                                                ->where('status','reopen')->count();
                    $data['reopenwedday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)
                                                ->where('update_day','Wednesday')
                                                ->where('status','reopen')->count();
                    $data['reopenthurday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)
                                                ->where('update_day','Thursday')
                                                ->where('status','reopen')->count();
                    $data['reopenfriday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)
                                                ->where('update_day','Friday')
                                                ->where('status','reopen')->count();
                    $data['reopensatday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)
                                                ->where('update_day','Saturday')
                                                ->where('status','reopen')->count();
                    $data['reopensunday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)
                                                ->where('update_day','Sunday')
                                                ->where('status','reopen')->count();
        
                    $data['notification'] = DB::table('notifications')->count();
                }
                if(!$request->from_date && !$request->to_date && !$request->city && !$request->category_id && $request->subcategory_id){
                    $data['ticket'] = Ticket::orderBy('id','desc')->get();
                    $data['open'] = Ticket::where('status','open')->where('subcategory_id', $request->subcategory_id)->count();
                    $data['waiting'] = Ticket::where('status','waiting')->where('subcategory_id', $request->subcategory_id)->count();
                    $data['visit'] = Ticket::where('status','visit')->where('subcategory_id', $request->subcategory_id)->count();
                    $data['revisit'] = Ticket::where('status','revisit')->where('subcategory_id', $request->subcategory_id)->count();
                    $data['close'] = Ticket::where('status','closed')->where('subcategory_id', $request->subcategory_id)->count();
                    $data['reopen'] = Ticket::where('status','reopen')->where('subcategory_id', $request->subcategory_id)->count();
                    $data['openmonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)
                                                ->where('update_day','Monday')
                                                ->where('status','open')
                                                ->count();
                    $data['opentuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)
                                                ->where('update_day','Tuesday')
                                                ->where('status','open')
                                                ->count();
                    $data['openwedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)
                                                ->where('update_day','Wednesday')
                                                ->where('status','open')->count();
                    $data['openthurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)
                                                ->where('update_day','Thursday')
                                                ->where('status','open')->count();
                    $data['openfriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)
                                                ->where('update_day','Friday')
                                                ->where('status','open')->count();
                    $data['opensatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)
                                                ->where('update_day','Saturday')
                                                ->where('status','open')->count();
                    $data['opensunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)
                                                ->where('update_day','Sunday')
                                                ->where('status','open')->count();
        
        
                    $data['waitingmonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)
                                                ->where('update_day','Monday')
                                                ->where('status','waiting')->count();
                    $data['waitingtuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)
                                                ->where('update_day','Tuesday')
                                                ->where('status','waiting')->count();
                    $data['waitingwedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)
                                                ->where('update_day','Wednesday')
                                                ->where('status','waiting')->count();
                    $data['waitingthurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)
                                                ->where('update_day','Thursday')
                                                ->where('status','waiting')->count();
                    $data['waitingfriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)
                                                ->where('update_day','Friday')
                                                ->where('status','waiting')->count();
                    $data['waitingsatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)
                                                ->where('update_day','Saturday')
                                                ->where('status','waiting')->count();
                    $data['waitingsunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)
                                                ->where('update_day','Sunday')
                                                ->where('status','waiting')->count();
                    
                    $data['visitmonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)
                                                ->where('visit_day','Monday')
                                                ->where('status','visit')->count();
                    $data['visittuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)
                                                ->where('visit_day','Tuesday')
                                                ->where('status','visit')->count();
                    $data['visitwedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)
                                                ->where('visit_day','Wednesday')
                                                ->where('status','visit')->count();
                    $data['visitthurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)
                                                ->where('visit_day','Thursday')
                                                ->where('status','visit')->count();
                    $data['visitfriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)
                                                ->where('visit_day','Friday')
                                                ->where('status','visit')->count();
                    $data['visitsatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)
                                                ->where('visit_day','Saturday')
                                                ->where('status','visit')->count();
                    $data['visitsunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)
                                                ->where('visit_day','Sunday')
                                                ->where('status','visit')->count();
        
                    $data['revisitmonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)
                                                ->where('visit_day','Monday')
                                                ->where('status','revisit')->count();
                    $data['revisittuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)
                                                ->where('visit_day','Tuesday')
                                                ->where('status','revisit')->count();
                    $data['revisitwedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)
                                                ->where('visit_day','Wednesday')
                                                ->where('status','revisit')->count();
                    $data['revisitthurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)
                                                ->where('visit_day','Thursday')
                                                ->where('status','revisit')->count();
                    $data['revisitfriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)
                                                ->where('visit_day','Friday')
                                                ->where('status','revisit')->count();
                    $data['revisitsatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)
                                                ->where('visit_day','Saturday')
                                                ->where('status','revisit')->count();
                    $data['revisitsunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)
                                                ->where('visit_day','Sunday')
                                                ->where('status','revisit')->count();
        
        
                    $data['closemonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)
                                                ->where('update_day','Monday')
                                                ->where('status','closed')->count();
                    $data['closetuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)
                                                ->where('update_day','Tuesday')
                                                ->where('status','closed')->count();
                    $data['closewedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)
                                                ->where('update_day','Wednesday')
                                                ->where('status','closed')->count();
                    $data['closethurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)
                                                ->where('update_day','Thursday')
                                                ->where('status','closed')->count();
                    $data['closefriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)
                                                ->where('update_day','Friday')
                                                ->where('status','closed')->count();
                    $data['closesatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)
                                                ->where('update_day','Saturday')
                                                ->where('status','closed')->count();
                    $data['closesunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)
                                                ->where('update_day','Sunday')
                                                ->where('status','closed')->count();
        
                    $data['reopenmonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)
                                                ->where('update_day','Monday')
                                                ->where('status','reopen')->count();
                    $data['reopentuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)
                                                ->where('update_day','Tuesday')
                                                ->where('status','reopen')->count();
                    $data['reopenwedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)
                                                ->where('update_day','Wednesday')
                                                ->where('status','reopen')->count();
                    $data['reopenthurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)
                                                ->where('update_day','Thursday')
                                                ->where('status','reopen')->count();
                    $data['reopenfriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)
                                                ->where('update_day','Friday')
                                                ->where('status','reopen')->count();
                    $data['reopensatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)
                                                ->where('update_day','Saturday')
                                                ->where('status','reopen')->count();
                    $data['reopensunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)
                                                ->where('update_day','Sunday')
                                                ->where('status','reopen')->count();
        
                    $data['notification'] = DB::table('notifications')->count();
                }
                if(!$request->from_date && !$request->to_date && !$request->city && $request->category_id && $request->subcategory_id){
                    $data['ticket'] = Ticket::orderBy('id','desc')->get();
                    $data['open'] = Ticket::where('status','open')->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['waiting'] = Ticket::where('status','waiting')->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['visit'] = Ticket::where('status','visit')->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['revisit'] = Ticket::where('status','revisit')->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['close'] = Ticket::where('status','closed')->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['reopen'] = Ticket::where('status','reopen')->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['openmonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)
                                                ->where('update_day','Monday')
                                                ->where('status','open')
                                                ->count();
                    $data['opentuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)
                                                ->where('update_day','Tuesday')
                                                ->where('status','open')
                                                ->count();
                    $data['openwedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)
                                                ->where('update_day','Wednesday')
                                                ->where('status','open')->count();
                    $data['openthurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)
                                                ->where('update_day','Thursday')
                                                ->where('status','open')->count();
                    $data['openfriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)
                                                ->where('update_day','Friday')
                                                ->where('status','open')->count();
                    $data['opensatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)
                                                ->where('update_day','Saturday')
                                                ->where('status','open')->count();
                    $data['opensunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)
                                                ->where('update_day','Sunday')
                                                ->where('status','open')->count();
        
        
                    $data['waitingmonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)
                                                ->where('update_day','Monday')
                                                ->where('status','waiting')->count();
                    $data['waitingtuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)
                                                ->where('update_day','Tuesday')
                                                ->where('status','waiting')->count();
                    $data['waitingwedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)
                                                ->where('update_day','Wednesday')
                                                ->where('status','waiting')->count();
                    $data['waitingthurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)
                                                ->where('update_day','Thursday')
                                                ->where('status','waiting')->count();
                    $data['waitingfriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)
                                                ->where('update_day','Friday')
                                                ->where('status','waiting')->count();
                    $data['waitingsatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)
                                                ->where('update_day','Saturday')
                                                ->where('status','waiting')->count();
                    $data['waitingsunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)
                                                ->where('update_day','Sunday')
                                                ->where('status','waiting')->count();
                    
                    $data['visitmonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)
                                                ->where('visit_day','Monday')
                                                ->where('status','visit')->count();
                    $data['visittuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)
                                                ->where('visit_day','Tuesday')
                                                ->where('status','visit')->count();
                    $data['visitwedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)
                                                ->where('visit_day','Wednesday')
                                                ->where('status','visit')->count();
                    $data['visitthurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)
                                                ->where('visit_day','Thursday')
                                                ->where('status','visit')->count();
                    $data['visitfriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)
                                                ->where('visit_day','Friday')
                                                ->where('status','visit')->count();
                    $data['visitsatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)
                                                ->where('visit_day','Saturday')
                                                ->where('status','visit')->count();
                    $data['visitsunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)
                                                ->where('visit_day','Sunday')
                                                ->where('status','visit')->count();
        
                    $data['revisitmonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)
                                                ->where('visit_day','Monday')
                                                ->where('status','revisit')->count();
                    $data['revisittuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)
                                                ->where('visit_day','Tuesday')
                                                ->where('status','revisit')->count();
                    $data['revisitwedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)
                                                ->where('visit_day','Wednesday')
                                                ->where('status','revisit')->count();
                    $data['revisitthurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)
                                                ->where('visit_day','Thursday')
                                                ->where('status','revisit')->count();
                    $data['revisitfriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)
                                                ->where('visit_day','Friday')
                                                ->where('status','revisit')->count();
                    $data['revisitsatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)
                                                ->where('visit_day','Saturday')
                                                ->where('status','revisit')->count();
                    $data['revisitsunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)
                                                ->where('visit_day','Sunday')
                                                ->where('status','revisit')->count();
        
        
                    $data['closemonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)
                                                ->where('update_day','Monday')
                                                ->where('status','closed')->count();
                    $data['closetuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)
                                                ->where('update_day','Tuesday')
                                                ->where('status','closed')->count();
                    $data['closewedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)
                                                ->where('update_day','Wednesday')
                                                ->where('status','closed')->count();
                    $data['closethurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)
                                                ->where('update_day','Thursday')
                                                ->where('status','closed')->count();
                    $data['closefriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)
                                                ->where('update_day','Friday')
                                                ->where('status','closed')->count();
                    $data['closesatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)
                                                ->where('update_day','Saturday')
                                                ->where('status','closed')->count();
                    $data['closesunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)
                                                ->where('update_day','Sunday')
                                                ->where('status','closed')->count();
        
                    $data['reopenmonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)
                                                ->where('update_day','Monday')
                                                ->where('status','reopen')->count();
                    $data['reopentuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)
                                                ->where('update_day','Tuesday')
                                                ->where('status','reopen')->count();
                    $data['reopenwedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)
                                                ->where('update_day','Wednesday')
                                                ->where('status','reopen')->count();
                    $data['reopenthurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)
                                                ->where('update_day','Thursday')
                                                ->where('status','reopen')->count();
                    $data['reopenfriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)
                                                ->where('update_day','Friday')
                                                ->where('status','reopen')->count();
                    $data['reopensatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)
                                                ->where('update_day','Saturday')
                                                ->where('status','reopen')->count();
                    $data['reopensunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)
                                                ->where('update_day','Sunday')
                                                ->where('status','reopen')->count();
        
                    $data['notification'] = DB::table('notifications')->count();
                }
                if(!$request->from_date && !$request->to_date && $request->city && $request->category_id && $request->subcategory_id){
                    $data['ticket'] = Ticket::orderBy('id','desc')->get();
                    $data['open'] = Ticket::where('status','open')->where('category_id', $request->category_id)->where('city',$request->city)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['waiting'] = Ticket::where('status','waiting')->where('category_id', $request->category_id)->where('city',$request->city)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['visit'] = Ticket::where('status','visit')->where('category_id', $request->category_id)->where('city',$request->city)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['revisit'] = Ticket::where('status','revisit')->where('category_id', $request->category_id)->where('city',$request->city)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['close'] = Ticket::where('status','closed')->where('category_id', $request->category_id)->where('city',$request->city)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['reopen'] = Ticket::where('status','reopen')->where('category_id', $request->category_id)->where('city',$request->city)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['openmonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('update_day','Monday')
                                                ->where('status','open')
                                                ->count();
                    $data['opentuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('update_day','Tuesday')
                                                ->where('status','open')
                                                ->count();
                    $data['openwedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('update_day','Wednesday')
                                                ->where('status','open')->count();
                    $data['openthurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('update_day','Thursday')
                                                ->where('status','open')->count();
                    $data['openfriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('update_day','Friday')
                                                ->where('status','open')->count();
                    $data['opensatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('update_day','Saturday')
                                                ->where('status','open')->count();
                    $data['opensunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('update_day','Sunday')
                                                ->where('status','open')->count();
        
        
                    $data['waitingmonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('update_day','Monday')
                                                ->where('status','waiting')->count();
                    $data['waitingtuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('update_day','Tuesday')
                                                ->where('status','waiting')->count();
                    $data['waitingwedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('update_day','Wednesday')
                                                ->where('status','waiting')->count();
                    $data['waitingthurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('update_day','Thursday')
                                                ->where('status','waiting')->count();
                    $data['waitingfriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('update_day','Friday')
                                                ->where('status','waiting')->count();
                    $data['waitingsatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('update_day','Saturday')
                                                ->where('status','waiting')->count();
                    $data['waitingsunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('update_day','Sunday')
                                                ->where('status','waiting')->count();
                    
                    $data['visitmonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('visit_day','Monday')
                                                ->where('status','visit')->count();
                    $data['visittuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('visit_day','Tuesday')
                                                ->where('status','visit')->count();
                    $data['visitwedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('visit_day','Wednesday')
                                                ->where('status','visit')->count();
                    $data['visitthurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('visit_day','Thursday')
                                                ->where('status','visit')->count();
                    $data['visitfriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('visit_day','Friday')
                                                ->where('status','visit')->count();
                    $data['visitsatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('visit_day','Saturday')
                                                ->where('status','visit')->count();
                    $data['visitsunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('visit_day','Sunday')
                                                ->where('status','visit')->count();
        
                    $data['revisitmonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('visit_day','Monday')
                                                ->where('status','revisit')->count();
                    $data['revisittuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('visit_day','Tuesday')
                                                ->where('status','revisit')->count();
                    $data['revisitwedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('visit_day','Wednesday')
                                                ->where('status','revisit')->count();
                    $data['revisitthurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('visit_day','Thursday')
                                                ->where('status','revisit')->count();
                    $data['revisitfriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('visit_day','Friday')
                                                ->where('status','revisit')->count();
                    $data['revisitsatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('visit_day','Saturday')
                                                ->where('status','revisit')->count();
                    $data['revisitsunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('visit_day','Sunday')
                                                ->where('status','revisit')->count();
        
        
                    $data['closemonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('update_day','Monday')
                                                ->where('status','closed')->count();
                    $data['closetuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('update_day','Tuesday')
                                                ->where('status','closed')->count();
                    $data['closewedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('update_day','Wednesday')
                                                ->where('status','closed')->count();
                    $data['closethurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('update_day','Thursday')
                                                ->where('status','closed')->count();
                    $data['closefriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('update_day','Friday')
                                                ->where('status','closed')->count();
                    $data['closesatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('update_day','Saturday')
                                                ->where('status','closed')->count();
                    $data['closesunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('update_day','Sunday')
                                                ->where('status','closed')->count();
        
                    $data['reopenmonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('update_day','Monday')
                                                ->where('status','reopen')->count();
                    $data['reopentuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('update_day','Tuesday')
                                                ->where('status','reopen')->count();
                    $data['reopenwedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('update_day','Wednesday')
                                                ->where('status','reopen')->count();
                    $data['reopenthurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('update_day','Thursday')
                                                ->where('status','reopen')->count();
                    $data['reopenfriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('update_day','Friday')
                                                ->where('status','reopen')->count();
                    $data['reopensatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('update_day','Saturday')
                                                ->where('status','reopen')->count();
                    $data['reopensunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('update_day','Sunday')
                                                ->where('status','reopen')->count();
        
                    $data['notification'] = DB::table('notifications')->count();
                }
                if($request->from_date && !$request->to_date && $request->city && $request->category_id && $request->subcategory_id){
                    $data['ticket'] = Ticket::orderBy('id','desc')->get();
                    $data['open'] = Ticket::where('status','open')->where('city',$request->city)->where('category_id', $request->category_id)->whereDate('update_date', $request->from_date)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['waiting'] = Ticket::where('status','waiting')->where('city',$request->city)->where('category_id', $request->category_id)->whereDate('update_date', $request->from_date)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['visit'] = Ticket::where('status','visit')->where('city',$request->city)->where('category_id', $request->category_id)->whereDate('visit_date', $request->from_date)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['revisit'] = Ticket::where('status','revisit')->where('city',$request->city)->where('category_id', $request->category_id)->whereDate('visit_date', $request->from_date)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['close'] = Ticket::where('status','closed')->where('city',$request->city)->where('category_id', $request->category_id)->whereDate('update_date', $request->from_date)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['reopen'] = Ticket::where('status','reopen')->where('city',$request->city)->where('category_id', $request->category_id)->whereDate('update_date', $request->from_date)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['openmonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Monday')
                                                ->where('status','open')
                                                ->count();
                    $data['opentuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Tuesday')
                                                ->where('status','open')
                                                ->count();
                    $data['openwedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Wednesday')
                                                ->where('status','open')->count();
                    $data['openthurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Thursday')
                                                ->where('status','open')->count();
                    $data['openfriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Friday')
                                                ->where('status','open')->count();
                    $data['opensatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Saturday')
                                                ->where('status','open')->count();
                    $data['opensunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Sunday')
                                                ->where('status','open')->count();
        
        
                    $data['waitingmonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Monday')
                                                ->where('status','waiting')->count();
                    $data['waitingtuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Tuesday')
                                                ->where('status','waiting')->count();
                    $data['waitingwedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Wednesday')
                                                ->where('status','waiting')->count();
                    $data['waitingthurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Thursday')
                                                ->where('status','waiting')->count();
                    $data['waitingfriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Friday')
                                                ->where('status','waiting')->count();
                    $data['waitingsatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Saturday')
                                                ->where('status','waiting')->count();
                    $data['waitingsunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Sunday')
                                                ->where('status','waiting')->count();
                    
                    $data['visitmonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Monday')
                                                ->where('status','visit')->count();
                    $data['visittuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Tuesday')
                                                ->where('status','visit')->count();
                    $data['visitwedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Wednesday')
                                                ->where('status','visit')->count();
                    $data['visitthurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Thursday')
                                                ->where('status','visit')->count();
                    $data['visitfriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Friday')
                                                ->where('status','visit')->count();
                    $data['visitsatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Saturday')
                                                ->where('status','visit')->count();
                    $data['visitsunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Sunday')
                                                ->where('status','visit')->count();
        
                    $data['revisitmonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Monday')
                                                ->where('status','revisit')->count();
                    $data['revisittuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Tuesday')
                                                ->where('status','revisit')->count();
                    $data['revisitwedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Wednesday')
                                                ->where('status','revisit')->count();
                    $data['revisitthurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Thursday')
                                                ->where('status','revisit')->count();
                    $data['revisitfriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Friday')
                                                ->where('status','revisit')->count();
                    $data['revisitsatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Saturday')
                                                ->where('status','revisit')->count();
                    $data['revisitsunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Sunday')
                                                ->where('status','revisit')->count();
        
        
                    $data['closemonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Monday')
                                                ->where('status','closed')->count();
                    $data['closetuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Tuesday')
                                                ->where('status','closed')->count();
                    $data['closewedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Wednesday')
                                                ->where('status','closed')->count();
                    $data['closethurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Thursday')
                                                ->where('status','closed')->count();
                    $data['closefriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Friday')
                                                ->where('status','closed')->count();
                    $data['closesatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Saturday')
                                                ->where('status','closed')->count();
                    $data['closesunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Sunday')
                                                ->where('status','closed')->count();
        
                    $data['reopenmonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Monday')
                                                ->where('status','reopen')->count();
                    $data['reopentuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Tuesday')
                                                ->where('status','reopen')->count();
                    $data['reopenwedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Wednesday')
                                                ->where('status','reopen')->count();
                    $data['reopenthurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Thursday')
                                                ->where('status','reopen')->count();
                    $data['reopenfriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Friday')
                                                ->where('status','reopen')->count();
                    $data['reopensatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Saturday')
                                                ->where('status','reopen')->count();
                    $data['reopensunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Sunday')
                                                ->where('status','reopen')->count();
        
                    $data['notification'] = DB::table('notifications')->count();
                }
                if(!$request->from_date && $request->to_date && $request->city && $request->category_id && $request->subcategory_id){
                    $data['ticket'] = Ticket::orderBy('id','desc')->get();
                    $data['open'] = Ticket::where('status','open')->where('city',$request->city)->where('category_id', $request->category_id)->whereDate('update_date', $request->to_date)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['waiting'] = Ticket::where('status','waiting')->where('city',$request->city)->where('category_id', $request->category_id)->whereDate('update_date', $request->to_date)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['visit'] = Ticket::where('status','visit')->where('city',$request->city)->where('category_id', $request->category_id)->whereDate('visit_date', $request->to_date)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['revisit'] = Ticket::where('status','revisit')->where('city',$request->city)->where('category_id', $request->category_id)->whereDate('visit_date', $request->to_date)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['close'] = Ticket::where('status','closed')->where('city',$request->city)->where('category_id', $request->category_id)->whereDate('update_date', $request->to_date)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['reopen'] = Ticket::where('status','reopen')->where('city',$request->city)->where('category_id', $request->category_id)->whereDate('update_date', $request->to_date)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['openmonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Monday')
                                                ->where('status','open')
                                                ->count();
                    $data['opentuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Tuesday')
                                                ->where('status','open')
                                                ->count();
                    $data['openwedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Wednesday')
                                                ->where('status','open')->count();
                    $data['openthurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Thursday')
                                                ->where('status','open')->count();
                    $data['openfriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Friday')
                                                ->where('status','open')->count();
                    $data['opensatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Saturday')
                                                ->where('status','open')->count();
                    $data['opensunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Sunday')
                                                ->where('status','open')->count();
        
        
                    $data['waitingmonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Monday')
                                                ->where('status','waiting')->count();
                    $data['waitingtuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Tuesday')
                                                ->where('status','waiting')->count();
                    $data['waitingwedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Wednesday')
                                                ->where('status','waiting')->count();
                    $data['waitingthurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Thursday')
                                                ->where('status','waiting')->count();
                    $data['waitingfriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Friday')
                                                ->where('status','waiting')->count();
                    $data['waitingsatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Saturday')
                                                ->where('status','waiting')->count();
                    $data['waitingsunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Sunday')
                                                ->where('status','waiting')->count();
                    
                    $data['visitmonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Monday')
                                                ->where('status','visit')->count();
                    $data['visittuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Tuesday')
                                                ->where('status','visit')->count();
                    $data['visitwedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Wednesday')
                                                ->where('status','visit')->count();
                    $data['visitthurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Thursday')
                                                ->where('status','visit')->count();
                    $data['visitfriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Friday')
                                                ->where('status','visit')->count();
                    $data['visitsatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Saturday')
                                                ->where('status','visit')->count();
                    $data['visitsunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Sunday')
                                                ->where('status','visit')->count();
        
                    $data['revisitmonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Monday')
                                                ->where('status','revisit')->count();
                    $data['revisittuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Tuesday')
                                                ->where('status','revisit')->count();
                    $data['revisitwedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Wednesday')
                                                ->where('status','revisit')->count();
                    $data['revisitthurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Thursday')
                                                ->where('status','revisit')->count();
                    $data['revisitfriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Friday')
                                                ->where('status','revisit')->count();
                    $data['revisitsatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Saturday')
                                                ->where('status','revisit')->count();
                    $data['revisitsunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Sunday')
                                                ->where('status','revisit')->count();
        
        
                    $data['closemonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Monday')
                                                ->where('status','closed')->count();
                    $data['closetuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Tuesday')
                                                ->where('status','closed')->count();
                    $data['closewedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Wednesday')
                                                ->where('status','closed')->count();
                    $data['closethurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Thursday')
                                                ->where('status','closed')->count();
                    $data['closefriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Friday')
                                                ->where('status','closed')->count();
                    $data['closesatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Saturday')
                                                ->where('status','closed')->count();
                    $data['closesunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Sunday')
                                                ->where('status','closed')->count();
        
                    $data['reopenmonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Monday')
                                                ->where('status','reopen')->count();
                    $data['reopentuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Tuesday')
                                                ->where('status','reopen')->count();
                    $data['reopenwedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Wednesday')
                                                ->where('status','reopen')->count();
                    $data['reopenthurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Thursday')
                                                ->where('status','reopen')->count();
                    $data['reopenfriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Friday')
                                                ->where('status','reopen')->count();
                    $data['reopensatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Saturday')
                                                ->where('status','reopen')->count();
                    $data['reopensunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Sunday')
                                                ->where('status','reopen')->count();
        
                    $data['notification'] = DB::table('notifications')->count();
                }
                if($request->from_date && $request->to_date && $request->city && $request->category_id && $request->subcategory_id){
                    $data['ticket'] = Ticket::orderBy('id','desc')->get();
                    $data['open'] = Ticket::where('status','open')->where('city',$request->city)->where('category_id', $request->category_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['waiting'] = Ticket::where('status','waiting')->where('city',$request->city)->where('category_id', $request->category_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['visit'] = Ticket::where('status','visit')->where('city',$request->city)->where('category_id', $request->category_id)->whereDate('visit_date','>=',$request->from_date)->whereDate('visit_date','<=', $request->to_date)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['revisit'] = Ticket::where('status','revisit')->where('city',$request->city)->where('category_id', $request->category_id)->whereDate('visit_date','>=', $request->to_date)->whereDate('visit_date','<=', $request->to_date)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['close'] = Ticket::where('status','closed')->where('city',$request->city)->where('category_id', $request->category_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['reopen'] = Ticket::where('status','reopen')->where('city',$request->city)->where('category_id', $request->category_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['openmonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Monday')
                                                ->where('status','open')
                                                ->count();
                    $data['opentuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Tuesday')
                                                ->where('status','open')
                                                ->count();
                    $data['openwedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Wednesday')
                                                ->where('status','open')->count();
                    $data['openthurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Thursday')
                                                ->where('status','open')->count();
                    $data['openfriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Friday')
                                                ->where('status','open')->count();
                    $data['opensatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Saturday')
                                                ->where('status','open')->count();
                    $data['opensunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Sunday')
                                                ->where('status','open')->count();
        
        
                    $data['waitingmonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Monday')
                                                ->where('status','waiting')->count();
                    $data['waitingtuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Tuesday')
                                                ->where('status','waiting')->count();
                    $data['waitingwedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Wednesday')
                                                ->where('status','waiting')->count();
                    $data['waitingthurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Thursday')
                                                ->where('status','waiting')->count();
                    $data['waitingfriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Friday')
                                                ->where('status','waiting')->count();
                    $data['waitingsatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Saturday')
                                                ->where('status','waiting')->count();
                    $data['waitingsunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Sunday')
                                                ->where('status','waiting')->count();
                    
                    $data['visitmonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('visit_date', '>=', $request->from_date)->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Monday')
                                                ->where('status','visit')->count();
                    $data['visittuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('visit_date', '>=', $request->from_date)->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Tuesday')
                                                ->where('status','visit')->count();
                    $data['visitwedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('visit_date', '>=', $request->from_date)->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Wednesday')
                                                ->where('status','visit')->count();
                    $data['visitthurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('visit_date', '>=', $request->from_date)->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Thursday')
                                                ->where('status','visit')->count();
                    $data['visitfriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('visit_date', '>=', $request->from_date)->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Friday')
                                                ->where('status','visit')->count();
                    $data['visitsatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('visit_date', '>=', $request->from_date)->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Saturday')
                                                ->where('status','visit')->count();
                    $data['visitsunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('visit_date', '>=', $request->from_date)->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Sunday')
                                                ->where('status','visit')->count();
        
                    $data['revisitmonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('visit_date', '>=', $request->from_date)->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Monday')
                                                ->where('status','revisit')->count();
                    $data['revisittuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('visit_date', '>=', $request->from_date)->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Tuesday')
                                                ->where('status','revisit')->count();
                    $data['revisitwedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('visit_date', '>=', $request->from_date)->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Wednesday')
                                                ->where('status','revisit')->count();
                    $data['revisitthurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('visit_date', '>=', $request->from_date)->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Thursday')
                                                ->where('status','revisit')->count();
                    $data['revisitfriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('visit_date', '>=', $request->from_date)->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Friday')
                                                ->where('status','revisit')->count();
                    $data['revisitsatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('visit_date', '>=', $request->from_date)->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Saturday')
                                                ->where('status','revisit')->count();
                    $data['revisitsunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('visit_date', '>=', $request->from_date)->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Sunday')
                                                ->where('status','revisit')->count();
        
        
                    $data['closemonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Monday')
                                                ->where('status','closed')->count();
                    $data['closetuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Tuesday')
                                                ->where('status','closed')->count();
                    $data['closewedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Wednesday')
                                                ->where('status','closed')->count();
                    $data['closethurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Thursday')
                                                ->where('status','closed')->count();
                    $data['closefriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Friday')
                                                ->where('status','closed')->count();
                    $data['closesatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Saturday')
                                                ->where('status','closed')->count();
                    $data['closesunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Sunday')
                                                ->where('status','closed')->count();
        
                    $data['reopenmonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Monday')
                                                ->where('status','reopen')->count();
                    $data['reopentuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Tuesday')
                                                ->where('status','reopen')->count();
                    $data['reopenwedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Wednesday')
                                                ->where('status','reopen')->count();
                    $data['reopenthurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Thursday')
                                                ->where('status','reopen')->count();
                    $data['reopenfriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Friday')
                                                ->where('status','reopen')->count();
                    $data['reopensatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Saturday')
                                                ->where('status','reopen')->count();
                    $data['reopensunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Sunday')
                                                ->where('status','reopen')->count();
        
                    $data['notification'] = DB::table('notifications')->count();
                }
                if($request->from_date && !$request->to_date && !$request->city && !$request->category_id && $request->subcategory_id){
                    $data['ticket'] = Ticket::orderBy('id','desc')->get();
                    $data['open'] = Ticket::where('status','open')->whereDate('update_date', $request->from_date)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['waiting'] = Ticket::where('status','waiting')->whereDate('update_date', $request->from_date)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['visit'] = Ticket::where('status','visit')->whereDate('visit_date',$request->from_date)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['revisit'] = Ticket::where('status','revisit')->whereDate('visit_date', $request->to_date)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['close'] = Ticket::where('status','closed')->whereDate('update_date', $request->from_date)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['reopen'] = Ticket::where('status','reopen')->whereDate('update_date', $request->from_date)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['openmonday'] = where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Monday')
                                                ->where('status','open')
                                                ->count();
                    $data['opentuesday'] = where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Tuesday')
                                                ->where('status','open')
                                                ->count();
                    $data['openwedday'] = where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Wednesday')
                                                ->where('status','open')->count();
                    $data['openthurday'] = where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Thursday')
                                                ->where('status','open')->count();
                    $data['openfriday'] = where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Friday')
                                                ->where('status','open')->count();
                    $data['opensatday'] = where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Saturday')
                                                ->where('status','open')->count();
                    $data['opensunday'] = where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Sunday')
                                                ->where('status','open')->count();
        
        
                    $data['waitingmonday'] = where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Monday')
                                                ->where('status','waiting')->count();
                    $data['waitingtuesday'] = where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Tuesday')
                                                ->where('status','waiting')->count();
                    $data['waitingwedday'] = where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Wednesday')
                                                ->where('status','waiting')->count();
                    $data['waitingthurday'] = where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Thursday')
                                                ->where('status','waiting')->count();
                    $data['waitingfriday'] = where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Friday')
                                                ->where('status','waiting')->count();
                    $data['waitingsatday'] = where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Saturday')
                                                ->where('status','waiting')->count();
                    $data['waitingsunday'] = where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Sunday')
                                                ->where('status','waiting')->count();
                    
                    $data['visitmonday'] = where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Monday')
                                                ->where('status','visit')->count();
                    $data['visittuesday'] = where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Tuesday')
                                                ->where('status','visit')->count();
                    $data['visitwedday'] = where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Wednesday')
                                                ->where('status','visit')->count();
                    $data['visitthurday'] = where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Thursday')
                                                ->where('status','visit')->count();
                    $data['visitfriday'] = where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Friday')
                                                ->where('status','visit')->count();
                    $data['visitsatday'] = where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Saturday')
                                                ->where('status','visit')->count();
                    $data['visitsunday'] = where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Sunday')
                                                ->where('status','visit')->count();
        
                    $data['revisitmonday'] = where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Monday')
                                                ->where('status','revisit')->count();
                    $data['revisittuesday'] = where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Tuesday')
                                                ->where('status','revisit')->count();
                    $data['revisitwedday'] = where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Wednesday')
                                                ->where('status','revisit')->count();
                    $data['revisitthurday'] = where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Thursday')
                                                ->where('status','revisit')->count();
                    $data['revisitfriday'] = where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Friday')
                                                ->where('status','revisit')->count();
                    $data['revisitsatday'] = where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Saturday')
                                                ->where('status','revisit')->count();
                    $data['revisitsunday'] = where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Sunday')
                                                ->where('status','revisit')->count();
        
        
                    $data['closemonday'] = where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Monday')
                                                ->where('status','closed')->count();
                    $data['closetuesday'] = where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Tuesday')
                                                ->where('status','closed')->count();
                    $data['closewedday'] = where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Wednesday')
                                                ->where('status','closed')->count();
                    $data['closethurday'] = where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Thursday')
                                                ->where('status','closed')->count();
                    $data['closefriday'] = where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Friday')
                                                ->where('status','closed')->count();
                    $data['closesatday'] = where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Saturday')
                                                ->where('status','closed')->count();
                    $data['closesunday'] = where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Sunday')
                                                ->where('status','closed')->count();
        
                    $data['reopenmonday'] = where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Monday')
                                                ->where('status','reopen')->count();
                    $data['reopentuesday'] = where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Tuesday')
                                                ->where('status','reopen')->count();
                    $data['reopenwedday'] = where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Wednesday')
                                                ->where('status','reopen')->count();
                    $data['reopenthurday'] = where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Thursday')
                                                ->where('status','reopen')->count();
                    $data['reopenfriday'] = where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Friday')
                                                ->where('status','reopen')->count();
                    $data['reopensatday'] = where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Saturday')
                                                ->where('status','reopen')->count();
                    $data['reopensunday'] = where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Sunday')
                                                ->where('status','reopen')->count();
        
                    $data['notification'] = DB::table('notifications')->count();
                }
                if(!$request->from_date && $request->to_date && !$request->city && !$request->category_id && $request->subcategory_id){
                    $data['ticket'] = Ticket::orderBy('id','desc')->get();
                    $data['open'] = Ticket::where('status','open')->whereDate('update_date', $request->to_date)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['waiting'] = Ticket::where('status','waiting')->whereDate('update_date', $request->to_date)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['visit'] = Ticket::where('status','visit')->whereDate('visit_date',$request->to_date)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['revisit'] = Ticket::where('status','revisit')->whereDate('visit_date', $request->to_date)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['close'] = Ticket::where('status','closed')->whereDate('update_date', $request->to_date)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['reopen'] = Ticket::where('status','reopen')->whereDate('update_date', $request->to_date)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['openmonday'] = Tiwhere('assign_to',$user->id)->cket::where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Monday')
                                                ->where('status','open')
                                                ->count();
                    $data['opentuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Tuesday')
                                                ->where('status','open')
                                                ->count();
                    $data['openwedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Wednesday')
                                                ->where('status','open')->count();
                    $data['openthurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Thursday')
                                                ->where('status','open')->count();
                    $data['openfriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Friday')
                                                ->where('status','open')->count();
                    $data['opensatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Saturday')
                                                ->where('status','open')->count();
                    $data['opensunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Sunday')
                                                ->where('status','open')->count();
        
        
                    $data['waitingmonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Monday')
                                                ->where('status','waiting')->count();
                    $data['waitingtuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Tuesday')
                                                ->where('status','waiting')->count();
                    $data['waitingwedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Wednesday')
                                                ->where('status','waiting')->count();
                    $data['waitingthurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Thursday')
                                                ->where('status','waiting')->count();
                    $data['waitingfriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Friday')
                                                ->where('status','waiting')->count();
                    $data['waitingsatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Saturday')
                                                ->where('status','waiting')->count();
                    $data['waitingsunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Sunday')
                                                ->where('status','waiting')->count();
                    
                    $data['visitmonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Monday')
                                                ->where('status','visit')->count();
                    $data['visittuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Tuesday')
                                                ->where('status','visit')->count();
                    $data['visitwedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Wednesday')
                                                ->where('status','visit')->count();
                    $data['visitthurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Thursday')
                                                ->where('status','visit')->count();
                    $data['visitfriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Friday')
                                                ->where('status','visit')->count();
                    $data['visitsatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Saturday')
                                                ->where('status','visit')->count();
                    $data['visitsunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Sunday')
                                                ->where('status','visit')->count();
        
                    $data['revisitmonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Monday')
                                                ->where('status','revisit')->count();
                    $data['revisittuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Tuesday')
                                                ->where('status','revisit')->count();
                    $data['revisitwedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Wednesday')
                                                ->where('status','revisit')->count();
                    $data['revisitthurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Thursday')
                                                ->where('status','revisit')->count();
                    $data['revisitfriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Friday')
                                                ->where('status','revisit')->count();
                    $data['revisitsatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Saturday')
                                                ->where('status','revisit')->count();
                    $data['revisitsunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Sunday')
                                                ->where('status','revisit')->count();
        
        
                    $data['closemonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Monday')
                                                ->where('status','closed')->count();
                    $data['closetuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Tuesday')
                                                ->where('status','closed')->count();
                    $data['closewedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Wednesday')
                                                ->where('status','closed')->count();
                    $data['closethurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Thursday')
                                                ->where('status','closed')->count();
                    $data['closefriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Friday')
                                                ->where('status','closed')->count();
                    $data['closesatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Saturday')
                                                ->where('status','closed')->count();
                    $data['closesunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Sunday')
                                                ->where('status','closed')->count();
        
                    $data['reopenmonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Monday')
                                                ->where('status','reopen')->count();
                    $data['reopentuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Tuesday')
                                                ->where('status','reopen')->count();
                    $data['reopenwedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Wednesday')
                                                ->where('status','reopen')->count();
                    $data['reopenthurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Thursday')
                                                ->where('status','reopen')->count();
                    $data['reopenfriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Friday')
                                                ->where('status','reopen')->count();
                    $data['reopensatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Saturday')
                                                ->where('status','reopen')->count();
                    $data['reopensunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Sunday')
                                                ->where('status','reopen')->count();
        
                    $data['notification'] = DB::table('notifications')->count();
                }
                if($request->from_date && !$request->to_date && !$request->city && $request->category_id && !$request->subcategory_id){
                    $data['ticket'] = Ticket::orderBy('id','desc')->get();
                    $data['open'] = Ticket::where('status','open')->whereDate('update_date', $request->from_date)->where('category_id', $request->category_id)->count();
                    $data['waiting'] = Ticket::where('status','waiting')->whereDate('update_date', $request->from_date)->where('category_id', $request->category_id)->count();
                    $data['visit'] = Ticket::where('status','visit')->whereDate('visit_date',$request->from_date)->where('category_id', $request->category_id)->count();
                    $data['revisit'] = Ticket::where('status','revisit')->whereDate('visit_date', $request->to_date)->where('category_id', $request->category_id)->count();
                    $data['close'] = Ticket::where('status','closed')->whereDate('update_date', $request->from_date)->where('category_id', $request->category_id)->count();
                    $data['reopen'] = Ticket::where('status','reopen')->whereDate('update_date', $request->from_date)->where('category_id', $request->category_id)->count();
                    $data['openmonday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Monday')
                                                ->where('status','open')
                                                ->count();
                    $data['opentuesday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Tuesday')
                                                ->where('status','open')
                                                ->count();
                    $data['openwedday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Wednesday')
                                                ->where('status','open')->count();
                    $data['openthurday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Thursday')
                                                ->where('status','open')->count();
                    $data['openfriday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Friday')
                                                ->where('status','open')->count();
                    $data['opensatday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Saturday')
                                                ->where('status','open')->count();
                    $data['opensunday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Sunday')
                                                ->where('status','open')->count();
        
        
                    $data['waitingmonday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Monday')
                                                ->where('status','waiting')->count();
                    $data['waitingtuesday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Tuesday')
                                                ->where('status','waiting')->count();
                    $data['waitingwedday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Wednesday')
                                                ->where('status','waiting')->count();
                    $data['waitingthurday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Thursday')
                                                ->where('status','waiting')->count();
                    $data['waitingfriday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Friday')
                                                ->where('status','waiting')->count();
                    $data['waitingsatday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Saturday')
                                                ->where('status','waiting')->count();
                    $data['waitingsunday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Sunday')
                                                ->where('status','waiting')->count();
                    
                    $data['visitmonday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Monday')
                                                ->where('status','visit')->count();
                    $data['visittuesday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Tuesday')
                                                ->where('status','visit')->count();
                    $data['visitwedday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Wednesday')
                                                ->where('status','visit')->count();
                    $data['visitthurday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Thursday')
                                                ->where('status','visit')->count();
                    $data['visitfriday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Friday')
                                                ->where('status','visit')->count();
                    $data['visitsatday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Saturday')
                                                ->where('status','visit')->count();
                    $data['visitsunday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Sunday')
                                                ->where('status','visit')->count();
        
                    $data['revisitmonday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Monday')
                                                ->where('status','revisit')->count();
                    $data['revisittuesday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Tuesday')
                                                ->where('status','revisit')->count();
                    $data['revisitwedday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Wednesday')
                                                ->where('status','revisit')->count();
                    $data['revisitthurday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Thursday')
                                                ->where('status','revisit')->count();
                    $data['revisitfriday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Friday')
                                                ->where('status','revisit')->count();
                    $data['revisitsatday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Saturday')
                                                ->where('status','revisit')->count();
                    $data['revisitsunday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Sunday')
                                                ->where('status','revisit')->count();
        
        
                    $data['closemonday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Monday')
                                                ->where('status','closed')->count();
                    $data['closetuesday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Tuesday')
                                                ->where('status','closed')->count();
                    $data['closewedday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Wednesday')
                                                ->where('status','closed')->count();
                    $data['closethurday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Thursday')
                                                ->where('status','closed')->count();
                    $data['closefriday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Friday')
                                                ->where('status','closed')->count();
                    $data['closesatday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Saturday')
                                                ->where('status','closed')->count();
                    $data['closesunday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Sunday')
                                                ->where('status','closed')->count();
        
                    $data['reopenmonday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Monday')
                                                ->where('status','reopen')->count();
                    $data['reopentuesday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Tuesday')
                                                ->where('status','reopen')->count();
                    $data['reopenwedday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Wednesday')
                                                ->where('status','reopen')->count();
                    $data['reopenthurday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Thursday')
                                                ->where('status','reopen')->count();
                    $data['reopenfriday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Friday')
                                                ->where('status','reopen')->count();
                    $data['reopensatday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Saturday')
                                                ->where('status','reopen')->count();
                    $data['reopensunday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Sunday')
                                                ->where('status','reopen')->count();
        
                    $data['notification'] = DB::table('notifications')->count();
                }
                if(!$request->from_date && $request->to_date && !$request->city && $request->category_id && !$request->subcategory_id){
                    $data['ticket'] = Ticket::orderBy('id','desc')->get();
                    $data['open'] = Ticket::where('status','open')->whereDate('update_date', $request->to_date)->where('category_id', $request->category_id)->count();
                    $data['waiting'] = Ticket::where('status','waiting')->whereDate('update_date', $request->to_date)->where('category_id', $request->category_id)->count();
                    $data['visit'] = Ticket::where('status','visit')->whereDate('visit_date',$request->to_date)->where('category_id', $request->category_id)->count();
                    $data['revisit'] = Ticket::where('status','revisit')->whereDate('visit_date', $request->to_date)->where('category_id', $request->category_id)->count();
                    $data['close'] = Ticket::where('status','closed')->whereDate('update_date', $request->to_date)->where('category_id', $request->category_id)->count();
                    $data['reopen'] = Ticket::where('status','reopen')->whereDate('update_date', $request->to_date)->where('category_id', $request->category_id)->count();
                    $data['openmonday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Monday')
                                                ->where('status','open')
                                                ->count();
                    $data['opentuesday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Tuesday')
                                                ->where('status','open')
                                                ->count();
                    $data['openwedday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Wednesday')
                                                ->where('status','open')->count();
                    $data['openthurday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Thursday')
                                                ->where('status','open')->count();
                    $data['openfriday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Friday')
                                                ->where('status','open')->count();
                    $data['opensatday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Saturday')
                                                ->where('status','open')->count();
                    $data['opensunday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Sunday')
                                                ->where('status','open')->count();
        
        
                    $data['waitingmonday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Monday')
                                                ->where('status','waiting')->count();
                    $data['waitingtuesday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Tuesday')
                                                ->where('status','waiting')->count();
                    $data['waitingwedday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Wednesday')
                                                ->where('status','waiting')->count();
                    $data['waitingthurday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Thursday')
                                                ->where('status','waiting')->count();
                    $data['waitingfriday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Friday')
                                                ->where('status','waiting')->count();
                    $data['waitingsatday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Saturday')
                                                ->where('status','waiting')->count();
                    $data['waitingsunday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Sunday')
                                                ->where('status','waiting')->count();
                    
                    $data['visitmonday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Monday')
                                                ->where('status','visit')->count();
                    $data['visittuesday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Tuesday')
                                                ->where('status','visit')->count();
                    $data['visitwedday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Wednesday')
                                                ->where('status','visit')->count();
                    $data['visitthurday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Thursday')
                                                ->where('status','visit')->count();
                    $data['visitfriday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Friday')
                                                ->where('status','visit')->count();
                    $data['visitsatday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Saturday')
                                                ->where('status','visit')->count();
                    $data['visitsunday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Sunday')
                                                ->where('status','visit')->count();
        
                    $data['revisitmonday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Monday')
                                                ->where('status','revisit')->count();
                    $data['revisittuesday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Tuesday')
                                                ->where('status','revisit')->count();
                    $data['revisitwedday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Wednesday')
                                                ->where('status','revisit')->count();
                    $data['revisitthurday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Thursday')
                                                ->where('status','revisit')->count();
                    $data['revisitfriday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Friday')
                                                ->where('status','revisit')->count();
                    $data['revisitsatday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Saturday')
                                                ->where('status','revisit')->count();
                    $data['revisitsunday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Sunday')
                                                ->where('status','revisit')->count();
        
        
                    $data['closemonday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Monday')
                                                ->where('status','closed')->count();
                    $data['closetuesday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Tuesday')
                                                ->where('status','closed')->count();
                    $data['closewedday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Wednesday')
                                                ->where('status','closed')->count();
                    $data['closethurday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Thursday')
                                                ->where('status','closed')->count();
                    $data['closefriday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Friday')
                                                ->where('status','closed')->count();
                    $data['closesatday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Saturday')
                                                ->where('status','closed')->count();
                    $data['closesunday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Sunday')
                                                ->where('status','closed')->count();
        
                    $data['reopenmonday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Monday')
                                                ->where('status','reopen')->count();
                    $data['reopentuesday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Tuesday')
                                                ->where('status','reopen')->count();
                    $data['reopenwedday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Wednesday')
                                                ->where('status','reopen')->count();
                    $data['reopenthurday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Thursday')
                                                ->where('status','reopen')->count();
                    $data['reopenfriday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Friday')
                                                ->where('status','reopen')->count();
                    $data['reopensatday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Saturday')
                                                ->where('status','reopen')->count();
                    $data['reopensunday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Sunday')
                                                ->where('status','reopen')->count();
        
                    $data['notification'] = DB::table('notifications')->count();
                }
                if($request->from_date && $request->to_date && !$request->city && $request->category_id && !$request->subcategory_id){
                    $data['ticket'] = Ticket::orderBy('id','desc')->get();
                    $data['open'] = Ticket::where('status','open')->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)->where('category_id', $request->category_id)->count();
                    $data['waiting'] = Ticket::where('status','waiting')->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)->where('category_id', $request->category_id)->count();
                    $data['visit'] = Ticket::where('status','visit')->whereDate('visit_date','>=', $request->from_date)->whereDate('visit_date','<=', $request->to_date)->where('category_id', $request->category_id)->count();
                    $data['revisit'] = Ticket::where('status','revisit')->whereDate('visit_date','>=', $request->from_date)->whereDate('visit_date','<=', $request->to_date)->where('category_id', $request->category_id)->count();
                    $data['close'] = Ticket::where('status','closed')->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)->where('category_id', $request->category_id)->count();
                    $data['reopen'] = Ticket::where('status','reopen')->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)->where('category_id', $request->category_id)->count();
                    $data['openmonday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Monday')
                                                ->where('status','open')
                                                ->count();
                    $data['opentuesday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Tuesday')
                                                ->where('status','open')
                                                ->count();
                    $data['openwedday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Wednesday')
                                                ->where('status','open')->count();
                    $data['openthurday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Thursday')
                                                ->where('status','open')->count();
                    $data['openfriday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Friday')
                                                ->where('status','open')->count();
                    $data['opensatday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Saturday')
                                                ->where('status','open')->count();
                    $data['opensunday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Sunday')
                                                ->where('status','open')->count();
        
        
                    $data['waitingmonday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Monday')
                                                ->where('status','waiting')->count();
                    $data['waitingtuesday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Tuesday')
                                                ->where('status','waiting')->count();
                    $data['waitingwedday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Wednesday')
                                                ->where('status','waiting')->count();
                    $data['waitingthurday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Thursday')
                                                ->where('status','waiting')->count();
                    $data['waitingfriday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Friday')
                                                ->where('status','waiting')->count();
                    $data['waitingsatday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Saturday')
                                                ->where('status','waiting')->count();
                    $data['waitingsunday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Sunday')
                                                ->where('status','waiting')->count();
                    
                    $data['visitmonday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('visit_date','>=', $request->from_date)->whereDate('visit_date','<=', $request->to_date)
                                                ->where('visit_day','Monday')
                                                ->where('status','visit')->count();
                    $data['visittuesday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('visit_date','>=', $request->from_date)->whereDate('visit_date','<=', $request->to_date)
                                                ->where('visit_day','Tuesday')
                                                ->where('status','visit')->count();
                    $data['visitwedday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('visit_date','>=', $request->from_date)->whereDate('visit_date','<=', $request->to_date)
                                                ->where('visit_day','Wednesday')
                                                ->where('status','visit')->count();
                    $data['visitthurday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('visit_date','>=', $request->from_date)->whereDate('visit_date','<=', $request->to_date)
                                                ->where('visit_day','Thursday')
                                                ->where('status','visit')->count();
                    $data['visitfriday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('visit_date','>=', $request->from_date)->whereDate('visit_date','<=', $request->to_date)
                                                ->where('visit_day','Friday')
                                                ->where('status','visit')->count();
                    $data['visitsatday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('visit_date','>=', $request->from_date)->whereDate('visit_date','<=', $request->to_date)
                                                ->where('visit_day','Saturday')
                                                ->where('status','visit')->count();
                    $data['visitsunday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('visit_date','>=', $request->from_date)->whereDate('visit_date','<=', $request->to_date)
                                                ->where('visit_day','Sunday')
                                                ->where('status','visit')->count();
        
                    $data['revisitmonday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('visit_date','>=', $request->from_date)->whereDate('visit_date','<=', $request->to_date)
                                                ->where('visit_day','Monday')
                                                ->where('status','revisit')->count();
                    $data['revisittuesday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('visit_date','>=', $request->from_date)->whereDate('visit_date','<=', $request->to_date)
                                                ->where('visit_day','Tuesday')
                                                ->where('status','revisit')->count();
                    $data['revisitwedday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('visit_date','>=', $request->from_date)->whereDate('visit_date','<=', $request->to_date)
                                                ->where('visit_day','Wednesday')
                                                ->where('status','revisit')->count();
                    $data['revisitthurday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('visit_date','>=', $request->from_date)->whereDate('visit_date','<=', $request->to_date)
                                                ->where('visit_day','Thursday')
                                                ->where('status','revisit')->count();
                    $data['revisitfriday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('visit_date','>=', $request->from_date)->whereDate('visit_date','<=', $request->to_date)
                                                ->where('visit_day','Friday')
                                                ->where('status','revisit')->count();
                    $data['revisitsatday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('visit_date','>=', $request->from_date)->whereDate('visit_date','<=', $request->to_date)
                                                ->where('visit_day','Saturday')
                                                ->where('status','revisit')->count();
                    $data['revisitsunday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('visit_date','>=', $request->from_date)->whereDate('visit_date','<=', $request->to_date)
                                                ->where('visit_day','Sunday')
                                                ->where('status','revisit')->count();
        
        
                    $data['closemonday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Monday')
                                                ->where('status','closed')->count();
                    $data['closetuesday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Tuesday')
                                                ->where('status','closed')->count();
                    $data['closewedday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Wednesday')
                                                ->where('status','closed')->count();
                    $data['closethurday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Thursday')
                                                ->where('status','closed')->count();
                    $data['closefriday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Friday')
                                                ->where('status','closed')->count();
                    $data['closesatday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Saturday')
                                                ->where('status','closed')->count();
                    $data['closesunday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Sunday')
                                                ->where('status','closed')->count();
        
                    $data['reopenmonday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Monday')
                                                ->where('status','reopen')->count();
                    $data['reopentuesday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Tuesday')
                                                ->where('status','reopen')->count();
                    $data['reopenwedday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Wednesday')
                                                ->where('status','reopen')->count();
                    $data['reopenthurday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Thursday')
                                                ->where('status','reopen')->count();
                    $data['reopenfriday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Friday')
                                                ->where('status','reopen')->count();
                    $data['reopensatday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Saturday')
                                                ->where('status','reopen')->count();
                    $data['reopensunday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Sunday')
                                                ->where('status','reopen')->count();
        
                    $data['notification'] = DB::table('notifications')->count();
                }
                if(!$request->from_date && $request->to_date && !$request->city && $request->category_id && $request->subcategory_id){
                    $data['ticket'] = Ticket::orderBy('id','desc')->get();
                    $data['open'] = Ticket::where('status','open')->whereDate('update_date', $request->to_date)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['waiting'] = Ticket::where('status','waiting')->whereDate('update_date', $request->to_date)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['visit'] = Ticket::where('status','visit')->whereDate('visit_date',$request->to_date)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['revisit'] = Ticket::where('status','revisit')->whereDate('visit_date', $request->to_date)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['close'] = Ticket::where('status','closed')->whereDate('update_date', $request->to_date)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['reopen'] = Ticket::where('status','reopen')->whereDate('update_date', $request->to_date)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['openmonday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Monday')
                                                ->where('status','open')
                                                ->count();
                    $data['opentuesday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Tuesday')
                                                ->where('status','open')
                                                ->count();
                    $data['openwedday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Wednesday')
                                                ->where('status','open')->count();
                    $data['openthurday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Thursday')
                                                ->where('status','open')->count();
                    $data['openfriday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Friday')
                                                ->where('status','open')->count();
                    $data['opensatday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Saturday')
                                                ->where('status','open')->count();
                    $data['opensunday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Sunday')
                                                ->where('status','open')->count();
        
        
                    $data['waitingmonday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Monday')
                                                ->where('status','waiting')->count();
                    $data['waitingtuesday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Tuesday')
                                                ->where('status','waiting')->count();
                    $data['waitingwedday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Wednesday')
                                                ->where('status','waiting')->count();
                    $data['waitingthurday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Thursday')
                                                ->where('status','waiting')->count();
                    $data['waitingfriday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Friday')
                                                ->where('status','waiting')->count();
                    $data['waitingsatday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Saturday')
                                                ->where('status','waiting')->count();
                    $data['waitingsunday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Sunday')
                                                ->where('status','waiting')->count();
                    
                    $data['visitmonday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Monday')
                                                ->where('status','visit')->count();
                    $data['visittuesday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Tuesday')
                                                ->where('status','visit')->count();
                    $data['visitwedday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Wednesday')
                                                ->where('status','visit')->count();
                    $data['visitthurday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Thursday')
                                                ->where('status','visit')->count();
                    $data['visitfriday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Friday')
                                                ->where('status','visit')->count();
                    $data['visitsatday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Saturday')
                                                ->where('status','visit')->count();
                    $data['visitsunday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Sunday')
                                                ->where('status','visit')->count();
        
                    $data['revisitmonday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Monday')
                                                ->where('status','revisit')->count();
                    $data['revisittuesday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Tuesday')
                                                ->where('status','revisit')->count();
                    $data['revisitwedday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Wednesday')
                                                ->where('status','revisit')->count();
                    $data['revisitthurday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Thursday')
                                                ->where('status','revisit')->count();
                    $data['revisitfriday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Friday')
                                                ->where('status','revisit')->count();
                    $data['revisitsatday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Saturday')
                                                ->where('status','revisit')->count();
                    $data['revisitsunday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->to_date)
                                                ->where('visit_day','Sunday')
                                                ->where('status','revisit')->count();
        
        
                    $data['closemonday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Monday')
                                                ->where('status','closed')->count();
                    $data['closetuesday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Tuesday')
                                                ->where('status','closed')->count();
                    $data['closewedday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Wednesday')
                                                ->where('status','closed')->count();
                    $data['closethurday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Thursday')
                                                ->where('status','closed')->count();
                    $data['closefriday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Friday')
                                                ->where('status','closed')->count();
                    $data['closesatday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Saturday')
                                                ->where('status','closed')->count();
                    $data['closesunday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Sunday')
                                                ->where('status','closed')->count();
        
                    $data['reopenmonday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Monday')
                                                ->where('status','reopen')->count();
                    $data['reopentuesday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Tuesday')
                                                ->where('status','reopen')->count();
                    $data['reopenwedday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Wednesday')
                                                ->where('status','reopen')->count();
                    $data['reopenthurday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Thursday')
                                                ->where('status','reopen')->count();
                    $data['reopenfriday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Friday')
                                                ->where('status','reopen')->count();
                    $data['reopensatday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Saturday')
                                                ->where('status','reopen')->count();
                    $data['reopensunday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)
                                                ->where('update_day','Sunday')
                                                ->where('status','reopen')->count();
        
                    $data['notification'] = DB::table('notifications')->count();
                }
                if($request->from_date && !$request->to_date && !$request->city && $request->category_id && $request->subcategory_id){
                    $data['ticket'] = Ticket::orderBy('id','desc')->get();
                    $data['open'] = Ticket::where('status','open')->whereDate('update_date', $request->from_date)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['waiting'] = Ticket::where('status','waiting')->whereDate('update_date', $request->from_date)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['visit'] = Ticket::where('status','visit')->whereDate('visit_date',$request->from_date)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['revisit'] = Ticket::where('status','revisit')->whereDate('visit_date', $request->from_date)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['close'] = Ticket::where('status','closed')->whereDate('update_date', $request->from_date)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['reopen'] = Ticket::where('status','reopen')->whereDate('update_date', $request->from_date)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['openmonday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Monday')
                                                ->where('status','open')
                                                ->count();
                    $data['opentuesday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Tuesday')
                                                ->where('status','open')
                                                ->count();
                    $data['openwedday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Wednesday')
                                                ->where('status','open')->count();
                    $data['openthurday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Thursday')
                                                ->where('status','open')->count();
                    $data['openfriday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Friday')
                                                ->where('status','open')->count();
                    $data['opensatday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Saturday')
                                                ->where('status','open')->count();
                    $data['opensunday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Sunday')
                                                ->where('status','open')->count();
        
        
                    $data['waitingmonday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Monday')
                                                ->where('status','waiting')->count();
                    $data['waitingtuesday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Tuesday')
                                                ->where('status','waiting')->count();
                    $data['waitingwedday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Wednesday')
                                                ->where('status','waiting')->count();
                    $data['waitingthurday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Thursday')
                                                ->where('status','waiting')->count();
                    $data['waitingfriday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Friday')
                                                ->where('status','waiting')->count();
                    $data['waitingsatday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Saturday')
                                                ->where('status','waiting')->count();
                    $data['waitingsunday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Sunday')
                                                ->where('status','waiting')->count();
                    
                    $data['visitmonday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Monday')
                                                ->where('status','visit')->count();
                    $data['visittuesday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Tuesday')
                                                ->where('status','visit')->count();
                    $data['visitwedday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Wednesday')
                                                ->where('status','visit')->count();
                    $data['visitthurday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Thursday')
                                                ->where('status','visit')->count();
                    $data['visitfriday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Friday')
                                                ->where('status','visit')->count();
                    $data['visitsatday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Saturday')
                                                ->where('status','visit')->count();
                    $data['visitsunday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Sunday')
                                                ->where('status','visit')->count();
        
                    $data['revisitmonday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Monday')
                                                ->where('status','revisit')->count();
                    $data['revisittuesday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Tuesday')
                                                ->where('status','revisit')->count();
                    $data['revisitwedday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Wednesday')
                                                ->where('status','revisit')->count();
                    $data['revisitthurday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Thursday')
                                                ->where('status','revisit')->count();
                    $data['revisitfriday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Friday')
                                                ->where('status','revisit')->count();
                    $data['revisitsatday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Saturday')
                                                ->where('status','revisit')->count();
                    $data['revisitsunday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->from_date)
                                                ->where('visit_day','Sunday')
                                                ->where('status','revisit')->count();
        
        
                    $data['closemonday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Monday')
                                                ->where('status','closed')->count();
                    $data['closetuesday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Tuesday')
                                                ->where('status','closed')->count();
                    $data['closewedday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Wednesday')
                                                ->where('status','closed')->count();
                    $data['closethurday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Thursday')
                                                ->where('status','closed')->count();
                    $data['closefriday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Friday')
                                                ->where('status','closed')->count();
                    $data['closesatday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Saturday')
                                                ->where('status','closed')->count();
                    $data['closesunday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Sunday')
                                                ->where('status','closed')->count();
        
                    $data['reopenmonday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Monday')
                                                ->where('status','reopen')->count();
                    $data['reopentuesday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Tuesday')
                                                ->where('status','reopen')->count();
                    $data['reopenwedday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Wednesday')
                                                ->where('status','reopen')->count();
                    $data['reopenthurday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Thursday')
                                                ->where('status','reopen')->count();
                    $data['reopenfriday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Friday')
                                                ->where('status','reopen')->count();
                    $data['reopensatday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Saturday')
                                                ->where('status','reopen')->count();
                    $data['reopensunday'] = where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)
                                                ->where('update_day','Sunday')
                                                ->where('status','reopen')->count();
        
                    $data['notification'] = DB::table('notifications')->count();
                }
                if(!$request->from_date && !$request->to_date && $request->city && $request->category_id && !$request->subcategory_id){
                    $data['ticket'] = Ticket::orderBy('id','desc')->get();
                    $data['open'] = Ticket::where('status','open')->where('city',$request->city)->where('category_id', $request->category_id)->count();
                    $data['waiting'] = Ticket::where('status','waiting')->where('city',$request->city)->where('category_id', $request->category_id)->count();
                    $data['visit'] = Ticket::where('status','visit')->where('city',$request->city)->where('category_id', $request->category_id)->count();
                    $data['revisit'] = Ticket::where('status','revisit')->where('city',$request->city)->where('category_id', $request->category_id)->count();
                    $data['close'] = Ticket::where('status','closed')->where('city',$request->city)->where('category_id', $request->category_id)->count();
                    $data['reopen'] = Ticket::where('status','reopen')->where('city',$request->city)->where('category_id', $request->category_id)->count();
                    $data['openmonday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('update_day','Monday')
                                                ->where('status','open')
                                                ->count();
                    $data['opentuesday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('update_day','Tuesday')
                                                ->where('status','open')
                                                ->count();
                    $data['openwedday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('update_day','Wednesday')
                                                ->where('status','open')->count();
                    $data['openthurday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('update_day','Thursday')
                                                ->where('status','open')->count();
                    $data['openfriday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('update_day','Friday')
                                                ->where('status','open')->count();
                    $data['opensatday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('update_day','Saturday')
                                                ->where('status','open')->count();
                    $data['opensunday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('update_day','Sunday')
                                                ->where('status','open')->count();
        
        
                    $data['waitingmonday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('update_day','Monday')
                                                ->where('status','waiting')->count();
                    $data['waitingtuesday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('update_day','Tuesday')
                                                ->where('status','waiting')->count();
                    $data['waitingwedday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('update_day','Wednesday')
                                                ->where('status','waiting')->count();
                    $data['waitingthurday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('update_day','Thursday')
                                                ->where('status','waiting')->count();
                    $data['waitingfriday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('update_day','Friday')
                                                ->where('status','waiting')->count();
                    $data['waitingsatday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('update_day','Saturday')
                                                ->where('status','waiting')->count();
                    $data['waitingsunday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('update_day','Sunday')
                                                ->where('status','waiting')->count();
                    
                    $data['visitmonday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('visit_day','Monday')
                                                ->where('status','visit')->count();
                    $data['visittuesday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('visit_day','Tuesday')
                                                ->where('status','visit')->count();
                    $data['visitwedday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('visit_day','Wednesday')
                                                ->where('status','visit')->count();
                    $data['visitthurday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('visit_day','Thursday')
                                                ->where('status','visit')->count();
                    $data['visitfriday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('visit_day','Friday')
                                                ->where('status','visit')->count();
                    $data['visitsatday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('visit_day','Saturday')
                                                ->where('status','visit')->count();
                    $data['visitsunday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('visit_day','Sunday')
                                                ->where('status','visit')->count();
        
                    $data['revisitmonday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('visit_day','Monday')
                                                ->where('status','revisit')->count();
                    $data['revisittuesday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('visit_day','Tuesday')
                                                ->where('status','revisit')->count();
                    $data['revisitwedday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('visit_day','Wednesday')
                                                ->where('status','revisit')->count();
                    $data['revisitthurday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('visit_day','Thursday')
                                                ->where('status','revisit')->count();
                    $data['revisitfriday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('visit_day','Friday')
                                                ->where('status','revisit')->count();
                    $data['revisitsatday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('visit_day','Saturday')
                                                ->where('status','revisit')->count();
                    $data['revisitsunday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('visit_day','Sunday')
                                                ->where('status','revisit')->count();
        
        
                    $data['closemonday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('update_day','Monday')
                                                ->where('status','closed')->count();
                    $data['closetuesday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('update_day','Tuesday')
                                                ->where('status','closed')->count();
                    $data['closewedday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('update_day','Wednesday')
                                                ->where('status','closed')->count();
                    $data['closethurday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('update_day','Thursday')
                                                ->where('status','closed')->count();
                    $data['closefriday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('update_day','Friday')
                                                ->where('status','closed')->count();
                    $data['closesatday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('update_day','Saturday')
                                                ->where('status','closed')->count();
                    $data['closesunday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('update_day','Sunday')
                                                ->where('status','closed')->count();
        
                    $data['reopenmonday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('update_day','Monday')
                                                ->where('status','reopen')->count();
                    $data['reopentuesday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('update_day','Tuesday')
                                                ->where('status','reopen')->count();
                    $data['reopenwedday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('update_day','Wednesday')
                                                ->where('status','reopen')->count();
                    $data['reopenthurday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('update_day','Thursday')
                                                ->where('status','reopen')->count();
                    $data['reopenfriday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('update_day','Friday')
                                                ->where('status','reopen')->count();
                    $data['reopensatday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('update_day','Saturday')
                                                ->where('status','reopen')->count();
                    $data['reopensunday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)
                                                ->where('update_day','Sunday')
                                                ->where('status','reopen')->count();
        
                    $data['notification'] = DB::table('notifications')->count();
                }
                if(!$request->from_date && !$request->to_date && $request->city && !$request->category_id && $request->subcategory_id){
                    $data['ticket'] = Ticket::orderBy('id','desc')->get();
                    $data['open'] = Ticket::where('status','open')->where('city',$request->city)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['waiting'] = Ticket::where('status','waiting')->where('city',$request->city)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['visit'] = Ticket::where('status','visit')->where('city',$request->city)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['revisit'] = Ticket::where('status','revisit')->where('city',$request->city)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['close'] = Ticket::where('status','closed')->where('city',$request->city)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['reopen'] = Ticket::where('status','reopen')->where('city',$request->city)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['openmonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)
                                                ->where('update_day','Monday')
                                                ->where('status','open')
                                                ->count();
                    $data['opentuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)
                                                ->where('update_day','Tuesday')
                                                ->where('status','open')
                                                ->count();
                    $data['openwedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)
                                                ->where('update_day','Wednesday')
                                                ->where('status','open')->count();
                    $data['openthurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)
                                                ->where('update_day','Thursday')
                                                ->where('status','open')->count();
                    $data['openfriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)
                                                ->where('update_day','Friday')
                                                ->where('status','open')->count();
                    $data['opensatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)
                                                ->where('update_day','Saturday')
                                                ->where('status','open')->count();
                    $data['opensunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)
                                                ->where('update_day','Sunday')
                                                ->where('status','open')->count();
        
        
                    $data['waitingmonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)
                                                ->where('update_day','Monday')
                                                ->where('status','waiting')->count();
                    $data['waitingtuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)
                                                ->where('update_day','Tuesday')
                                                ->where('status','waiting')->count();
                    $data['waitingwedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)
                                                ->where('update_day','Wednesday')
                                                ->where('status','waiting')->count();
                    $data['waitingthurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)
                                                ->where('update_day','Thursday')
                                                ->where('status','waiting')->count();
                    $data['waitingfriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)
                                                ->where('update_day','Friday')
                                                ->where('status','waiting')->count();
                    $data['waitingsatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)
                                                ->where('update_day','Saturday')
                                                ->where('status','waiting')->count();
                    $data['waitingsunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)
                                                ->where('update_day','Sunday')
                                                ->where('status','waiting')->count();
                    
                    $data['visitmonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)
                                                ->where('visit_day','Monday')
                                                ->where('status','visit')->count();
                    $data['visittuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)
                                                ->where('visit_day','Tuesday')
                                                ->where('status','visit')->count();
                    $data['visitwedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)
                                                ->where('visit_day','Wednesday')
                                                ->where('status','visit')->count();
                    $data['visitthurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)
                                                ->where('visit_day','Thursday')
                                                ->where('status','visit')->count();
                    $data['visitfriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)
                                                ->where('visit_day','Friday')
                                                ->where('status','visit')->count();
                    $data['visitsatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)
                                                ->where('visit_day','Saturday')
                                                ->where('status','visit')->count();
                    $data['visitsunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)
                                                ->where('visit_day','Sunday')
                                                ->where('status','visit')->count();
        
                    $data['revisitmonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)
                                                ->where('visit_day','Monday')
                                                ->where('status','revisit')->count();
                    $data['revisittuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)
                                                ->where('visit_day','Tuesday')
                                                ->where('status','revisit')->count();
                    $data['revisitwedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)
                                                ->where('visit_day','Wednesday')
                                                ->where('status','revisit')->count();
                    $data['revisitthurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)
                                                ->where('visit_day','Thursday')
                                                ->where('status','revisit')->count();
                    $data['revisitfriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)
                                                ->where('visit_day','Friday')
                                                ->where('status','revisit')->count();
                    $data['revisitsatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)
                                                ->where('visit_day','Saturday')
                                                ->where('status','revisit')->count();
                    $data['revisitsunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)
                                                ->where('visit_day','Sunday')
                                                ->where('status','revisit')->count();
        
        
                    $data['closemonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)
                                                ->where('update_day','Monday')
                                                ->where('status','closed')->count();
                    $data['closetuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)
                                                ->where('update_day','Tuesday')
                                                ->where('status','closed')->count();
                    $data['closewedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)
                                                ->where('update_day','Wednesday')
                                                ->where('status','closed')->count();
                    $data['closethurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)
                                                ->where('update_day','Thursday')
                                                ->where('status','closed')->count();
                    $data['closefriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)
                                                ->where('update_day','Friday')
                                                ->where('status','closed')->count();
                    $data['closesatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)
                                                ->where('update_day','Saturday')
                                                ->where('status','closed')->count();
                    $data['closesunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)
                                                ->where('update_day','Sunday')
                                                ->where('status','closed')->count();
        
                    $data['reopenmonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)
                                                ->where('update_day','Monday')
                                                ->where('status','reopen')->count();
                    $data['reopentuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)
                                                ->where('update_day','Tuesday')
                                                ->where('status','reopen')->count();
                    $data['reopenwedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)
                                                ->where('update_day','Wednesday')
                                                ->where('status','reopen')->count();
                    $data['reopenthurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)
                                                ->where('update_day','Thursday')
                                                ->where('status','reopen')->count();
                    $data['reopenfriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)
                                                ->where('update_day','Friday')
                                                ->where('status','reopen')->count();
                    $data['reopensatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)
                                                ->where('update_day','Saturday')
                                                ->where('status','reopen')->count();
                    $data['reopensunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)
                                                ->where('update_day','Sunday')
                                                ->where('status','reopen')->count();
        
                    $data['notification'] = DB::table('notifications')->count();
                }
                if($request->from_date && $request->to_date && !$request->city && !$request->category_id && $request->subcategory_id){
                    $data['ticket'] = Ticket::orderBy('id','desc')->get();
                    $data['open'] = Ticket::where('status','open')->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['waiting'] = Ticket::where('status','waiting')->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['visit'] = Ticket::where('status','visit')->whereDate('visit_date','>=', $request->from_date)->whereDate('visit_date','<=', $request->to_date)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['revisit'] = Ticket::where('status','revisit')->whereDate('visit_date','>=', $request->from_date)->whereDate('visit_date','<=', $request->to_date)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['close'] = Ticket::where('status','closed')->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['reopen'] = Ticket::where('status','reopen')->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['openmonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Monday')
                                                ->where('status','open')
                                                ->count();
                    $data['opentuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Tuesday')
                                                ->where('status','open')
                                                ->count();
                    $data['openwedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Wednesday')
                                                ->where('status','open')->count();
                    $data['openthurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Thursday')
                                                ->where('status','open')->count();
                    $data['openfriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Friday')
                                                ->where('status','open')->count();
                    $data['opensatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Saturday')
                                                ->where('status','open')->count();
                    $data['opensunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Sunday')
                                                ->where('status','open')->count();
        
        
                    $data['waitingmonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Monday')
                                                ->where('status','waiting')->count();
                    $data['waitingtuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Tuesday')
                                                ->where('status','waiting')->count();
                    $data['waitingwedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Wednesday')
                                                ->where('status','waiting')->count();
                    $data['waitingthurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Thursday')
                                                ->where('status','waiting')->count();
                    $data['waitingfriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Friday')
                                                ->where('status','waiting')->count();
                    $data['waitingsatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Saturday')
                                                ->where('status','waiting')->count();
                    $data['waitingsunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Sunday')
                                                ->where('status','waiting')->count();
                    
                    $data['visitmonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date','>=', $request->from_date)->whereDate('visit_date','<=', $request->to_date)
                                                ->where('visit_day','Monday')
                                                ->where('status','visit')->count();
                    $data['visittuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date','>=', $request->from_date)->whereDate('visit_date','<=', $request->to_date)
                                                ->where('visit_day','Tuesday')
                                                ->where('status','visit')->count();
                    $data['visitwedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date','>=', $request->from_date)->whereDate('visit_date','<=', $request->to_date)
                                                ->where('visit_day','Wednesday')
                                                ->where('status','visit')->count();
                    $data['visitthurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date','>=', $request->from_date)->whereDate('visit_date','<=', $request->to_date)
                                                ->where('visit_day','Thursday')
                                                ->where('status','visit')->count();
                    $data['visitfriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date','>=', $request->from_date)->whereDate('visit_date','<=', $request->to_date)
                                                ->where('visit_day','Friday')
                                                ->where('status','visit')->count();
                    $data['visitsatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date','>=', $request->from_date)->whereDate('visit_date','<=', $request->to_date)
                                                ->where('visit_day','Saturday')
                                                ->where('status','visit')->count();
                    $data['visitsunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date','>=', $request->from_date)->whereDate('visit_date','<=', $request->to_date)
                                                ->where('visit_day','Sunday')
                                                ->where('status','visit')->count();
        
                    $data['revisitmonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date','>=', $request->from_date)->whereDate('visit_date','<=', $request->to_date)
                                                ->where('visit_day','Monday')
                                                ->where('status','revisit')->count();
                    $data['revisittuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date','>=', $request->from_date)->whereDate('visit_date','<=', $request->to_date)
                                                ->where('visit_day','Tuesday')
                                                ->where('status','revisit')->count();
                    $data['revisitwedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date','>=', $request->from_date)->whereDate('visit_date','<=', $request->to_date)
                                                ->where('visit_day','Wednesday')
                                                ->where('status','revisit')->count();
                    $data['revisitthurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date','>=', $request->from_date)->whereDate('visit_date','<=', $request->to_date)
                                                ->where('visit_day','Thursday')
                                                ->where('status','revisit')->count();
                    $data['revisitfriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date','>=', $request->from_date)->whereDate('visit_date','<=', $request->to_date)
                                                ->where('visit_day','Friday')
                                                ->where('status','revisit')->count();
                    $data['revisitsatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date','>=', $request->from_date)->whereDate('visit_date','<=', $request->to_date)
                                                ->where('visit_day','Saturday')
                                                ->where('status','revisit')->count();
                    $data['revisitsunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date','>=', $request->from_date)->whereDate('visit_date','<=', $request->to_date)
                                                ->where('visit_day','Sunday')
                                                ->where('status','revisit')->count();
        
        
                    $data['closemonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Monday')
                                                ->where('status','closed')->count();
                    $data['closetuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Tuesday')
                                                ->where('status','closed')->count();
                    $data['closewedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Wednesday')
                                                ->where('status','closed')->count();
                    $data['closethurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Thursday')
                                                ->where('status','closed')->count();
                    $data['closefriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Friday')
                                                ->where('status','closed')->count();
                    $data['closesatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Saturday')
                                                ->where('status','closed')->count();
                    $data['closesunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Sunday')
                                                ->where('status','closed')->count();
        
                    $data['reopenmonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Monday')
                                                ->where('status','reopen')->count();
                    $data['reopentuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Tuesday')
                                                ->where('status','reopen')->count();
                    $data['reopenwedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Wednesday')
                                                ->where('status','reopen')->count();
                    $data['reopenthurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Thursday')
                                                ->where('status','reopen')->count();
                    $data['reopenfriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Friday')
                                                ->where('status','reopen')->count();
                    $data['reopensatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Saturday')
                                                ->where('status','reopen')->count();
                    $data['reopensunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Sunday')
                                                ->where('status','reopen')->count();
        
                    $data['notification'] = DB::table('notifications')->count();
                }
                if($request->from_date && $request->to_date && $request->city && !$request->category_id && $request->subcategory_id){
                    $data['ticket'] = Ticket::orderBy('id','desc')->get();
                    $data['open'] = Ticket::where('status','open')->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['waiting'] = Ticket::where('status','waiting')->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['visit'] = Ticket::where('status','visit')->where('city',$request->city)->whereDate('visit_date','>=',$request->from_date)->whereDate('visit_date','<=', $request->to_date)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['revisit'] = Ticket::where('status','revisit')->where('city',$request->city)->whereDate('visit_date','>=', $request->to_date)->whereDate('visit_date','<=', $request->to_date)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['close'] = Ticket::where('status','closed')->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['reopen'] = Ticket::where('status','reopen')->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['openmonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Monday')
                                                ->where('status','open')
                                                ->count();
                    $data['opentuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Tuesday')
                                                ->where('status','open')
                                                ->count();
                    $data['openwedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Wednesday')
                                                ->where('status','open')->count();
                    $data['openthurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Thursday')
                                                ->where('status','open')->count();
                    $data['openfriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Friday')
                                                ->where('status','open')->count();
                    $data['opensatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Saturday')
                                                ->where('status','open')->count();
                    $data['opensunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Sunday')
                                                ->where('status','open')->count();
        
        
                    $data['waitingmonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Monday')
                                                ->where('status','waiting')->count();
                    $data['waitingtuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Tuesday')
                                                ->where('status','waiting')->count();
                    $data['waitingwedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Wednesday')
                                                ->where('status','waiting')->count();
                    $data['waitingthurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Thursday')
                                                ->where('status','waiting')->count();
                    $data['waitingfriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Friday')
                                                ->where('status','waiting')->count();
                    $data['waitingsatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Saturday')
                                                ->where('status','waiting')->count();
                    $data['waitingsunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Sunday')
                                                ->where('status','waiting')->count();
                    
                    $data['visitmonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)->whereDate('visit_date', '>=', $request->from_date)->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Monday')
                                                ->where('status','visit')->count();
                    $data['visittuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)->whereDate('visit_date', '>=', $request->from_date)->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Tuesday')
                                                ->where('status','visit')->count();
                    $data['visitwedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)->whereDate('visit_date', '>=', $request->from_date)->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Wednesday')
                                                ->where('status','visit')->count();
                    $data['visitthurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)->whereDate('visit_date', '>=', $request->from_date)->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Thursday')
                                                ->where('status','visit')->count();
                    $data['visitfriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)->whereDate('visit_date', '>=', $request->from_date)->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Friday')
                                                ->where('status','visit')->count();
                    $data['visitsatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)->whereDate('visit_date', '>=', $request->from_date)->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Saturday')
                                                ->where('status','visit')->count();
                    $data['visitsunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)->whereDate('visit_date', '>=', $request->from_date)->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Sunday')
                                                ->where('status','visit')->count();
        
                    $data['revisitmonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)->whereDate('visit_date', '>=', $request->from_date)->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Monday')
                                                ->where('status','revisit')->count();
                    $data['revisittuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)->whereDate('visit_date', '>=', $request->from_date)->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Tuesday')
                                                ->where('status','revisit')->count();
                    $data['revisitwedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)->whereDate('visit_date', '>=', $request->from_date)->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Wednesday')
                                                ->where('status','revisit')->count();
                    $data['revisitthurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)->whereDate('visit_date', '>=', $request->from_date)->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Thursday')
                                                ->where('status','revisit')->count();
                    $data['revisitfriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)->whereDate('visit_date', '>=', $request->from_date)->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Friday')
                                                ->where('status','revisit')->count();
                    $data['revisitsatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)->whereDate('visit_date', '>=', $request->from_date)->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Saturday')
                                                ->where('status','revisit')->count();
                    $data['revisitsunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)->whereDate('visit_date', '>=', $request->from_date)->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Sunday')
                                                ->where('status','revisit')->count();
        
        
                    $data['closemonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Monday')
                                                ->where('status','closed')->count();
                    $data['closetuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Tuesday')
                                                ->where('status','closed')->count();
                    $data['closewedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Wednesday')
                                                ->where('status','closed')->count();
                    $data['closethurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Thursday')
                                                ->where('status','closed')->count();
                    $data['closefriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Friday')
                                                ->where('status','closed')->count();
                    $data['closesatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Saturday')
                                                ->where('status','closed')->count();
                    $data['closesunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Sunday')
                                                ->where('status','closed')->count();
        
                    $data['reopenmonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Monday')
                                                ->where('status','reopen')->count();
                    $data['reopentuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Tuesday')
                                                ->where('status','reopen')->count();
                    $data['reopenwedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Wednesday')
                                                ->where('status','reopen')->count();
                    $data['reopenthurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Thursday')
                                                ->where('status','reopen')->count();
                    $data['reopenfriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Friday')
                                                ->where('status','reopen')->count();
                    $data['reopensatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Saturday')
                                                ->where('status','reopen')->count();
                    $data['reopensunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Sunday')
                                                ->where('status','reopen')->count();
        
                    $data['notification'] = DB::table('notifications')->count();
                }
                if($request->from_date && $request->to_date && $request->city && $request->category_id && !$request->subcategory_id){
                    $data['ticket'] = Ticket::orderBy('id','desc')->get();
                    $data['open'] = Ticket::where('status','open')->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)->where('category_id', $request->category_id)->count();
                    $data['waiting'] = Ticket::where('status','waiting')->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)->where('category_id', $request->category_id)->count();
                    $data['visit'] = Ticket::where('status','visit')->where('city',$request->city)->whereDate('visit_date','>=',$request->from_date)->whereDate('visit_date','<=', $request->to_date)->where('category_id', $request->category_id)->count();
                    $data['revisit'] = Ticket::where('status','revisit')->where('city',$request->city)->whereDate('visit_date','>=', $request->to_date)->whereDate('visit_date','<=', $request->to_date)->where('category_id', $request->category_id)->count();
                    $data['close'] = Ticket::where('status','closed')->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)->where('category_id', $request->category_id)->count();
                    $data['reopen'] = Ticket::where('status','reopen')->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)->where('category_id', $request->category_id)->count();
                    $data['openmonday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Monday')
                                                ->where('status','open')
                                                ->count();
                    $data['opentuesday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Tuesday')
                                                ->where('status','open')
                                                ->count();
                    $data['openwedday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Wednesday')
                                                ->where('status','open')->count();
                    $data['openthurday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Thursday')
                                                ->where('status','open')->count();
                    $data['openfriday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Friday')
                                                ->where('status','open')->count();
                    $data['opensatday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Saturday')
                                                ->where('status','open')->count();
                    $data['opensunday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Sunday')
                                                ->where('status','open')->count();
        
        
                    $data['waitingmonday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Monday')
                                                ->where('status','waiting')->count();
                    $data['waitingtuesday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Tuesday')
                                                ->where('status','waiting')->count();
                    $data['waitingwedday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Wednesday')
                                                ->where('status','waiting')->count();
                    $data['waitingthurday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Thursday')
                                                ->where('status','waiting')->count();
                    $data['waitingfriday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Friday')
                                                ->where('status','waiting')->count();
                    $data['waitingsatday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Saturday')
                                                ->where('status','waiting')->count();
                    $data['waitingsunday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Sunday')
                                                ->where('status','waiting')->count();
                    
                    $data['visitmonday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('visit_date', '>=', $request->from_date)->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Monday')
                                                ->where('status','visit')->count();
                    $data['visittuesday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('visit_date', '>=', $request->from_date)->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Tuesday')
                                                ->where('status','visit')->count();
                    $data['visitwedday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('visit_date', '>=', $request->from_date)->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Wednesday')
                                                ->where('status','visit')->count();
                    $data['visitthurday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('visit_date', '>=', $request->from_date)->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Thursday')
                                                ->where('status','visit')->count();
                    $data['visitfriday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('visit_date', '>=', $request->from_date)->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Friday')
                                                ->where('status','visit')->count();
                    $data['visitsatday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('visit_date', '>=', $request->from_date)->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Saturday')
                                                ->where('status','visit')->count();
                    $data['visitsunday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('visit_date', '>=', $request->from_date)->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Sunday')
                                                ->where('status','visit')->count();
        
                    $data['revisitmonday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('visit_date', '>=', $request->from_date)->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Monday')
                                                ->where('status','revisit')->count();
                    $data['revisittuesday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('visit_date', '>=', $request->from_date)->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Tuesday')
                                                ->where('status','revisit')->count();
                    $data['revisitwedday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('visit_date', '>=', $request->from_date)->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Wednesday')
                                                ->where('status','revisit')->count();
                    $data['revisitthurday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('visit_date', '>=', $request->from_date)->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Thursday')
                                                ->where('status','revisit')->count();
                    $data['revisitfriday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('visit_date', '>=', $request->from_date)->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Friday')
                                                ->where('status','revisit')->count();
                    $data['revisitsatday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('visit_date', '>=', $request->from_date)->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Saturday')
                                                ->where('status','revisit')->count();
                    $data['revisitsunday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('visit_date', '>=', $request->from_date)->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Sunday')
                                                ->where('status','revisit')->count();
        
        
                    $data['closemonday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Monday')
                                                ->where('status','closed')->count();
                    $data['closetuesday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Tuesday')
                                                ->where('status','closed')->count();
                    $data['closewedday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Wednesday')
                                                ->where('status','closed')->count();
                    $data['closethurday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Thursday')
                                                ->where('status','closed')->count();
                    $data['closefriday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Friday')
                                                ->where('status','closed')->count();
                    $data['closesatday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Saturday')
                                                ->where('status','closed')->count();
                    $data['closesunday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Sunday')
                                                ->where('status','closed')->count();
        
                    $data['reopenmonday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Monday')
                                                ->where('status','reopen')->count();
                    $data['reopentuesday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Tuesday')
                                                ->where('status','reopen')->count();
                    $data['reopenwedday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Wednesday')
                                                ->where('status','reopen')->count();
                    $data['reopenthurday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Thursday')
                                                ->where('status','reopen')->count();
                    $data['reopenfriday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Friday')
                                                ->where('status','reopen')->count();
                    $data['reopensatday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Saturday')
                                                ->where('status','reopen')->count();
                    $data['reopensunday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('city',$request->city)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Sunday')
                                                ->where('status','reopen')->count();
        
                    $data['notification'] = DB::table('notifications')->count();
                }
                if(!$request->from_date && $request->to_date && $request->city && !$request->category_id && $request->subcategory_id){
                    $data['ticket'] = Ticket::orderBy('id','desc')->get();
                    $data['open'] = Ticket::where('status','open')->where('city',$request->city)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)->count();
                    $data['waiting'] = Ticket::where('status','waiting')->where('city',$request->city)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)->count();
                    $data['visit'] = Ticket::where('status','visit')->where('city',$request->city)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->to_date)->count();
                    $data['revisit'] = Ticket::where('status','revisit')->where('city',$request->city)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->to_date)->count();
                    $data['close'] = Ticket::where('status','closed')->where('city',$request->city)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)->count();
                    $data['reopen'] = Ticket::where('status','reopen')->where('city',$request->city)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)->count();
                    $data['openmonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Monday')
                                                ->where('status','open')
                                                ->count();
                    $data['opentuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Tuesday')
                                                ->where('status','open')
                                                ->count();
                    $data['openwedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Wednesday')
                                                ->where('status','open')->count();
                    $data['openthurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Thursday')
                                                ->where('status','open')->count();
                    $data['openfriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Friday')
                                                ->where('status','open')->count();
                    $data['opensatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Saturday')
                                                ->where('status','open')->count();
                    $data['opensunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Sunday')
                                                ->where('status','open')->count();
        
        
                    $data['waitingmonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Monday')
                                                ->where('status','waiting')->count();
                    $data['waitingtuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Tuesday')
                                                ->where('status','waiting')->count();
                    $data['waitingwedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Wednesday')
                                                ->where('status','waiting')->count();
                    $data['waitingthurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Thursday')
                                                ->where('status','waiting')->count();
                    $data['waitingfriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Friday')
                                                ->where('status','waiting')->count();
                    $data['waitingsatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Saturday')
                                                ->where('status','waiting')->count();
                    $data['waitingsunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Sunday')
                                                ->where('status','waiting')->count();
                    
                    $data['visitmonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->to_date)->where('city',$request->city)
                                                ->where('visit_day','Monday')
                                                ->where('status','visit')->count();
                    $data['visittuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->to_date)->where('city',$request->city)
                                                ->where('visit_day','Tuesday')
                                                ->where('status','visit')->count();
                    $data['visitwedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->to_date)->where('city',$request->city)
                                                ->where('visit_day','Wednesday')
                                                ->where('status','visit')->count();
                    $data['visitthurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->to_date)->where('city',$request->city)
                                                ->where('visit_day','Thursday')
                                                ->where('status','visit')->count();
                    $data['visitfriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->to_date)->where('city',$request->city)
                                                ->where('visit_day','Friday')
                                                ->where('status','visit')->count();
                    $data['visitsatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->to_date)->where('city',$request->city)
                                                ->where('visit_day','Saturday')
                                                ->where('status','visit')->count();
                    $data['visitsunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->to_date)->where('city',$request->city)
                                                ->where('visit_day','Sunday')
                                                ->where('status','visit')->count();
        
                    $data['revisitmonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->to_date)->where('city',$request->city)
                                                ->where('visit_day','Monday')
                                                ->where('status','revisit')->count();
                    $data['revisittuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->to_date)->where('city',$request->city)
                                                ->where('visit_day','Tuesday')
                                                ->where('status','revisit')->count();
                    $data['revisitwedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->to_date)->where('city',$request->city)
                                                ->where('visit_day','Wednesday')
                                                ->where('status','revisit')->count();
                    $data['revisitthurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->to_date)->where('city',$request->city)
                                                ->where('visit_day','Thursday')
                                                ->where('status','revisit')->count();
                    $data['revisitfriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->to_date)->where('city',$request->city)
                                                ->where('visit_day','Friday')
                                                ->where('status','revisit')->count();
                    $data['revisitsatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->to_date)->where('city',$request->city)
                                                ->where('visit_day','Saturday')
                                                ->where('status','revisit')->count();
                    $data['revisitsunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->to_date)->where('city',$request->city)
                                                ->where('visit_day','Sunday')
                                                ->where('status','revisit')->count();
        
        
                    $data['closemonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Monday')
                                                ->where('status','closed')->count();
                    $data['closetuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Tuesday')
                                                ->where('status','closed')->count();
                    $data['closewedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Wednesday')
                                                ->where('status','closed')->count();
                    $data['closethurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Thursday')
                                                ->where('status','closed')->count();
                    $data['closefriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Friday')
                                                ->where('status','closed')->count();
                    $data['closesatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Saturday')
                                                ->where('status','closed')->count();
                    $data['closesunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Sunday')
                                                ->where('status','closed')->count();
        
                    $data['reopenmonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Monday')
                                                ->where('status','reopen')->count();
                    $data['reopentuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Tuesday')
                                                ->where('status','reopen')->count();
                    $data['reopenwedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Wednesday')
                                                ->where('status','reopen')->count();
                    $data['reopenthurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Thursday')
                                                ->where('status','reopen')->count();
                    $data['reopenfriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Friday')
                                                ->where('status','reopen')->count();
                    $data['reopensatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Saturday')
                                                ->where('status','reopen')->count();
                    $data['reopensunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Sunday')
                                                ->where('status','reopen')->count();
        
                    $data['notification'] = DB::table('notifications')->count();
                }
                if(!$request->from_date && $request->to_date && $request->city && $request->category_id && !$request->subcategory_id){
                    $data['ticket'] = Ticket::orderBy('id','desc')->get();
                    $data['open'] = Ticket::where('status','open')->where('city',$request->city)->where('category_id', $request->category_id)->whereDate('update_date', $request->to_date)->count();
                    $data['waiting'] = Ticket::where('status','waiting')->where('city',$request->city)->where('category_id', $request->category_id)->whereDate('update_date', $request->to_date)->count();
                    $data['visit'] = Ticket::where('status','visit')->where('city',$request->city)->where('category_id', $request->category_id)->whereDate('visit_date', $request->to_date)->count();
                    $data['revisit'] = Ticket::where('status','revisit')->where('city',$request->city)->where('category_id', $request->category_id)->whereDate('visit_date', $request->to_date)->count();
                    $data['close'] = Ticket::where('status','closed')->where('city',$request->city)->where('category_id', $request->category_id)->whereDate('update_date', $request->to_date)->count();
                    $data['reopen'] = Ticket::where('status','reopen')->where('city',$request->city)->where('category_id', $request->category_id)->whereDate('update_date', $request->to_date)->count();
                    $data['openmonday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Monday')
                                                ->where('status','open')
                                                ->count();
                    $data['opentuesday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Tuesday')
                                                ->where('status','open')
                                                ->count();
                    $data['openwedday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Wednesday')
                                                ->where('status','open')->count();
                    $data['openthurday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Thursday')
                                                ->where('status','open')->count();
                    $data['openfriday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Friday')
                                                ->where('status','open')->count();
                    $data['opensatday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Saturday')
                                                ->where('status','open')->count();
                    $data['opensunday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Sunday')
                                                ->where('status','open')->count();
        
        
                    $data['waitingmonday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Monday')
                                                ->where('status','waiting')->count();
                    $data['waitingtuesday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Tuesday')
                                                ->where('status','waiting')->count();
                    $data['waitingwedday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Wednesday')
                                                ->where('status','waiting')->count();
                    $data['waitingthurday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Thursday')
                                                ->where('status','waiting')->count();
                    $data['waitingfriday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Friday')
                                                ->where('status','waiting')->count();
                    $data['waitingsatday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Saturday')
                                                ->where('status','waiting')->count();
                    $data['waitingsunday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Sunday')
                                                ->where('status','waiting')->count();
                    
                    $data['visitmonday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('visit_date', $request->to_date)->where('city',$request->city)
                                                ->where('visit_day','Monday')
                                                ->where('status','visit')->count();
                    $data['visittuesday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('visit_date', $request->to_date)->where('city',$request->city)
                                                ->where('visit_day','Tuesday')
                                                ->where('status','visit')->count();
                    $data['visitwedday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('visit_date', $request->to_date)->where('city',$request->city)
                                                ->where('visit_day','Wednesday')
                                                ->where('status','visit')->count();
                    $data['visitthurday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('visit_date', $request->to_date)->where('city',$request->city)
                                                ->where('visit_day','Thursday')
                                                ->where('status','visit')->count();
                    $data['visitfriday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('visit_date', $request->to_date)->where('city',$request->city)
                                                ->where('visit_day','Friday')
                                                ->where('status','visit')->count();
                    $data['visitsatday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('visit_date', $request->to_date)->where('city',$request->city)
                                                ->where('visit_day','Saturday')
                                                ->where('status','visit')->count();
                    $data['visitsunday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('visit_date', $request->to_date)->where('city',$request->city)
                                                ->where('visit_day','Sunday')
                                                ->where('status','visit')->count();
        
                    $data['revisitmonday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('visit_date', $request->to_date)->where('city',$request->city)
                                                ->where('visit_day','Monday')
                                                ->where('status','revisit')->count();
                    $data['revisittuesday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('visit_date', $request->to_date)->where('city',$request->city)
                                                ->where('visit_day','Tuesday')
                                                ->where('status','revisit')->count();
                    $data['revisitwedday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('visit_date', $request->to_date)->where('city',$request->city)
                                                ->where('visit_day','Wednesday')
                                                ->where('status','revisit')->count();
                    $data['revisitthurday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('visit_date', $request->to_date)->where('city',$request->city)
                                                ->where('visit_day','Thursday')
                                                ->where('status','revisit')->count();
                    $data['revisitfriday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('visit_date', $request->to_date)->where('city',$request->city)
                                                ->where('visit_day','Friday')
                                                ->where('status','revisit')->count();
                    $data['revisitsatday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('visit_date', $request->to_date)->where('city',$request->city)
                                                ->where('visit_day','Saturday')
                                                ->where('status','revisit')->count();
                    $data['revisitsunday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('visit_date', $request->to_date)->where('city',$request->city)
                                                ->where('visit_day','Sunday')
                                                ->where('status','revisit')->count();
        
        
                    $data['closemonday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Monday')
                                                ->where('status','closed')->count();
                    $data['closetuesday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Tuesday')
                                                ->where('status','closed')->count();
                    $data['closewedday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Wednesday')
                                                ->where('status','closed')->count();
                    $data['closethurday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Thursday')
                                                ->where('status','closed')->count();
                    $data['closefriday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Friday')
                                                ->where('status','closed')->count();
                    $data['closesatday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Saturday')
                                                ->where('status','closed')->count();
                    $data['closesunday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Sunday')
                                                ->where('status','closed')->count();
        
                    $data['reopenmonday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Monday')
                                                ->where('status','reopen')->count();
                    $data['reopentuesday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Tuesday')
                                                ->where('status','reopen')->count();
                    $data['reopenwedday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Wednesday')
                                                ->where('status','reopen')->count();
                    $data['reopenthurday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Thursday')
                                                ->where('status','reopen')->count();
                    $data['reopenfriday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Friday')
                                                ->where('status','reopen')->count();
                    $data['reopensatday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Saturday')
                                                ->where('status','reopen')->count();
                    $data['reopensunday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Sunday')
                                                ->where('status','reopen')->count();
        
                    $data['notification'] = DB::table('notifications')->count();
                }
                if(!$request->from_date && $request->to_date && $request->city && $request->category_id && $request->subcategory_id){
                    $data['ticket'] = Ticket::orderBy('id','desc')->get();
                    $data['open'] = Ticket::where('status','open')->where('city',$request->city)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)->count();
                    $data['waiting'] = Ticket::where('status','waiting')->where('city',$request->city)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)->count();
                    $data['visit'] = Ticket::where('status','visit')->where('city',$request->city)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->to_date)->count();
                    $data['revisit'] = Ticket::where('status','revisit')->where('city',$request->city)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->to_date)->count();
                    $data['close'] = Ticket::where('status','closed')->where('city',$request->city)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)->count();
                    $data['reopen'] = Ticket::where('status','reopen')->where('city',$request->city)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)->count();
                    $data['openmonday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Monday')
                                                ->where('status','open')
                                                ->count();
                    $data['opentuesday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Tuesday')
                                                ->where('status','open')
                                                ->count();
                    $data['openwedday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Wednesday')
                                                ->where('status','open')->count();
                    $data['openthurday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Thursday')
                                                ->where('status','open')->count();
                    $data['openfriday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Friday')
                                                ->where('status','open')->count();
                    $data['opensatday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Saturday')
                                                ->where('status','open')->count();
                    $data['opensunday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Sunday')
                                                ->where('status','open')->count();
        
        
                    $data['waitingmonday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Monday')
                                                ->where('status','waiting')->count();
                    $data['waitingtuesday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Tuesday')
                                                ->where('status','waiting')->count();
                    $data['waitingwedday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Wednesday')
                                                ->where('status','waiting')->count();
                    $data['waitingthurday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Thursday')
                                                ->where('status','waiting')->count();
                    $data['waitingfriday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Friday')
                                                ->where('status','waiting')->count();
                    $data['waitingsatday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Saturday')
                                                ->where('status','waiting')->count();
                    $data['waitingsunday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Sunday')
                                                ->where('status','waiting')->count();
                    
                    $data['visitmonday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->to_date)->where('city',$request->city)
                                                ->where('visit_day','Monday')
                                                ->where('status','visit')->count();
                    $data['visittuesday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->to_date)->where('city',$request->city)
                                                ->where('visit_day','Tuesday')
                                                ->where('status','visit')->count();
                    $data['visitwedday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->to_date)->where('city',$request->city)
                                                ->where('visit_day','Wednesday')
                                                ->where('status','visit')->count();
                    $data['visitthurday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->to_date)->where('city',$request->city)
                                                ->where('visit_day','Thursday')
                                                ->where('status','visit')->count();
                    $data['visitfriday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->to_date)->where('city',$request->city)
                                                ->where('visit_day','Friday')
                                                ->where('status','visit')->count();
                    $data['visitsatday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->to_date)->where('city',$request->city)
                                                ->where('visit_day','Saturday')
                                                ->where('status','visit')->count();
                    $data['visitsunday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->to_date)->where('city',$request->city)
                                                ->where('visit_day','Sunday')
                                                ->where('status','visit')->count();
        
                    $data['revisitmonday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->to_date)->where('city',$request->city)
                                                ->where('visit_day','Monday')
                                                ->where('status','revisit')->count();
                    $data['revisittuesday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->to_date)->where('city',$request->city)
                                                ->where('visit_day','Tuesday')
                                                ->where('status','revisit')->count();
                    $data['revisitwedday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->to_date)->where('city',$request->city)
                                                ->where('visit_day','Wednesday')
                                                ->where('status','revisit')->count();
                    $data['revisitthurday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->to_date)->where('city',$request->city)
                                                ->where('visit_day','Thursday')
                                                ->where('status','revisit')->count();
                    $data['revisitfriday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->to_date)->where('city',$request->city)
                                                ->where('visit_day','Friday')
                                                ->where('status','revisit')->count();
                    $data['revisitsatday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->to_date)->where('city',$request->city)
                                                ->where('visit_day','Saturday')
                                                ->where('status','revisit')->count();
                    $data['revisitsunday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->to_date)->where('city',$request->city)
                                                ->where('visit_day','Sunday')
                                                ->where('status','revisit')->count();
        
        
                    $data['closemonday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Monday')
                                                ->where('status','closed')->count();
                    $data['closetuesday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Tuesday')
                                                ->where('status','closed')->count();
                    $data['closewedday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Wednesday')
                                                ->where('status','closed')->count();
                    $data['closethurday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Thursday')
                                                ->where('status','closed')->count();
                    $data['closefriday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Friday')
                                                ->where('status','closed')->count();
                    $data['closesatday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Saturday')
                                                ->where('status','closed')->count();
                    $data['closesunday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Sunday')
                                                ->where('status','closed')->count();
        
                    $data['reopenmonday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Monday')
                                                ->where('status','reopen')->count();
                    $data['reopentuesday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Tuesday')
                                                ->where('status','reopen')->count();
                    $data['reopenwedday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Wednesday')
                                                ->where('status','reopen')->count();
                    $data['reopenthurday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Thursday')
                                                ->where('status','reopen')->count();
                    $data['reopenfriday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Friday')
                                                ->where('status','reopen')->count();
                    $data['reopensatday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Saturday')
                                                ->where('status','reopen')->count();
                    $data['reopensunday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->to_date)->where('city',$request->city)
                                                ->where('update_day','Sunday')
                                                ->where('status','reopen')->count();
        
                    $data['notification'] = DB::table('notifications')->count();
                }
                if($request->from_date && !$request->to_date && $request->city && $request->category_id && $request->subcategory_id){
                    $data['ticket'] = Ticket::orderBy('id','desc')->get();
                    $data['open'] = Ticket::where('status','open')->where('city',$request->city)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)->count();
                    $data['waiting'] = Ticket::where('status','waiting')->where('city',$request->city)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)->count();
                    $data['visit'] = Ticket::where('status','visit')->where('city',$request->city)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->from_date)->count();
                    $data['revisit'] = Ticket::where('status','revisit')->where('city',$request->city)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->from_date)->count();
                    $data['close'] = Ticket::where('status','closed')->where('city',$request->city)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)->count();
                    $data['reopen'] = Ticket::where('status','reopen')->where('city',$request->city)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)->count();
                    $data['openmonday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)->where('city',$request->city)
                                                ->where('update_day','Monday')
                                                ->where('status','open')
                                                ->count();
                    $data['opentuesday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)->where('city',$request->city)
                                                ->where('update_day','Tuesday')
                                                ->where('status','open')
                                                ->count();
                    $data['openwedday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)->where('city',$request->city)
                                                ->where('update_day','Wednesday')
                                                ->where('status','open')->count();
                    $data['openthurday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)->where('city',$request->city)
                                                ->where('update_day','Thursday')
                                                ->where('status','open')->count();
                    $data['openfriday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)->where('city',$request->city)
                                                ->where('update_day','Friday')
                                                ->where('status','open')->count();
                    $data['opensatday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)->where('city',$request->city)
                                                ->where('update_day','Saturday')
                                                ->where('status','open')->count();
                    $data['opensunday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)->where('city',$request->city)
                                                ->where('update_day','Sunday')
                                                ->where('status','open')->count();
        
        
                    $data['waitingmonday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)->where('city',$request->city)
                                                ->where('update_day','Monday')
                                                ->where('status','waiting')->count();
                    $data['waitingtuesday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)->where('city',$request->city)
                                                ->where('update_day','Tuesday')
                                                ->where('status','waiting')->count();
                    $data['waitingwedday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)->where('city',$request->city)
                                                ->where('update_day','Wednesday')
                                                ->where('status','waiting')->count();
                    $data['waitingthurday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)->where('city',$request->city)
                                                ->where('update_day','Thursday')
                                                ->where('status','waiting')->count();
                    $data['waitingfriday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)->where('city',$request->city)
                                                ->where('update_day','Friday')
                                                ->where('status','waiting')->count();
                    $data['waitingsatday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)->where('city',$request->city)
                                                ->where('update_day','Saturday')
                                                ->where('status','waiting')->count();
                    $data['waitingsunday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)->where('city',$request->city)
                                                ->where('update_day','Sunday')
                                                ->where('status','waiting')->count();
                    
                    $data['visitmonday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->from_date)->where('city',$request->city)
                                                ->where('visit_day','Monday')
                                                ->where('status','visit')->count();
                    $data['visittuesday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->from_date)->where('city',$request->city)
                                                ->where('visit_day','Tuesday')
                                                ->where('status','visit')->count();
                    $data['visitwedday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->from_date)->where('city',$request->city)
                                                ->where('visit_day','Wednesday')
                                                ->where('status','visit')->count();
                    $data['visitthurday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->from_date)->where('city',$request->city)
                                                ->where('visit_day','Thursday')
                                                ->where('status','visit')->count();
                    $data['visitfriday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->from_date)->where('city',$request->city)
                                                ->where('visit_day','Friday')
                                                ->where('status','visit')->count();
                    $data['visitsatday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->from_date)->where('city',$request->city)
                                                ->where('visit_day','Saturday')
                                                ->where('status','visit')->count();
                    $data['visitsunday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->from_date)->where('city',$request->city)
                                                ->where('visit_day','Sunday')
                                                ->where('status','visit')->count();
        
                    $data['revisitmonday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->from_date)->where('city',$request->city)
                                                ->where('visit_day','Monday')
                                                ->where('status','revisit')->count();
                    $data['revisittuesday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->from_date)->where('city',$request->city)
                                                ->where('visit_day','Tuesday')
                                                ->where('status','revisit')->count();
                    $data['revisitwedday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->from_date)->where('city',$request->city)
                                                ->where('visit_day','Wednesday')
                                                ->where('status','revisit')->count();
                    $data['revisitthurday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->from_date)->where('city',$request->city)
                                                ->where('visit_day','Thursday')
                                                ->where('status','revisit')->count();
                    $data['revisitfriday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->from_date)->where('city',$request->city)
                                                ->where('visit_day','Friday')
                                                ->where('status','revisit')->count();
                    $data['revisitsatday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->from_date)->where('city',$request->city)
                                                ->where('visit_day','Saturday')
                                                ->where('status','revisit')->count();
                    $data['revisitsunday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('visit_date', $request->from_date)->where('city',$request->city)
                                                ->where('visit_day','Sunday')
                                                ->where('status','revisit')->count();
        
        
                    $data['closemonday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)->where('city',$request->city)
                                                ->where('update_day','Monday')
                                                ->where('status','closed')->count();
                    $data['closetuesday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)->where('city',$request->city)
                                                ->where('update_day','Tuesday')
                                                ->where('status','closed')->count();
                    $data['closewedday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)->where('city',$request->city)
                                                ->where('update_day','Wednesday')
                                                ->where('status','closed')->count();
                    $data['closethurday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)->where('city',$request->city)
                                                ->where('update_day','Thursday')
                                                ->where('status','closed')->count();
                    $data['closefriday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)->where('city',$request->city)
                                                ->where('update_day','Friday')
                                                ->where('status','closed')->count();
                    $data['closesatday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)->where('city',$request->city)
                                                ->where('update_day','Saturday')
                                                ->where('status','closed')->count();
                    $data['closesunday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)->where('city',$request->city)
                                                ->where('update_day','Sunday')
                                                ->where('status','closed')->count();
        
                    $data['reopenmonday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)->where('city',$request->city)
                                                ->where('update_day','Monday')
                                                ->where('status','reopen')->count();
                    $data['reopentuesday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)->where('city',$request->city)
                                                ->where('update_day','Tuesday')
                                                ->where('status','reopen')->count();
                    $data['reopenwedday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)->where('city',$request->city)
                                                ->where('update_day','Wednesday')
                                                ->where('status','reopen')->count();
                    $data['reopenthurday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)->where('city',$request->city)
                                                ->where('update_day','Thursday')
                                                ->where('status','reopen')->count();
                    $data['reopenfriday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)->where('city',$request->city)
                                                ->where('update_day','Friday')
                                                ->where('status','reopen')->count();
                    $data['reopensatday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)->where('city',$request->city)
                                                ->where('update_day','Saturday')
                                                ->where('status','reopen')->count();
                    $data['reopensunday'] = Ticket::where('assign_to',$user->id)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->whereDate('update_date', $request->from_date)->where('city',$request->city)
                                                ->where('update_day','Sunday')
                                                ->where('status','reopen')->count();
        
                    $data['notification'] = DB::table('notifications')->count();
                }
                if($request->from_date && $request->to_date && !$request->city && $request->category_id && $request->subcategory_id){
                    $data['ticket'] = Ticket::orderBy('id','desc')->get();
                    $data['open'] = Ticket::where('status','open')->where('category_id', $request->category_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['waiting'] = Ticket::where('status','waiting')->where('category_id', $request->category_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['visit'] = Ticket::where('status','visit')->where('category_id', $request->category_id)->whereDate('visit_date','>=',$request->from_date)->whereDate('visit_date','<=', $request->to_date)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['revisit'] = Ticket::where('status','revisit')->where('category_id', $request->category_id)->whereDate('visit_date','>=', $request->to_date)->whereDate('visit_date','<=', $request->to_date)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['close'] = Ticket::where('status','closed')->where('category_id', $request->category_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['reopen'] = Ticket::where('status','reopen')->where('category_id', $request->category_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)->where('subcategory_id', $request->subcategory_id)->count();
                    $data['openmonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Monday')
                                                ->where('status','open')
                                                ->count();
                    $data['opentuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Tuesday')
                                                ->where('status','open')
                                                ->count();
                    $data['openwedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Wednesday')
                                                ->where('status','open')->count();
                    $data['openthurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Thursday')
                                                ->where('status','open')->count();
                    $data['openfriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Friday')
                                                ->where('status','open')->count();
                    $data['opensatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Saturday')
                                                ->where('status','open')->count();
                    $data['opensunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Sunday')
                                                ->where('status','open')->count();
        
        
                    $data['waitingmonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Monday')
                                                ->where('status','waiting')->count();
                    $data['waitingtuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Tuesday')
                                                ->where('status','waiting')->count();
                    $data['waitingwedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Wednesday')
                                                ->where('status','waiting')->count();
                    $data['waitingthurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Thursday')
                                                ->where('status','waiting')->count();
                    $data['waitingfriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Friday')
                                                ->where('status','waiting')->count();
                    $data['waitingsatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Saturday')
                                                ->where('status','waiting')->count();
                    $data['waitingsunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Sunday')
                                                ->where('status','waiting')->count();
                    
                    $data['visitmonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->whereDate('visit_date', '>=', $request->from_date)->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Monday')
                                                ->where('status','visit')->count();
                    $data['visittuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->whereDate('visit_date', '>=', $request->from_date)->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Tuesday')
                                                ->where('status','visit')->count();
                    $data['visitwedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->whereDate('visit_date', '>=', $request->from_date)->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Wednesday')
                                                ->where('status','visit')->count();
                    $data['visitthurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->whereDate('visit_date', '>=', $request->from_date)->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Thursday')
                                                ->where('status','visit')->count();
                    $data['visitfriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->whereDate('visit_date', '>=', $request->from_date)->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Friday')
                                                ->where('status','visit')->count();
                    $data['visitsatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->whereDate('visit_date', '>=', $request->from_date)->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Saturday')
                                                ->where('status','visit')->count();
                    $data['visitsunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->whereDate('visit_date', '>=', $request->from_date)->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Sunday')
                                                ->where('status','visit')->count();
        
                    $data['revisitmonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->whereDate('visit_date', '>=', $request->from_date)->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Monday')
                                                ->where('status','revisit')->count();
                    $data['revisittuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->whereDate('visit_date', '>=', $request->from_date)->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Tuesday')
                                                ->where('status','revisit')->count();
                    $data['revisitwedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->whereDate('visit_date', '>=', $request->from_date)->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Wednesday')
                                                ->where('status','revisit')->count();
                    $data['revisitthurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->whereDate('visit_date', '>=', $request->from_date)->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Thursday')
                                                ->where('status','revisit')->count();
                    $data['revisitfriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->whereDate('visit_date', '>=', $request->from_date)->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Friday')
                                                ->where('status','revisit')->count();
                    $data['revisitsatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->whereDate('visit_date', '>=', $request->from_date)->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Saturday')
                                                ->where('status','revisit')->count();
                    $data['revisitsunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->whereDate('visit_date', '>=', $request->from_date)->whereDate('visit_date', '<=', $request->to_date)
                                                ->where('visit_day','Sunday')
                                                ->where('status','revisit')->count();
        
        
                    $data['closemonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Monday')
                                                ->where('status','closed')->count();
                    $data['closetuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Tuesday')
                                                ->where('status','closed')->count();
                    $data['closewedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Wednesday')
                                                ->where('status','closed')->count();
                    $data['closethurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Thursday')
                                                ->where('status','closed')->count();
                    $data['closefriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Friday')
                                                ->where('status','closed')->count();
                    $data['closesatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Saturday')
                                                ->where('status','closed')->count();
                    $data['closesunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Sunday')
                                                ->where('status','closed')->count();
        
                    $data['reopenmonday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Monday')
                                                ->where('status','reopen')->count();
                    $data['reopentuesday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Tuesday')
                                                ->where('status','reopen')->count();
                    $data['reopenwedday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Wednesday')
                                                ->where('status','reopen')->count();
                    $data['reopenthurday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Thursday')
                                                ->where('status','reopen')->count();
                    $data['reopenfriday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Friday')
                                                ->where('status','reopen')->count();
                    $data['reopensatday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Saturday')
                                                ->where('status','reopen')->count();
                    $data['reopensunday'] = Ticket::where('assign_to',$user->id)->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->whereDate('update_date','>=', $request->from_date)->whereDate('update_date','<=', $request->to_date)
                                                ->where('update_day','Sunday')
                                                ->where('status','reopen')->count();
        
                    $data['notification'] = DB::table('notifications')->count();
                }
array_push($data2,$data);
            
        }
            return view('admin.chart',compact('cities','category','subcategory','data2','users') );
        }
    
}
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
