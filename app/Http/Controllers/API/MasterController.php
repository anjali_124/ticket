<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\City;
use App\Models\Ticket;
use App\Models\Category;
use App\Models\SubCategory;
use App\Models\TypeOfPlant;
use Hash;
use Auth;
class MasterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $reqbody['name']='required';
        $reqbody['driving_licence']='required|unique:users,driving_licence';
        $reqbody['type_of_plant_id']='required';
        $reqbody['email']='required';
        $reqbody['phone_no']='required';
        $reqbody['count_of_plant']='required';  
        $reqbody['city_id']='required';  
        $validation = $this->validation($request,$reqbody);
        if($validation) return $validation;
        $params = $request->all();
        $params['username']=$request->name;
        $params['password']=Hash::make('123456');
        $params['role']='customer';
        unset($params['_token']);
        User::create($params);
        $customer = User::where('driving_licence',$request->driving_licence)->first();
        $customer1 = [
            'id' => $customer->id,
            'name' => $customer->name,
            'type_of_plant_id' => $customer->type_of_plant_id,
            'email' => $customer->email,
            'phone_no' => $customer->phone_no,
            'count_of_plant' => $customer->count_of_plant,
            'city_id' => $customer->city_id,
            'driving_licence' => $customer->driving_licence,
            'role' => $customer->role,
            'image'=>asset('images/avatar.png'),
        ];
        $result['customer'] =$customer1;
        return $this->sendResponse($result,"Register successfully");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function userLogin(Request $request)
    {
        $reqbody['driving_licence'] = 'required';
        $validation = $this->validation($request,$reqbody);
        if($validation) return $validation;
        $customer = User::where('driving_licence',$request->driving_licence)->get();
        if(count($customer) !=0){
                $customer = User::where('driving_licence',$request->driving_licence)->first();
                $customer1 = [
                    'id' => $customer->id,
                    'name' => $customer->name,
                    'type_of_plant_id' => $customer->type_of_plant_id,
                    'email' => $customer->email,
                    'phone_no' => $customer->phone_no,
                    'count_of_plant' => $customer->count_of_plant,
                    'city_id' => $customer->city_id,
                    'driving_licence' => $customer->driving_licence,
                    'role' => $customer->role,
                    'image'=>asset('images/avatar.png'),
                ];
                $result['customer'] =$customer1;
                $token = $customer->createToken('MyApp')->accessToken;
                $result['token'] = $token;
                return $this->sendResponse($result,"Login successfully");
        }else{
            return $this->sendError1("Invalid Driving Licence");
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function technicianloginpost(Request $request)
    {
        $data['email'] = 'required';
        $data['password'] = 'required';
        $validation = $this->validation($request, $data);
        if ($validation) return $validation;
            if(Auth::attempt(['email'=>$request->email, 'password'=>$request->password ])){ 
                $customer = User::where('email',$request->email)->first();
                if($customer->role == 'technician'){
                    $customer1 = [
                        'id' => $customer->id,
                        'name' => $customer->name,
                        'username' => $customer->username,
                        'email' => $customer->email,
                        'phone_no' => $customer->phone_no,
                        'role' => $customer->role,
                        'image'=>$customer->image,
                    ];
                    $result['technician'] =$customer1;
                    $token = $customer->createToken('MyApp')->accessToken;
                    $result['token'] = $token;
                    return $this->sendResponse($result,"Login successfully");
                }
                else{
                    return $this->sendError1("Invalid credentials");
                }
            }
        else{
            return $this->sendError1("Invalid credentials");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function cityShow()
    {
        $data=City::Where('status','active')->get();
        $result['Cities'] =$data;
        return $this->sendResponse($result,"My Cities List Get successfully");
    }
    public function categoryShow()
    {
        $data=Category::Where('status','active')->get();
        $result['Category'] =$data;
        return $this->sendResponse($result,"My Category List Get successfully");
    }
    public function subCategoryShow()
    {
        $data=SubCategory::Where('status','active')->get();
        $result['SubCategory'] =$data;
        return $this->sendResponse($result,"My SubCategory List Get successfully");
    }
    public function PlantShow()
    {
        $data=TypeOfPlant::Where('status','active')->get();
        $result['Type Of Plant'] =$data;
        return $this->sendResponse($result,"My Plant List Get successfully");
    }
    public function getUserDetail(Request $request)
    {
        $customer = User::where('driving_licence',$request->driving_licence)->first();
        
        if(isset($customer)){
        $result['customer'] =[
            'id' => $customer->id,
            'name' => $customer->name,
            'type_of_plant_id' => $customer->TypeOfPlant->name,
            'email' => $customer->email,
            'phone_no' => $customer->phone_no,
            'count_of_plant' => $customer->count_of_plant,
            'city' => $customer->City->name,
            'driving_licence' => $customer->driving_licence,
            'role' => $customer->role,
            'image'=>asset('images/avatar.png'),
        ];

        return $this->sendResponse($result,"fetch user detail");
        }
        else{
            $result['driving_licence'] =$request->driving_licence;

        return $this->sendError1($result,"Not found");
        }
    }
    public function getTechnicianDetail(Request $request)
    {
        $customer = User::where('email',$request->email)->first();
        if(isset($customer)){
        $result['customer'] =[
            'id' => $customer->id,
            'name' => $customer->name,
            'email' => $customer->email,
            'username' => $customer->username,
            'phone_no' => $customer->phone_no,
            'role' => $customer->role,
            'image'=>asset('images/avatar.png'),
        ];

        return $this->sendResponse($result,"fetch technician detail");
        }
        else{
            $result['email'] =$request->email;

        return $this->sendError1($result,"Not found technician");
        }
    }
    public function updateUserDetail(Request $request)
    {
        $data['id']='required';
        $data['name']='required';
        $data['driving_licence']='required';
        $data['type_of_plant_id']='required';
        $data['email']='required';
        $data['phone_no']='required';
        $data['count_of_plant']='required';  
        $data['city_id']='required';  
        $validation = $this->validateform($request, $data);
        if ($validation) return $validation;
        $params = $request->all();
        unset($params['_token']);
        unset($params['image']);
        if($request->hasFile('image')){
            $params['image'] = uploadFile($request,'image','profile-image/');
            }
        $city = User::whereId($request->id)->first();
        $city->update($params);
        $customer = User::where('email',$request->email)->first();
        $result['customer'] =[
            'id' => $customer->id,
            'name' => $customer->name,
            'type_of_plant_id' => $customer->type_of_plant_id,
            'email' => $customer->email,
            'phone_no' => $customer->phone_no,
            'count_of_plant' => $customer->count_of_plant,
            'city_id' => $customer->city_id,
            'driving_licence' => $customer->driving_licence,
            'role' => $customer->role,
            'image'=>asset($customer->image),
        ];

        return $this->sendResponse($result,"Update user detail successfully");
    }
    public function updateTechnicianDetail(Request $request)
    {
        $data['id']='required';
        $data['name']='required';
        $data['username']='required';
        $data['phone_no']='required';
        $data['email']='required';
        $validation = $this->validateform($request, $data);
        if ($validation) return $validation;
        $params = $request->all();
        unset($params['_token']);
        unset($params['image']);
        if($request->hasFile('image')){
            $params['image'] = uploadFile($request,'image','profile-image/');
            }
        $city = User::whereId($request->id)->first();
        $city->update($params);
        $customer = User::where('email',$request->email)->first();
        $result['customer'] =[
            'id' => $customer->id,
            'name' => $customer->name,
            'email' => $customer->email,
            'phone_no' => $customer->phone_no,
            'username' => $customer->username,
            'role' => $customer->role,
            'image'=>$customer->image,
        ];

        return $this->sendResponse($result,"Update user detail successfully");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function countofdashorad(Request $request)
    {
        $user =User::where('id', $request->id)->first();
        if($user->role == "technician"){
            $result['open'] = Ticket::where('status','open')->where('assign_to',$user->id)->count();
            $result['waiting'] = Ticket::where('status','waiting')->where('assign_to',$user->id)->count();
            $result['visit'] = Ticket::where('status','visit')->where('assign_to',$user->id)->count();
            $result['revisit'] = Ticket::where('status','revisit')->where('assign_to',$user->id)->count();
            $result['close'] = Ticket::where('status','closed')->where('assign_to',$user->id)->count();
        }
      
        if($user->role == "customer"){
            $result['open'] = Ticket::where('status','open')->where('user_id',$user->id)->count();
            $result['waiting'] = Ticket::where('status','waiting')->where('user_id',$user->id)->count();
            $result['visit'] = Ticket::where('status','visit')->where('user_id',$user->id)->count();
            $result['revisit'] = Ticket::where('status','revisit')->where('user_id',$user->id)->count();
            $result['close'] = Ticket::where('status','closed')->where('user_id',$user->id)->count();
        }
        return $this->sendResponse($result,"Count fetch successfully");
    }
}
