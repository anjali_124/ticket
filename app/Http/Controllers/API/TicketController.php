<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Ticket;
class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        $user=User::Where('id',$request->id)->first();
        
        if($user->role == 'technician'){
            $data=Ticket::where('assign_to',$user->id)->orderBy('id','desc')->get(['id','title','ticket_number','created','status','created_by']);
        }
        if($user->role == 'customer'){
        $data=Ticket::where('user_id',$user->id)->orderBy('id','desc')->get(['id','title','ticket_number','created','status']);
        }
        $result['Tickets'] =$data;
        return $this->sendResponse($result,"My Ticket List Get successfully");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $data['title']='required';
        $data['user_id']='required';
        $data['created_by']='required';
        $data['description']='required';
        $validation = $this->validation($request, $data);
        if ($validation) return $validation;
        $images = uploadMultipleFile($request,'image','ticket/');

        $params = $request->all();
        unset($params['_token']);
        unset($params['image']);
        $params['status']='open';
        $params['created']=date("Y-m-d");
        $params['update_date']=date("Y-m-d");
        $user = User::where('id',$request->user_id)->first();
        $params['city']=$user->City->name;
        $ticket =Ticket::create($params);
        if(count($images)){
            $ticket->TicaketImage()->delete();
            $user_id=$request->user_id;
            foreach($images as $image)
            $ticket->TicaketImage()->create(['image'=>$image,'user_id'=>$user_id]);
        }
        $ticket_number=generateotp($ticket->id);
        $ticket->update(['ticket_number'=>$ticket_number]);
        $result['Ticket'] =$ticket;
        return $this->sendResponse($result,"My Ticket Create successfully");
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
