<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TypeOfPlant;

class TypeOfPlantController extends Controller
{ /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
   public function index()
   {
       $data['typeofplants'] = TypeOfPlant::orderBy('id','Desc')->paginate(10);
       return view('admin.typeofplant.index' ,$data);
   }

   /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
   public function create()
   {
       return view('admin.typeofplant.create');
   }

   /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
   public function store(Request $request)
   {
       
       $data['name']='required|unique:cities,name';
       $data['status']='required';  
       $validation = $this->validateform($request, $data);
       if ($validation) return $validation;
       $params = $request->all();
       unset($params['_token']);
       TypeOfPlant::create($params);
       return redirect(route('typeofplant'))->with('message', 'success|creado con éxito');
   }

   /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function show($id)
   {
       //
   }

   /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function edit($id)
   {
       $city['typeofplant'] = TypeOfPlant::where('id',$id)->first();
       return view('admin.typeofplant.edit',$city);
   }

   /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function update(Request $request)
   {
       $data['name']='required';  
       $data['status']='required';  

       $validation = $this->validateform($request, $data);
       if ($validation) return $validation;
       $params = $request->all();
       unset($params['_token']);
       $city = TypeOfPlant::whereId($request->id)->first();
     
       $city->update($params);
       return redirect(route('typeofplant'))->with('message', 'success|actualizado correctamente');
   }


   /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function destroy($id)
   {
    TypeOfPlant::whereId($id)->delete();
       return redirect(route('typeofplant'))->with('message', 'success|Eliminar con éxito ');
   }
}
