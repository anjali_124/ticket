<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\SubCategory;
class SubCategoryController extends Controller
{ /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
   public function index()
   {
       $data['subcategory'] = SubCategory::orderBy('id','Desc')->paginate(10);
       return view('admin.subcategory.index' ,$data);
   }

   /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
   public function create()
   {

       $data['category'] = Category::where('status','active')->orderBy('id','Desc')->get();
       return view('admin.subcategory.create',$data);
   }

   /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
   public function store(Request $request)
   {
       
       $data['name']='required';
       $data['category_id']='required';  
       $data['status']='required';  
       $validation = $this->validateform($request, $data);
       if ($validation) return $validation;
       $params = $request->all();
       unset($params['_token']);
       SubCategory::create($params);
       return redirect(route('subcategory'))->with('message', 'success|creado con éxito');
   }
 
   /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function show($id)
   {
       //
   }

   /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function edit($id)
   {
       $city['category'] = Category::where('status','active')->get();
       $city['subcategory'] = SubCategory::where('id',$id)->first();
       return view('admin.subcategory.edit',$city);
   }

   /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function update(Request $request)
   {
       $data['name']='required';  
       $data['status']='required';  

       $validation = $this->validateform($request, $data);
       if ($validation) return $validation;
       $params = $request->all();
       unset($params['_token']);
       $city = SubCategory::whereId($request->id)->first();
     
       $city->update($params);
       return redirect(route('subcategory'))->with('message', 'success|actualizado correctamente');
   }


   /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function destroy($id)
   {
    Category::whereId($id)->delete();
       return redirect(route('subcategory'))->with('message', 'success|Eliminar con éxito ');
   }
}
