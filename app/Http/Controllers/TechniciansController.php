<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Hash;
use Auth;
use App\Models\Ticket;
use App\Models\TicaketImage;
class TechniciansController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers['customers'] = User::where('role', 'technician')->paginate(10);
        return view('admin.technicians.index',$customers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.technicians.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data['name']='required';
        $data['username']='required';
        $data['email']='required|unique:users,email|regex:/(.+)@(.+)\.(.+)/i|email';
        $data['phone_no']='required';
        $data['password']='required';  
        $data['confirm_password']='required|same:password';  
        $validation = $this->validateform($request, $data);
        if ($validation) return $validation;
        $params = $request->all();
        $params['driving_licence']='0';
        $params['count_of_plant']='0';
        $params['type_of_plant_id']='0';
        $params['city_id']='0';
        $params['password']=Hash::make($request->password);
        $params['role']='technician';
        unset($params['_token']);
        unset($params['confirm_password']);
        User::create($params);
        if(Auth::user()){
            return redirect(route('list-technicians'))->with('message', 'success|creado con éxito');
        }
        else{
            return redirect()->back()->with('message', 'success|creado con éxito');
        }
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user['customer'] = User::where('id',$id)->first();
        return view('admin.technicians.edit',$user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data['name']='required';
        $data['username']='required';
        $data['email']='required|regex:/(.+)@(.+)\.(.+)/i|email';
        $data['phone_no']='required';
        $validation = $this->validateform($request, $data);
        if ($validation) return $validation;
       
        $params = $request->all();
        unset($params['_token']);
        $city = User::whereId($request->id)->first();
        $city->update($params);
        return redirect(route('list-technicians'))->with('message', 'success|creado con éxito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::whereId($id)->delete();
        return redirect(route('list-technicians'))->with('message', 'success|Eliminar con éxito');
    }
}
