<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function sendResponse($result, $message)
    {
        $response = [
            'status' => true,
            'result'    => $result,
            'message' => $message,
            'statuscode' => 200,
        ];

        return response()->json($response, 200);
    }


    public function sendError($error, $errorMessages = [], $code = 401)
    {
        $response = [
            'status' => false,
            'message' => $error,
        ];

        if(!empty($errorMessages)){
            $response['result'] = $errorMessages;
        }

        return response()->json($response, $code);
    }
    public function sendError1($result,$error, $code = 400)
    {
        $response = [
            'status' => false,
            'result'    => $result,
            'message' => $error,
            'statuscode' => 404,
        ];

        // if(!empty($errorMessages)){
        //     $response['result'] = $errorMessages;
        // }

        return response()->json($response, $code);
    }
    public function validateform(Request $request,$data)
    {
        $customMessages = ['required' => 'Este campo es obligatorio.'];
        $validator = Validator::make($request->all(),$data,$customMessages);
    
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }else{
            return false;
        }
    }

    public function validation(Request $request,$data)
    {
        $validator = Validator::make($request->all(),$data);
    
        if ($validator->fails()) {
            return $this->sendError('Validation Error!',$validator->errors());
        }else{
            return false;
        }
    }
}
