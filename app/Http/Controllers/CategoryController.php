<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\SubCategory;
class CategoryController extends Controller
{   /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
   public function index()
   {
       $data['category'] = Category::orderBy('id','Desc')->paginate(10);
       return view('admin.category.index' ,$data);
   }

   /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
   public function create()
   {
       return view('admin.category.create');
   }

   /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
   public function store(Request $request)
   {
       
       $data['name']='required|unique:categories,name';
       $data['status']='required';  
       $validation = $this->validateform($request, $data);
       if ($validation) return $validation;
       $params = $request->all();
       if($request->hasFile('image')){
           $params['image'] = uploadFile($request,'image','city-image/');
           }
       unset($params['_token']);
       Category::create($params);
       return redirect(route('category'))->with('message', 'success|creado con éxito');
   }

   /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function show($id)
   {
       //
   }

   /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function edit($id)
   {
       $city['category'] = Category::where('id',$id)->first();
       return view('admin.category.edit',$city);
   }

   /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function update(Request $request)
   {
       $data['name']='required';  
       $data['status']='required';  

       $validation = $this->validateform($request, $data);
       if ($validation) return $validation;
       $params = $request->all();
       unset($params['_token']);
       $city = Category::whereId($request->id)->first();
     
       $city->update($params);
       return redirect(route('category'))->with('message', 'success|actualizado correctamente');
   }


   /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function destroy($id)
   {
    $subcategory = SubCategory::where('category_id', $id)->get();
    foreach($subcategory as $sub){
        $sub->delete();
    }
    Category::whereId($id)->delete();
       return redirect(route('category'))->with('message', 'success|Eliminar con éxito ');
   }
}
