<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Ticket;
use App\Models\TicaketImage;
use App\Models\TicketTag;
use App\Models\TicketImageReply;
use App\Models\User;
use App\Models\Category;
use App\Models\SubCategory;
use App\Models\TicketConversation;
use Notification;
use App\Notifications\OffersNotification;
use Auth;
use App\Models\City;
use Carbon\Carbon;
class TicketContorller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user=Auth::user();
        if($user->role == 'admin'){
            $data['user']=User::where('role','technician')->get();
            $data['tickets']=Ticket::orderBy('id','desc')->where('status','open')->paginate(10);
            $data['cities']=City::where('status','active')->get();
        }
      if($user->role == 'technician'){
            $data['tickets']=Ticket::where('assign_to',$user->id)->orderBy('id','desc')->paginate(10);
            $data['cities']=City::where('status','active')->get();
        }
        if($user->role == 'customer'){
        $data['tickets']=Ticket::where('user_id',$user->id)->orderBy('id','desc')->paginate(10);
        $data['cities']=City::where('status','active')->get();
        }
        return view('user.showticket',$data);
    }

    
    public function assignindex()
    {
        $user=Auth::user();
        if($user->role == 'admin'){
            $data['user']=User::where('role','technician')->get();
            $data['tickets']=Ticket::orderBy('id','desc')->where('status','waiting')->orwhere('status','visit')->orwhere('status','revisit')->orwhere('status','closed')->orwhere('status','reopen')->paginate(10);
            $data['cities']=City::where('status','active')->get();
        }
        return view('user.assignticket',$data);
    }
    public function alltickets()
    {
        $user=Auth::user();
        $data['category'] =Category::where('status','active')->get();
        $data['subcategory'] =SubCategory::where('status','active')->get();
        if($user->role == 'admin'){
            $data['user']=User::where('role','technician')->get();

            $data['tickets']=Ticket::orderBy('id','desc')->paginate(10);
            $data['cities']=City::where('status','active')->get();
        }
      if($user->role == 'technician'){
        $data['user']=User::where('role','technician')->get();

            $data['tickets']=Ticket::where('assign_to',$user->id)->orderBy('id','desc')->paginate(10);
            $data['cities']=City::where('status','active')->get();
        }
        if($user->role == 'customer'){
        $data['tickets']=Ticket::where('user_id',$user->id)->orderBy('id','desc')->paginate(10);
        $data['cities']=City::where('status','active')->get();
        }
        return view('user.totalticket',$data);
    }
    public function allticketStatus($status)
    {
        $user=Auth::user();
        if($user->role == 'admin'){
            $data['user']=User::where('role','technician')->get();
            $data['tickets']=Ticket::where('status',$status)->orderBy('id','desc')->paginate(10);
            $data['cities']=City::where('status','active')->get();
        }
        if($user->role == 'technician'){
            $data['tickets']=Ticket::where('assign_to',$user->id)->where('status',$status)->orderBy('id','desc')->paginate(10);
            $data['cities']=City::where('status','active')->get();
        }
        if($user->role == 'customer'){
            $data['tickets']=Ticket::where('user_id',$user->id)->where('status',$status)->orderBy('id','desc')->paginate(10);
            $data['cities']=City::where('status','active')->get();
        }
        return view('user.totalticket',$data);
    }


    public function filtertickets(Request $request)
    {
        $data['user']=User::where('role','technician')->get();
        $data['category'] =Category::where('status','active')->get();
        $data['subcategory'] =SubCategory::where('status','active')->get();
        $user=Auth::user();
        $data['cities']=City::where('status','active')->get();
        if($user->role == 'admin'){
            if($request->search && $request->status && $request->city && $request->category_id && $request->subcategory_id && $request->ticket_status){
                $data['tickets']=Ticket::where('title','like', '%'.$request->search.'%')->
                where('status',$request->status)->
                where('city',$request->city)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->where('ticket_status',$request->ticket_status)->orderBy('id','desc')->paginate(10);
            }
            if(!$request->search && !$request->status && $request->city && !$request->category_id && $request->subcategory_id && !$request->ticket_status){
                $data['tickets']=Ticket::where('city',$request->city)->where('subcategory_id',$request->subcategory_id)->orderBy('id','desc')->paginate(10);
            }
            if(!$request->search && !$request->status && $request->city && !$request->category_id && !$request->subcategory_id && $request->ticket_status){
                $data['tickets']=Ticket::where('city',$request->city)->where('ticket_status',$request->ticket_status)->orderBy('id','desc')->paginate(10);
            }
            if(!$request->search && !$request->status && $request->city && $request->category_id && $request->subcategory_id && $request->ticket_status){
                $data['tickets']=Ticket::where('city',$request->city)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->where('ticket_status',$request->ticket_status)->orderBy('id','desc')->paginate(10);
            }
            if(!$request->search && $request->status && !$request->city && !$request->category_id && $request->subcategory_id && $request->ticket_status){
                $data['tickets']=Ticket::where('subcategory_id',$request->subcategory_id)->where('status',$request->status)->orderBy('id','desc')->paginate(10);
            }
            if(!$request->search && $request->status && !$request->city && $request->category_id && !$request->subcategory_id && $request->ticket_status){
                $data['tickets']=Ticket::where('category_id',$request->category_id)->where('ticket_status',$request->ticket_status)->orderBy('id','desc')->paginate(10);
            }
            if(!$request->search && $request->status && !$request->city && !$request->category_id && !$request->subcategory_id && $request->ticket_status){
                $data['tickets']=Ticket::where('status',$request->status)->where('ticket_status',$request->ticket_status)->orderBy('id','desc')->paginate(10);
            }
            if(!$request->search && !$request->status && !$request->city && !$request->category_id && !$request->subcategory_id && $request->ticket_status){
              
                $data['tickets']=Ticket::where('ticket_status', $request->ticket_status)->orderBy('id','desc')->paginate(10);
            }
            if(!$request->search && !$request->status && !$request->city && !$request->category_id && $request->subcategory_id && !$request->ticket_status){
                $data['tickets']=Ticket::where('subcategory_id', $request->subcategory_id)->orderBy('id','desc')->paginate(10);
            }
            if(!$request->search && !$request->status && !$request->city && $request->category_id && !$request->subcategory_id && !$request->ticket_status){
                $data['tickets']=Ticket::where('category_id', $request->category_id)->orderBy('id','desc')->paginate(10);
            }
            if(!$request->search && !$request->status && !$request->city && !$request->category_id && $request->subcategory_id && $request->ticket_status){
                $data['tickets']=Ticket::where('subcategory_id', $request->subcategory_id)->where('ticket_status',$request->ticket_status)->orderBy('id','desc')->paginate(10);
            }
            if(!$request->search && !$request->status && !$request->city && $request->category_id && !$request->subcategory_id && $request->ticket_status){
                $data['tickets']=Ticket::where('category_id', $request->category_id)->where('ticket_status',$request->ticket_status)->orderBy('id','desc')->paginate(10);
            }
            if(!$request->search && !$request->status && !$request->city && $request->category_id && $request->subcategory_id && !$request->ticket_status){
                $data['tickets']=Ticket::where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->orderBy('id','desc')->paginate(10);
            }
            if(!$request->search && !$request->status && !$request->city && !$request->category_id && !$request->subcategory_id && !$request->ticket_status){
                $data['tickets']=Ticket::orderBy('id','desc')->paginate(10);
            }
            if($request->search && !$request->status && !$request->city && $request->category_id && $request->subcategory_id && !$request->ticket_status){
                $data['tickets']=Ticket::where('title','like', '%'.$request->search.'%')->where('subcategory_id', $request->subcategory_id)->where('category_id', $request->category_id)->where('ticket_status',$request->ticket_status)->orderBy('id','desc')->paginate(10);
            }
            if($request->search && !$request->status && !$request->city && $request->category_id && !$request->subcategory_id && $request->ticket_status){
                $data['tickets']=Ticket::where('title','like', '%'.$request->search.'%')->where('category_id', $request->category_id)->where('ticket_status',$request->ticket_status)->orderBy('id','desc')->paginate(10);
            }
            if($request->search && !$request->status && !$request->city && !$request->category_id && $request->subcategory_id && $request->ticket_status){
                $data['tickets']=Ticket::where('title','like', '%'.$request->search.'%')->where('subcategory_id', $request->subcategory_id)->where('ticket_status',$request->ticket_status)->orderBy('id','desc')->paginate(10);
            }
            if(!$request->search && $request->status && !$request->city && $request->category_id && $request->subcategory_id && !$request->ticket_status){
                $data['tickets']=Ticket::where('status',$request->status)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->orderBy('id','desc')->paginate(10);
            }
            if(!$request->search && $request->status && !$request->city && $request->category_id && !$request->subcategory_id && $request->ticket_status){
                $data['tickets']=Ticket::where('status',$request->status)->where('category_id', $request->category_id)->where('ticket_status',$request->ticket_status)->orderBy('id','desc')->paginate(10);
            }
            if(!$request->search && $request->status && !$request->city && !$request->category_id && $request->subcategory_id && $request->ticket_status){
                $data['tickets']=Ticket::where('status',$request->status)->where('subcategory_id', $request->subcategory_id)->where('ticket_status',$request->ticket_status)->orderBy('id','desc')->paginate(10);
            }
            if(!$request->search && !$request->status && !$request->city && $request->category_id && $request->subcategory_id && $request->ticket_status){
                $data['tickets']=Ticket::where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->where('ticket_status',$request->ticket_status)->orderBy('id','desc')->paginate(10);
            }
            if(!$request->search && !$request->status && $request->city && $request->category_id && $request->subcategory_id && !$request->ticket_status){
                $data['tickets']=Ticket::where('city',$request->city)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->orderBy('id','desc')->paginate(10);
            }
            if(!$request->search && !$request->status && $request->city && !$request->category_id && $request->subcategory_id && $request->ticket_status){
                $data['tickets']=Ticket::where('city',$request->city)->where('subcategory_id', $request->subcategory_id)->where('ticket_status',$request->ticket_status)->orderBy('id','desc')->paginate(10);
            }
            if(!$request->search && !$request->status && $request->city && $request->category_id && !$request->subcategory_id && $request->ticket_status){
                $data['tickets']=Ticket::where('city',$request->city)->where('subcategory_id', $request->subcategory_id)->where('ticket_status',$request->ticket_status)->orderBy('id','desc')->paginate(10);
            }
            if(!$request->search && $request->status && $request->city && $request->category_id && $request->subcategory_id && !$request->ticket_status){
                $data['tickets']=Ticket::where('status',$request->status)->
                where('city',$request->city)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->orderBy('id','desc')->paginate(10);
            }
            if(!$request->search && $request->status && $request->city && !$request->category_id && $request->subcategory_id && $request->ticket_status){
                $data['tickets']=Ticket::where('status',$request->status)->
                where('city',$request->city)->where('subcategory_id', $request->subcategory_id)->where('ticket_status',$request->ticket_status)->orderBy('id','desc')->paginate(10);
            }
            if(!$request->search && $request->status && $request->city && $request->category_id && !$request->subcategory_id && $request->ticket_status){
                $data['tickets']=Ticket::where('status',$request->status)->
                where('city',$request->city)->where('category_id', $request->category_id)->where('ticket_status',$request->ticket_status)->orderBy('id','desc')->paginate(10);
            }
            if($request->search && $request->status && !$request->city && $request->category_id && $request->subcategory_id && $request->ticket_status){
                $data['tickets']=Ticket::where('title','like', '%'.$request->search.'%')->
                where('status',$request->status)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->where('ticket_status',$request->ticket_status)->orderBy('id','desc')->paginate(10);
            }
            if(!$request->search && $request->status && $request->city && $request->category_id && $request->subcategory_id && $request->ticket_status){
                $data['tickets']=Ticket::where('status',$request->status)->
                where('city',$request->city)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->where('ticket_status',$request->ticket_status)->orderBy('id','desc')->paginate(10);
            }
            if($request->search && !$request->status && $request->city && $request->category_id && $request->subcategory_id && $request->ticket_status){
                $data['tickets']=Ticket::where('title','like', '%'.$request->search.'%')->
                where('city',$request->city)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->where('ticket_status',$request->ticket_status)->orderBy('id','desc')->paginate(10);
            }
            if($request->search && $request->status && $request->city && $request->category_id && $request->subcategory_id && !$request->ticket_status){
                $data['tickets']=Ticket::where('title','like', '%'.$request->search.'%')->
                where('status',$request->status)->
                where('city',$request->city)->where('category_id', $request->category_id)->where('subcategory_id', $request->subcategory_id)->orderBy('id','desc')->paginate(10);
            }
            if($request->search && $request->status && $request->city && $request->category_id && !$request->subcategory_id && $request->ticket_status){
                $data['tickets']=Ticket::where('title','like', '%'.$request->search.'%')->
                where('status',$request->status)->
                where('city',$request->city)->where('category_id', $request->category_id)->where('ticket_status',$request->ticket_status)->orderBy('id','desc')->paginate(10);
            }
            if($request->search && $request->status && $request->city && !$request->category_id && $request->subcategory_id && $request->ticket_status){
                $data['tickets']=Ticket::where('title','like', '%'.$request->search.'%')->
                where('status',$request->status)->
                where('city',$request->city)->where('subcategory_id', $request->subcategory_id)->where('ticket_status',$request->ticket_status)->orderBy('id','desc')->paginate(10);
            }
            if(!$request->search && !$request->status && !$request->city && !$request->category_id && !$request->subcategory_id && !$request->ticket_status){
                $data['tickets']=Ticket::orderBy('id','desc')->paginate(10);
            }
            if(!$request->search && !$request->status && $request->city && !$request->category_id && !$request->subcategory_id && !$request->ticket_status){
                $data['tickets']=Ticket::where('city',$request->city)->orderBy('id','desc')->paginate(10);
            }
            if(!$request->search && $request->status && !$request->city && !$request->category_id && !$request->subcategory_id && !$request->ticket_status){
                $data['tickets']=Ticket::where('status',$request->status)->orderBy('id','desc')->paginate(10);
            }
            if($request->search && !$request->status && !$request->city && !$request->category_id && !$request->subcategory_id && !$request->ticket_status){
                $data['tickets']=Ticket::where('title','like', '%'.$request->search.'%')->orderBy('id','desc')->paginate(10);
            }
            if(!$request->search && $request->status && $request->city && !$request->category_id && !$request->subcategory_id && !$request->ticket_status){
                $data['tickets']=Ticket::where('status',$request->status)->
                where('city',$request->city)->orderBy('id','desc')->paginate(10);
            }
            if($request->search && $request->status && !$request->city && !$request->category_id && !$request->subcategory_id && !$request->ticket_status){
                $data['tickets']=Ticket::where('title','like', '%'.$request->search.'%')->
                where('status',$request->status)->orderBy('id','desc')->paginate(10);
            }
            if($request->search && !$request->status && $request->city && !$request->category_id && !$request->subcategory_id && !$request->ticket_status){
                $data['tickets']=Ticket::where('title','like', '%'.$request->search.'%')->
                where('city',$request->city)->orderBy('id','desc')->paginate(10);
            }
            if($data['tickets'] == ''){
                $data['tickets']=Ticket::where('title','like', '%'.$request->search.'%')->
                orWhere('status',$request->status)->
                orWhere('city',$request->city)->orWhere('category_id', $request->category_id)->orWhere('subcategory_id', $request->subcategory_id)->orwhere('ticket_status',$request->ticket_status)->orderBy('id','desc')->paginate(10);
            }
            $data['cities']=City::where('status','active')->paginate(10);

        }
        if($user->role == 'technician'){
            if($request->search && $request->status && $request->city){
                $data['tickets']=Ticket::where('assign_to',$user->id)->where('title','like', '%'.$request->search.'%')->
                where('status',$request->status)->
                where('city',$request->city)->orderBy('id','desc')->paginate(10);
            }
            if(!$request->search && !$request->status && !$request->city){
                $data['tickets']=Ticket::where('assign_to',$user->id)->orderBy('id','desc')->paginate(10);
            }

            if(!$request->search && !$request->status && $request->city){
                $data['tickets']=Ticket::where('assign_to',$user->id)->where('city',$request->city)->orderBy('id','desc')->paginate(10);
            }
            
            if(!$request->search && $request->status && !$request->city){
                $data['tickets']=Ticket::where('assign_to',$user->id)->where('status',$request->status)->orderBy('id','desc')->paginate(10);
            }
            if($request->search && !$request->status && !$request->city){
                $data['tickets']=Ticket::where('assign_to',$user->id)->where('title','like', '%'.$request->search.'%')->orderBy('id','desc')->paginate(10);
            }
            if(!$request->search && $request->status && $request->city){
                $data['tickets']=Ticket::where('assign_to',$user->id)->where('status',$request->status)->
                where('city',$request->city)->orderBy('id','desc')->paginate(10);
            }
            if($request->search && $request->status && !$request->city){
                $data['tickets']=Ticket::where('assign_to',$user->id)->where('title','like', '%'.$request->search.'%')->
                where('status',$request->status)->orderBy('id','desc')->paginate(10);
            }
            if($request->search && !$request->status && $request->city){
                $data['tickets']=Ticket::where('assign_to',$user->id)->where('title','like', '%'.$request->search.'%')->
                where('city',$request->city)->orderBy('id','desc')->paginate(10);
            }
        }
        if($user->role == 'customer'){
            if($request->search && $request->status && $request->city){
                $data['tickets']=Ticket::where('user_id',$user->id)->where('title','like', '%'.$request->search.'%')->
                where('status',$request->status)->
                where('city',$request->city)->orderBy('id','desc')->paginate(10);
            }
            if(!$request->search && !$request->status && !$request->city){
                $data['tickets']=Ticket::where('user_id',$user->id)->orderBy('id','desc')->paginate(10);
            }

            if(!$request->search && !$request->status && $request->city){
                $data['tickets']=Ticket::where('user_id',$user->id)->where('city',$request->city)->orderBy('id','desc')->paginate(10);
            }
            
            if(!$request->search && $request->status && !$request->city){
                $data['tickets']=Ticket::where('user_id',$user->id)->where('status',$request->status)->orderBy('id','desc')->paginate(10);
            }
            if($request->search && !$request->status && !$request->city){
                $data['tickets']=Ticket::where('user_id',$user->id)->where('title','like', '%'.$request->search.'%')->orderBy('id','desc')->paginate(10);
            }
            if(!$request->search && $request->status && $request->city){
                $data['tickets']=Ticket::where('user_id',$user->id)->where('status',$request->status)->
                where('city',$request->city)->orderBy('id','desc')->paginate(10);
            }
            if($request->search && $request->status && !$request->city){
                $data['tickets']=Ticket::where('user_id',$user->id)->where('title','like', '%'.$request->search.'%')->
                where('status',$request->status)->orderBy('id','desc')->paginate(10);
            }
            if($request->search && !$request->status && $request->city){
                $data['tickets']=Ticket::where('user_id',$user->id)->where('title','like', '%'.$request->search.'%')->
                where('city',$request->city)->orderBy('id','desc')->paginate(10);
            }
        }
        return view('user.totalticket',$data);
    }
    public function indextech($id)
    {
        $user=Auth::user();
        $data['place']=Ticket::where('assign_to',$id)->paginate(10);
        $data['placeimgs']=TicaketImage::where('ticket_id',$id)->get();
        return view('user.showdetailticket',$data);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('user.createticket');
    }

    /**
     * Store a  newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data['title']='required';
        $data['editor']='required';
        $validation = $this->validateform($request, $data);
        if ($validation) return $validation;
       
        $images = uploadMultipleFile($request,'image','ticket/');
        $params = $request->all();
        unset($params['_token']);
        unset($params['editor']);
        unset($params['image']);
        $params['status']='open';
        $params['description']=$request->editor;
        $params['created']=date("Y-m-d");
        $params['update_date']=date("Y-m-d");
        $params['update_day']=date('l', strtotime(date("Y-m-d")));
        $user = User::where('id', $request->user_id)->first();
        $params['city']=$user->City->name;
        $ticket =Ticket::create($params);
        if(count($images)){
            $ticket->TicaketImage()->delete();
            $user_id=$request->user_id;
            foreach($images as $image)
            $ticket->TicaketImage()->create(['image'=>$image,'user_id'=>$user_id]);
        }
        $ticket_number=generateotp($ticket->id);
        $ticket->update(['ticket_number'=>$ticket_number]);
        $admin = User::find(1);
        $offerData = [  
            'id' =>    $ticket->id,
            'title' =>    $ticket->title,
            'username' =>    $user->name,
            'status' =>    $ticket->status,
            'assignto' =>    '',
        ];  
        Notification::send($admin, new OffersNotification($offerData));

        return redirect(route('alltickets'))->with('message', 'success|actualizado correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['place']=Ticket::where('id',$id)->first();
        $data['user']=User::where('id',$data['place']->user_id)->first();
        $data['placeimgs']=TicaketImage::where('ticket_id',$id)->get();
        $data['placetag']=TicketTag::where('ticket_id',$id)->get();
        $data['placetechimgs']=TicketImageReply::where('ticket_id',$id)->get();
        $data['placeconversation']=TicketConversation::where('ticket_id',$id)->get();
        return view('user.showdetailticket',$data);
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['place']=Ticket::where('id',$id)->first();
        $data['user']=User::where('role','technician')->get();
        $data['category']=Category::all();
        $data['subcategory']=SubCategory::all();
        $data['placeimgs']=TicaketImage::where('ticket_id',$id)->get();
        $data['placetag']=TicketTag::where('ticket_id',$id)->get();
        $data['placetechimgs']=TicketImageReply::where('ticket_id',$id)->get();
        $data['placeconversation']=TicketConversation::where('ticket_id',$id)->get();
        return view('user.editticket',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     public function filter(Request $request)
     {
        $user = Auth::user();
         if(!$request->from_date && !$request->to_date && $request->status ){
            if($user->role == "customer"){
                $events['tickets'] = Ticket::Where('status',$request->status)
                                            ->where('user_id',$user->id)
                                            ->paginate(10);
            }
            if($user->role == "admin"){
                $events['tickets'] = Ticket::orderBy('id','desc')->paginate(10);
            }
            if($user->role == "technician"){
                $events['tickets'] = Ticket::Where('status',$request->status)
                                            ->wwhere('assign_to',$user->id)
                                            ->paginate(10);
            }
            return view('user.showticket', $events);
         }
         if($request->from_date && !$request->to_date && $request->status ){
            if($user->role == "customer"){
                $events['tickets'] = Ticket::Where('status',$request->status)
                                            ->Where('created',$request->from_date)
                                            ->where('user_id',$user->id)
                                            ->paginate(10);
            }
            if($user->role == "admin"){
                $events['tickets'] = Ticket::Where('created',$request->from_date)->
                orderBy('id','desc')->paginate(10);
            }
            if($user->role == "technician"){
                $events['tickets'] = Ticket::Where('status',$request->status)
                                            ->Where('created',$request->from_date)
                                            ->where('assign_to',$user->id)
                                            ->paginate(10);
            }
            return view('user.showticket', $events);
         }
         if(!$request->from_date && $request->to_date && $request->status ){
            if($user->role == "customer"){
                $events['tickets'] = Ticket::Where('status',$request->status)
                                            ->Where('created',$request->to_date)
                                            ->where('user_id',$user->id)
                                            ->paginate(10);
            }
            if($user->role == "admin"){
                $events['tickets'] = Ticket::Where('created',$request->to_date)
                                            ->orderBy('id','desc')->paginate(10);
            }
            if($user->role == "technician"){
                $events['tickets'] = Ticket::Where('status', $request->status)
                                            ->Where('created',$request->to_date)
                                            ->where('assign_to',$user->id)
                                            ->paginate(10);
            }
            return view('user.showticket', $events);
         }
         if($request->from_date && $request->to_date && !$request->status ){
           
            if($user->role == "customer"){
              
                $events['tickets'] = Ticket::whereDate('created', '>=', $request->from_date)
                                            ->whereDate('created', '<=', $request->to_date)
                                            ->orderBy('id','desc')->where('user_id',$user->id)
                                            ->paginate(10);
            }
            if($user->role == "admin"){
                $events['tickets'] = Ticket::whereDate('created', '>=', $request->from_date)
                                            ->whereDate('created', '<=', $request->to_date)
                                            ->orderBy('id','desc')
                                            ->paginate(10);
            }
            if($user->role == "technician"){
                $events['tickets'] = Ticket::whereDate('created', '>=', $request->from_date)
                                            ->whereDate('created', '<=', $request->to_date)
                                            ->orderBy('id','desc')->where('assign_to',$user->id)
                                            ->paginate(10);
            }
            return view('user.showticket', $events);
         }
         if(!$request->from_date && $request->to_date && !$request->status ){
           
            if($user->role == "customer"){
                $events['tickets'] = Ticket::whereDate('created', $request->to_date)
                                            ->orderBy('id','desc')
                                            ->where('user_id',$user->id)->paginate(10);
            }
            if($user->role == "admin"){
                $events['tickets'] = Ticket::whereDate('created', $request->to_date)
                                            ->orderBy('id','desc')
                                            ->paginate(10);
            }
            if($user->role == "technician"){
                $events['tickets'] = Ticket::whereDate('created', $request->to_date)
                                            ->orderBy('id','desc')
                                            ->where('assign_to',$user->id)
                                            ->paginate(10);
            }
            return view('user.showticket', $events);
         }
         if($request->from_date && !$request->to_date && !$request->status ){
            if($user->role == "customer"){
                $events['tickets'] = Ticket::whereDate('created', $request->from_date)
                                            ->orderBy('id','desc')
                                            ->where('user_id',$user->id)
                                            ->paginate(10);
            }
            if($user->role == "admin"){
                $events['tickets'] = Ticket::whereDate('created', $request->from_date)
                                            ->orderBy('id','desc')
                                            ->paginate(10);
            }
            if($user->role == "technician"){
                $events['tickets'] = Ticket::whereDate('created', $request->from_date)
                                            ->orderBy('id','desc')
                                            ->where('assign_to',$user->id)
                                            ->paginate(10);

            }
            return view('user.showticket', $events);
         }
         if($request->from_date && $request->to_date && $request->status ){
            
            if($user->role == "customer"){
                $events['tickets'] = Ticket::Where('status',$request->status)
                                            ->whereDate('created', '>=', $request->from_date)
                                            ->whereDate('created', '<=', $request->to_date)
                                            ->where('user_id',$user->id)
                                            ->paginate(10);
    
            }
            if($user->role == "admin"){
                $events['tickets'] = Ticket::Where('status',$request->status)
                                            ->whereDate('created', '>=', $request->from_date)
                                            ->whereDate('created', '<=', $request->to_date)
                                            ->paginate(10);
    
            }
            if($user->role == "technician"){
                $events['tickets'] = Ticket::Where('status',$request->status)
                                            ->whereDate('created', '>=', $request->from_date)
                                            ->whereDate('created', '<=', $request->to_date)
                                            ->where('assign_to',$user->id)
                                            ->paginate(10);
            }
            return view('user.showticket', $events);
         }
         
         if(!$request->from_date && !$request->to_date && !$request->status  ){
            
            if($user->role == "customer"){
                $events['tickets'] = Ticket::where('user_id',$user->id)
                                            ->paginate(10);
    
            }
            if($user->role == "admin"){
                $events['tickets'] = Ticket::orderBy('id','desc')->paginate(10);
            }
            if($user->role == "technician"){
                $events['tickets'] = Ticket::where('assign_to',$user->id)
                                            ->paginate(10);
            }
            return view('user.showticket', $events);
         }
     }


    public function update(Request $request)
    {
        if($request->assign_to != 'admin'){
            $data['assign_to']='required';
            // $data['status']='required';
        }
            
        if(Auth::user()->role =="technician"){
            $data['description']='required';
            $data['category']='required';
            $data['subcategory']='required';
            $data['assign_to']='required';
        }
        if(Auth::user()->role=='admin'){
            $data['description']='required';
            $data['category']='required';
            $data['subcategory']='required';
            $data['ticket_status']='required';
        }
        if(Auth::user()->role =="customer"){
            $data['description']='required';
        }
        $validation = $this->validateform($request, $data);
        if ($validation) return $validation;
       
        $images = uploadMultipleFile($request,'image','ticket_tech/');
        $params = $request->all();
        unset($params['_token']);
        unset($params['image']);
        unset($params['tag']);
        unset($params['ticket_status']);
        $ticket2 =Ticket::where('id',$request->ticket_id)->first();
        $day = date('l', strtotime($request->visit_date));

        if(Auth::user()->role =="technician" ||Auth::user()->role =="admin"){
            $ticket =TicketConversation::create($params);
            $ticket3 = $ticket2->update(['ticket_status'=>$request->ticket_status,'category_id'=>$request->category,'subcategory_id'=>$request->subcategory,'update_date'=>date("Y-m-d"),'update_day'=>date('l', strtotime(date("Y-m-d")))]);
            $user = User::where('id',$ticket2->user_id)->first();
            $assigner = User::where('id',$request->assign_to)->first();
            $offerData = [  
                'id' =>    $ticket2->id,
                'title' =>    $ticket2->title,
                'username' =>    $user->name,
                'status' => $ticket2->status,
                'assignto' => $assigner->name,
            ];  
        Notification::send($user, new OffersNotification($offerData));
            if(isset($request->tag)){
                $ticket->TicketTag()->create(['name'=>$request->tag,'ticket_id'=>$request->ticket_id]);
                }
            if(count($images)){
                $ticket->TicketImageReply()->delete();
                $user_id=$request->user_id;
                foreach($images as $image)
                $ticket->TicketImageReply()->create(['image'=>$image,'user_id'=>$user_id,'assign_to'=>$request->assign_to,'ticket_id'=>$request->ticket_id]);
            }
        }
       
            return redirect(route('alltickets'))->with('message', 'success|actualizado correctamente');
    }

    public function updateTechnician(Request $request)
    {
        if($request->assign_to != 'admin'){
            $data['assign_to']='required';
        }
        $validation = $this->validateform($request, $data);
        if ($validation) return $validation;
       
        $params = $request->all();
        unset($params['_token']);
        $ticket2 =Ticket::where('id',$request->ticket_id)->first();
        $day = date('l', strtotime($request->visit_date));

        if(Auth::user()->role =="admin"){
        $assign_date= date('Y-m-d');
        $ticket4 = $ticket2->update(['status'=>$request->status,'assign_to'=>$request->assign_to,'assign_date'=>$assign_date,'update_date'=>date("Y-m-d"),'update_day'=>date('l', strtotime(date("Y-m-d")))]);
        
        $user = User::where('id',$ticket2->user_id)->first();
        $assigner = User::where('id',$request->assign_to)->first();
        $offerData = [  
            'id' =>    $ticket2->id,
            'title' =>    $ticket2->title,
            'username' =>    $user->name,
            'status' => $ticket2->status,
            'assignto' => $assigner->name,
        ];  
        Notification::send($user, new OffersNotification($offerData));
        Notification::send($assigner, new OffersNotification($offerData));
        }
        

        // if($ticketstatus == "waiting"){
        // return redirect(route('assign-ticket'))->with('message', 'success|actualizado correctamente');

        // }
        // else{
            return redirect()->back()->with('message', 'success|actualizado correctamente');
        // }
    }


    public function reopenTicket(Request $request)
    {
        $data['description']='required';
        $validation = $this->validateform($request, $data);
        if ($validation) return $validation;
        $params = $request->all();
        unset($params['_token']);
        unset($params['image']);
        unset($params['status']);
       
        $ticket1 =Ticket::where('id',$request->ticket_id)->first();
        $ticket2 = $ticket1->update(['status'=>$request->status]);
        $ticket =TicketConversation::create($params);
        $user = User::where('id',$ticket->user_id)->first();
        $assigner = User::where('id',$ticket->assign_to)->first();
        if(isset($assigner)){
            $offerData = [  
                'id' =>    $ticket1->id,
                'title' =>    $ticket1->title,
                'username' =>    $user->name,
                'status' => $ticket1->status,
                'assignto' => $assigner->name,
            ];  
         
        Notification::send($assigner, new OffersNotification($offerData));
        }
      
    return redirect(route('list-ticket'))->with('message', 'success|actualizado correctamente');
        
    }




    public function statusChange(Request $request){
        $ticket2 =Ticket::where('id',$request->ticket_id)->first();
        $ticket3 =TicketConversation::where('ticket_id',$request->ticket_id)->orderBy('id', 'desc')->first();
        $day = date('l', strtotime($request->visit_date));

        if(Auth::user()->role =="technician"){
            if(isset($ticket3)){
                if(isset($request->visit_date)){
                $ticket4 = $ticket3->update(['visit_date'=>$request->visit_date,'time'=>$request->time]);
                }}
           if(isset($request->visit_date)){
            $ticket5 = $ticket2->update(['status'=>$request->status,'visit_date'=>$request->visit_date,'category_id'=>$request->category,'subcategory_id'=>$request->subcategory,'visit_time'=>$request->time,'visit_day'=>$day,'update_date'=>date("Y-m-d"),'update_day'=>date('l', strtotime(date("Y-m-d")))]);
           }
           else{
            
            $ticket6 = $ticket2->update(['status'=>$request->status ,'update_date'=>date("Y-m-d"),'update_day'=>date('l', strtotime(date("Y-m-d")))]);
           }
            $user = User::where('id',$ticket2->user_id)->first();
            $assigner = User::where('id',$request->assign_to)->first();

            $offerData = [  
                'id' =>    $ticket2->id,
                'title' =>    $ticket2->title,
                'username' =>    $user->name,
                'status' => $ticket2->status,
                'assignto' => $assigner->name,
            ];  
            Notification::send($user, new OffersNotification($offerData));
        }
        
        if(Auth::user()->role =="admin"){
        $assign_date= date('Y-m-d');
        if(isset($ticket3)){
            if(isset($request->visit_date)){
            $ticket4 = $ticket3->update(['visit_date'=>$request->visit_date,'time'=>$request->time]);
            }}
       if(isset($request->visit_date)){
        $ticket5 = $ticket2->update(['status'=>$request->status,'visit_date'=>$request->visit_date,'category_id'=>$request->category,'subcategory_id'=>$request->subcategory,'visit_time'=>$request->time,'visit_day'=>$day,'update_date'=>date("Y-m-d"),'update_day'=>date('l', strtotime(date("Y-m-d")))]);
       }
       else{
        $ticket6 = $ticket2->update(['status'=>$request->status ,'update_date'=>date("Y-m-d"),'update_day'=>date('l', strtotime(date("Y-m-d")))]);
       }
        
        $user = User::where('id',$ticket2->user_id)->first();
        $assigner = User::where('id',$request->assign_to)->first();
        $offerData = [  
            'id' =>    $ticket2->id,
            'title' =>    $ticket2->title,
            'username' =>    $user->name,
            'status' => $ticket2->status,
            'assignto' => $assigner->name,
        ];  
        Notification::send($user, new OffersNotification($offerData));
        Notification::send($assigner, new OffersNotification($offerData));
    }
    return redirect(route('alltickets'))->with('message', 'success|actualizado correctamente');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
