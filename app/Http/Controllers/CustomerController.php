<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Hash;
use Auth;
use App\Models\Ticket;
use App\Models\TicketConversation;
use App\Models\TicketTag;
use App\Models\TicketImageReply;
use App\Models\City;
use App\Models\TypeOfPlant;
use App\Models\TicaketImage;
class CustomerController extends Controller
{
       
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers['customers'] = User::where('role', 'customer')->orderBy('id','desc')->paginate(10);
        return view('admin.user.index',$customers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $city['city']=City::where('status','active')->get();
        $city['typeofplant']=TypeOfPlant::where('status','active')->get();
        return view('user.register',$city);
    }
    public function adminuser()
    {
        $city['city']=City::where('status','active')->get();
        $city['typeofplant']=TypeOfPlant::where('status','active')->get();
        return view('admin.user.createuser',$city);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data['name']='required';
        $data['driving_licence']='required|unique:users,driving_licence';
        $data['type_of_plant_id']='required';
        $data['email']='required';
        $data['phone_no']='required';
        $data['count_of_plant']='required';  
        $data['county']='required';  
        $data['country']='required';  
        $data['state']='required';  
        $data['city_id']='required';  
        $validation = $this->validateform($request, $data);
        if ($validation) return $validation;
        $params = $request->all();
        $params['username']=$request->name;
        $params['password']=Hash::make('123456');
        $params['role']='customer';
        unset($params['_token']);
        User::create($params);
        if(Auth::user()){
            return redirect(route('list-user'))->with('message', 'success|creado con éxito');
        }
        return redirect(route('login'))->with('message', 'success|creado con éxito');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Auth::user();
        $user['user'] = User::where('id',$user->id)->first();
        $user['city']=City::where('status','active')->get();
        $user['typeofplant']=TypeOfPlant::where('status','active')->get();
        return view('user.editprofile',$user);
    }

    public function edituser($id)
    {
        
        $user['customer'] = User::where('id',$id)->first();
        $user['city']=City::where('status','active')->get();
        $user['typeofplant']=TypeOfPlant::where('status','active')->get();
        return view('admin.user.useredit',$user);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data['name']='required';
        $data['driving_licence']='required';
        $data['type_of_plant_id']='required';
        $data['city_id']='required'; 
        $data['count_of_plant']='required';  
        $data['email']='required';
        $data['phone_no']='required';
        $validation = $this->validateform($request, $data);
        if ($validation) return $validation;
        $params = $request->all();
        unset($params['_token']);
        $city = User::whereId($request->id)->first();
        $city->update($params);
            return redirect(route('list-user'))->with('message', 'success|actualizado correctamente');
    }
    public function Profileupdate(Request $request)
    {
        $data['name']='required';
        if(Auth::user()->role == "admin" ||Auth::user()->role == "technician"){
            $data['username']='required';  
        }
        if(Auth::user()->role == "customer"){
            $data['driving_licence']='required';
            $data['type_of_plant_id']='required';
            $data['city_id']='required'; 
            $data['count_of_plant']='required';  
        }
        $data['email']='required';
        $data['phone_no']='required';
        $validation = $this->validateform($request, $data);
        if ($validation) return $validation;
        $params = $request->all();
        unset($params['_token']);
        $city = User::whereId($request->id)->first();
        $city->update($params);
        return redirect()->back()->with('message', 'success|actualizado correctamente');
            
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    $tickets= Ticket::where('user_id',$id)->get();
    foreach($tickets as $ticket){
        if(isset($ticket)){

        $tags = TicketTag::where('ticket_id',$ticket->id)->get();
        foreach($tags as $tag){
            $tag->delete();
        }
        $ticketImageReply=TicketImageReply::where('ticket_id',$ticket->id)->get();
        foreach($ticketImageReply as $ticketImageReply1){
            $ticketImageReply1->delete();
        }
        $ticketconversations= TicketConversation::where('ticket_id',$ticket->id)->get();
        foreach($ticketconversations as $ticketconversation){
            $ticketconversation->delete();
        }
        $ticketimg = TicaketImage::where('ticket_id',$ticket->id)->get();
        foreach($ticketimg as $img){
            $img->delete();
        }
    }
        $ticket->delete();
    }
        User::whereId($id)->delete();
        return redirect(route('list-user'))->with('message', 'success|Eliminar con éxito');
    }
}
