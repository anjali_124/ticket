<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
// use Laravel\Sanctum\HasApiTokens;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $guarded = [];
    
    public function Ticket()
    {
        return $this->hasMany('App\Models\Ticket');
    }
    public function TicketConversation()
    {
        return $this->hasMany('App\Models\TicketConversation');
    }
    public function City()
    {
        return $this->belongsTo('App\Models\City', 'city_id');
    }
    public function TypeOfPlant()
    {
        return $this->belongsTo('App\Models\TypeOfPlant', 'type_of_plant_id');
    }
}
