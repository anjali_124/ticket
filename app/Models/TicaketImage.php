<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TicaketImage extends Model
{
    use HasFactory;
    protected $guarded = [];
    public function Ticket()
    {
        return $this->belongsTo('App\Models\Ticket', 'ticket_id');
    }
}
