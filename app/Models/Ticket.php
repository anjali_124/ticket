<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
class Ticket extends Model
{
    use HasFactory , Notifiable;
    protected $guarded = [];
    public function TicaketImage()
    {
        return $this->hasMany('App\Models\TicaketImage');
    }
    public function User()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
    public function AssignUser()
    {
        return $this->belongsTo('App\Models\User', 'assign_to');
    }
    public function TicketImageReply()
    {
        return $this->hasMany('App\Models\TicketImageReply');
    }
    public function TicketTag()
    {
        return $this->hasMany('App\Models\TicketTag');
    }
    public function TicketConversation()
    {
        return $this->hasMany('App\Models\TicketConversation');
    }
    
}
