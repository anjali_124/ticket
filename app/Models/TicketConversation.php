<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TicketConversation extends Model
{
    use HasFactory;
    protected $guarded = [];
    public function Ticket()
    {
        return $this->belongsTo('App\Models\Ticket', 'ticket_id');
    }
    public function AssignUser()
    {
        return $this->belongsTo('App\Models\User', 'assign_to');
    }
    public function User()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
    public function TicketImageReply()
    {
        return $this->hasMany('App\Models\TicketImageReply');
    }
    public function TicketTag()
    {
        return $this->hasMany('App\Models\TicketTag');
    }
    public function Category()
    {
        return $this->belongsTo('App\Models\Category', 'category');
    }
    public function SubCategory()
    {
        return $this->belongsTo('App\Models\SubCategory', 'subcategory');
    }
}
