<?php
use Illuminate\Support\Facades\Storage;

function user()
{
	$user= Auth::user();
	return $user;
}

function uploadFile($request='',$fileName='',$dir='',$del='')
{
	if( $request && $fileName ){
		if($request->hasFile($fileName)){
			$dir = 'store-files/'.$dir;
            $file = $request->file($fileName);
            	$fileName = uniqid().'-'.$file->getClientOriginalName();
            \Storage::disk('custom-disk')->put($dir.$fileName, fopen($file, 'r+'));
			if($del)
			\Storage::disk('custom-disk')->delete($dir.$del);
            return $dir.$fileName;
		}else{
			return '';
		}
	}else{
		return '';
	}
}
function generateotp($id)
{
	   return rand(1000,9999).$id;
}
function uploadMultipleFile($request='',$fileName='',$dir='')
{
	$resfiles = [];
	if( $request && $fileName ){

		if($request->hasFile($fileName)){
			$dir = 'store-files/'.$dir;
			$filenam = $request->file($fileName);
			foreach($filenam as $file){
				$fileName = uniqid().'-'.$file->getClientOriginalName();
				\Storage::disk('custom-disk')->put($dir.$fileName, fopen($file, 'r+'));
				array_push($resfiles,$dir.$fileName);
			}
            return $resfiles;
		}else{
			return $resfiles;
		}
	}else{
		return $resfiles;
	}
}

