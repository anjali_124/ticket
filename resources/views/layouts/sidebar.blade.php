<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <div class="logo" style="text-align: center;">
        
    </div>
    <ul class="nav">
        <li class="nav-item {{(request()->segment(1)=='dashboard')?'active':''}}">
        <a href="{{ route('dashborad') }}" class="simple-text">
            <img src="{{ asset('assets/img/elora 1.png') }}">
        </a>
        </li>
        <li class="nav-item {{(request()->segment(1)=='dashboard')?'active':''}}">
            <a class="nav-link" href="{{ route('dashborad') }}">
               <img src="{{ asset('images/Vector.png') }}" class="mr-2"> <span class="menu-title">Tablero de control</span>
            </a>
        </li>
        @if(Auth::user()->role =='customer')
        <li class="nav-item {{(request()->segment(1)=='ticket-create')?'active':''}}">
           
            <a class="nav-link" href="{{ route('create-ticket') }}">
                <img src="{{ asset('images/Vector.png') }}" class="mr-2"> <span class="menu-title">Crear Ticket</span>
            </a>
        </li>
        @endif
        @if(Auth::user()->role =='admin')
            <li class="nav-item {{(request()->segment(1)=='city')?'active':''}}">
                <a class="nav-link" href="{{ route('city') }}">
                    <img src="{{ asset('images/Vector.png') }}" class="mr-2">  <span class="menu-title">Administrar ciudades</span>
                </a>
            </li>
            <li class="nav-item {{(request()->segment(1)=='category')?'active':''}}">
                <a class="nav-link" href="{{ route('category') }}">
                    <img src="{{ asset('images/Vector.png') }}" class="mr-2">  <span class="menu-title">Administrar Categorías</span>
                </a>
            </li>
            <li class="nav-item {{(request()->segment(1)=='subcategory')?'active':''}}">
                <a class="nav-link" href="{{ route('subcategory') }}">
                    <img src="{{ asset('images/Vector.png') }}" class="mr-2">  <span class="menu-title">Administrar Subcategorías</span>
                </a>
            </li>
            <li class="nav-item {{(request()->segment(1)=='typeofplant')?'active':''}}">
                <a class="nav-link" href="{{ route('typeofplant') }}">
                    <img src="{{ asset('images/Vector.png') }}" class="mr-2">  <span class="menu-title">Administrar Tipo de plantación
                    </span>
                </a>
            </li>
            <li class="nav-item {{((request()->segment(1)=='user')||(request()->segment(2)=='user'))?'active':''}}">
                <a class="nav-link" href="{{ route('list-user') }}">
                    <img src="{{ asset('images/Vector.png') }}" class="mr-2">  <span class="menu-title">Administrar usuario</span>
                </a>
            </li>
            <li class="nav-item {{(request()->segment(2)=='technicians')?'active':''}}">
                <a class="nav-link" href="{{ route('list-technicians') }}">   
                    <img src="{{ asset('images/Vector.png') }}" class="mr-2">  <span class="menu-title">Administrar Técnicos</span>
                </a>
            </li>
            <li class="nav-item {{((request()->segment(1)=='alltickets'))?'active':''}}">
                <a class="nav-link" href="{{ route('alltickets') }}">
                    <img src="{{ asset('images/Vector.png') }}" class="mr-2">  <span class="menu-title">Administrar Ticket</span>
                </a>
            </li>
            <li class="nav-item {{((request()->segment(1)=='ticket')||(request()->segment(1)=='ticket-list')||(request()->segment(1)=='ticket-show')||(request()->segment(1)=='ticket-create')||(request()->segment(1)=='ticket-edit'))?'active':''}}">
                <a class="nav-link" href="{{ route('list-ticket') }}">
                    <img src="{{ asset('images/Vector.png') }}" class="mr-2">  <span class="menu-title">Asignar Técnico
                    </span>
                </a>
            </li>
            <li class="nav-item {{((request()->segment(1)=='assignticket')||(request()->segment(1)=='assign-ticket-edit')||(request()->segment(1)=='assign-ticket-show'))?'active':''}}">
                <a class="nav-link" href="{{ route('assign-ticket') }}">
                    <img src="{{ asset('images/Vector.png') }}" class="mr-2">  <span class="menu-title">Asignar Ticket</span>
                </a>
            </li>

            <li class="nav-item {{((request()->segment(1)=='fullcalender')||(request()->segment(2)=='fullcalender'))?'active':''}}">
                <a class="nav-link" href="{{ route('visitcalender') }}">
                    <img src="{{ asset('images/Vector.png') }}" class="mr-2">  <span class="menu-title">Administrar visitas técnicas</span>
                </a>
            </li>
            <li class="nav-item {{((request()->segment(1)=='chart')||(request()->segment(2)=='chart'))?'active':''}}">
                <a class="nav-link" href="{{ route('chart') }}">
                    <img src="{{ asset('images/Vector.png') }}" class="mr-2">  <span class="menu-title">Reportes</span>
                </a>
            </li>
        
        @endif
        @if(Auth::user()->role =='technician')
        <li class="nav-item {{((request()->segment(1)=='alltickets')||(request()->segment(1)=='ticket-show')||(request()->segment(1)=='ticket-edit'))?'active':''}}">
            <a class="nav-link" href="{{ route('alltickets') }}">
                <img src="{{ asset('images/Vector.png') }}" class="mr-2">  <span class="menu-title">Mis Tickets</span>
            </a>
        </li>
        <li class="nav-item {{((request()->segment(1)=='fullcalender')||(request()->segment(2)=='fullcalender'))?'active':''}}">
            <a class="nav-link" href="{{ route('visitcalender') }}">
                <img src="{{ asset('images/Vector.png') }}" class="mr-2">  <span class="menu-title">Administrar visitas técnicas</span>
            </a>
        </li>
        @endif
      
        @if(Auth::user()->role =='customer')
        <li class="nav-item {{((request()->segment(1)=='alltickets')||(request()->segment(1)=='ticket-show')||(request()->segment(1)=='ticket-edit'))?'active':''}}">
            <a class="nav-link" href="{{ route('alltickets') }}">
                <img src="{{ asset('images/Vector.png') }}" class="mr-2">  <span class="menu-title">Mis Tickets</span>
            </a>
        </li>
        @endif
       
    </ul>
</nav>
