<!DOCTYPE html>
<html lang="en">

<head>

    <title>@yield('title','Admin - Dashboard')</title>

    @yield('csslink')

    @yield('styles')

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Ticket Project</title>
    <link rel="stylesheet" href="{{ asset('vendors/feather/feather.css') }}">
    <link rel="stylesheet" href="{{ asset('vendors/ti-icons/css/themify-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('vendors/css/vendor.bundle.base.css') }}">

    <link rel="stylesheet" href="{{ asset('css/vertical-layout-light/style.css') }}">
    <link rel="stylesheet" href="{{asset('vendors/ti-icons/css/themify-icons.css')}}">
    <link rel="stylesheet" href="{{asset('vendors/mdi/css/materialdesignicons.min.css')}}">

    <link rel="stylesheet" href="{{asset('css/vertical-layout-light/style.css')}}">
    <!-- endinject -->
    <link rel="shortcut icon" href="{{asset('images/favicon.png')}}" />
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jq-3.3.1/jszip-2.5.0/dt-1.11.0/b-2.0.0/b-colvis-2.0.0/b-html5-2.0.0/b-print-2.0.0/date-1.1.1/r-2.2.9/rg-1.1.3/datatables.min.css"/>
 
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/jq-3.3.1/jszip-2.5.0/dt-1.11.0/b-2.0.0/b-colvis-2.0.0/b-html5-2.0.0/b-print-2.0.0/date-1.1.1/r-2.2.9/rg-1.1.3/datatables.min.js"></script>
    
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lato&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/solid.min.css"
        integrity="sha512-6mc0R607di/biCutMUtU9K7NtNewiGQzrvWX4bWTeqmljZdJrwYvKJtnhgR+Ryvj+NRJ8+NnnCM/biGqMe/iRA=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/fontawesome.min.css"
        integrity="sha512-giQeaPns4lQTBMRpOOHsYnGw1tGVzbAIHUyHRgn7+6FmiEgGGjaG0T2LZJmAPMzRCl+Cug0ItQ2xDZpTmEc+CQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Inter&display=swap" rel="stylesheet">
    <script src="https://cdn.ckeditor.com/4.13.0/full/ckeditor.js"></script>


    <style>
        .wrd-th-heading {
            padding: 0px !important;
            font-size: 25px;
            background-color: black;
            color: white !important;
            text-align: center;
        }

        thead {
            font-size: x-small !important;
        }

        td {
            font-size: small !important;
            padding: 5px !important;
        }

        .subbtn {
            border-radius: 8px !important;
            color: #FFFFFF !important;
            padding: 14px 18px;
            background: #0B1D4D;
            border: 1px solid #FFFFFF;
            font-size: 14px !important;
        }

        .filterrow {
            height: 50px;
            background: #F2F2F2;
            border-radius: 15px;
            display: flex;
            align-content: center;
            align-items: center;
            margin: 22px 0px;
            height: 56px;
        }

        .filterby {
            display: flex;
            align-items: center;
            justify-content: flex-end;
            font-family: 'Inter';
            font-style: normal;
            font-weight: 400;
            font-size: 15px;
            line-height: 18px;
            color: #555555;
        }

        .btn-success {
            background: #44AB2E;
            border-radius: 8px;
        }
        .pagination-wrapper{
            float: right !important;
    margin-top: 2rem !important;;
        } 
        .table-bordered {
            border: 1px solid #818181 !important;
            width: 100%;
        }

        .table-bordered td,
        .table-bordered th {
            border: 1px solid #818181 !important;

        }

        thead {
            height: 38px;
            background: #818181;
        }

        .table-bordered tr {
            height: 50px;
        }

        th {
            color: #FFFFFF !important;
            font-family: 'Inter';
            font-style: normal;
            font-weight: 700;
            font-size: 13px;
            line-height: 16px;


            color: #FFFFFF;

        }

        .table-bordered td,
        .table-bordered th {
            text-align: center;
        }


        .backbtn {
            float: right;
            text-decoration-line: underline;
            color: #1D3874;
            font-size: 14px;
        }

        .addbtn {
            float: right;
            border-radius: 8px !important;
            color: #FFFFFF !important;
            padding: 14px 18px;
            background: #0B1D4D;
            border: 1px solid #FFFFFF;

            font-size: 14px !important;
        }

        .filterbtn {
            background: #555555;
            border-radius: 8px !important;
            padding: 7px 39px !important;
            font-family: 'Inter';
            font-style: normal;
            font-weight: 700;
            font-size: 13px !important;
            line-height: 16px;
            color: #FFFFFF !important;
        }
    </style>
</head>

<body>


    <div class="container-scroller">
        <div class="container-fluid page-body-wrapper">
            @include('layouts.sidebar')
            <div class="main-panel">
                <div style="height: 70px">
                    <nav class="navbar navbar-expand-lg navbar-absolute" style="box-shadow: none;
                     }">
                        <div class="container-fluid">

                            <div class="collapse navbar-collapse justify-content-end" id="navigation">
                                <ul class="navbar-nav">
                                    {{-- @if(Auth::user()->role == 'admin') --}}
                                    <li class="nav-item ">
                                        <a class="nav-link " href="{{ route('notification') }}"
                                            style="position: relative"> <img
                                                src="{{ asset('assets/img/Notification Icon.png') }}">
                                            <p style="    position: absolute;
                                                top: 2px;
                                                background: #4fb232;
                                                border-radius: 50%;
                                                padding: 3px;
                                                font-size: .75rem;
                                                font-weight: bold;
                                                color: #f4f5fa;
                                                left: 1.2rem;
                                                width: 1.5rem;
                                                text-align: center;
                                                margin: auto;
                                                height: 1.5rem;">{{ count(user()->notifications) }}
                                            </p>
                                        </a>

                                    </li>
                                    {{-- @endif --}}
                                    <li class="nav-item dropdown">
                                        <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <img src="{{ asset('assets/img/User Icon.png') }}">

                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right"
                                            aria-labelledby="navbarDropdownMenuLink">

                                            <a class="dropdown-item"
                                                href="{{ route('editprofile', Auth::user()->id) }}">Editar Perfil</a>

                                            <a class="dropdown-item" href="{{ route('logout') }}">Cerrar sesión</a>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>
                @yield('content')
            </div>
        </div>
    </div>
    <script src="{{ asset('vendors/js/vendor.bundle.base.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    <script>
        $(document).ready(function() {

        CKEDITOR.replace('editor');

        });
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script>
        $(function(){
        $('#submit').click(function(){
            var $fileUpload = $("#images");
            if (parseInt($fileUpload.get(0).files.length)>3){
            alert("You can only upload a maximum of 3 files");
            return false;
            }
        });    
        });
    </script>
    <script>
        $('#profile').hide();  
        function readURL(input)
        {
            if (input.files && input.files[0]) 
            {
                var reader = new FileReader();
                
                reader.onload = function (e)
                {
                    $('#profile').show();
                    $('#edit_profile').hide();  

                    $('#profile').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $(document).ready(function(){
        $('#images').on('change', function(){ //on file input change
        $('#edit_preview_img').hide();  
        $('.thumb').remove();
            if (window.File && window.FileReader && window.FileList && window.Blob) //check File API supported browser
            {
        
                var data = $(this)[0].files; //this file data
                
                $.each(data, function(index, file){ //loop though each file
                    if(/(\.|\/)(gif|jpe?g|png)$/i.test(file.type)){ //check supported file type
                        var fRead = new FileReader(); //new filereader
                        fRead.onload = (function(file){ //trigger function on successful read
                        return function(e) {
                            
                            var img = $('<img width="100" height="100"/>').addClass('thumb mt-1 mr-1').attr('src', e.target.result); //create image element 
                            console.log( img)
                            $('#preview_img').append(img); //append image to output element
                        };
                        })(file);
                        fRead.readAsDataURL(file); //URL representing the file's data.
                    }
                });
                
            }else{
                alert("Your browser doesn't support File API!"); //if File API is absent
            }
        });
        });
    </script>
    @yield('scripts')

</body>

</html>