@extends('layouts.app')



@section('styles')

@endsection

@section('content')

<!-- End Navbar -->
{{-- <div class="panel-header panel-header-sm"> --}}
    <!-- <canvas id="bigDashboardChart"></canvas> -->
{{-- </div> --}}

<div class="content-wrapper">
    <div class="row m-0">
        <div class="col-md-12 p-0">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">
                        Agregar nuevo usuario
                        <a href="{{ route('list-user') }}" class="backbtn" >
                            Volver a Administrar usuarios
                        </a>
                    </h4>
                </div>
                <form action="{{ route('user-store') }}" method="post" class="mt-2" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6 pr-1">
                                <div class="form-group">
                                    <label>Nombres Completos <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" placeholder="Enter Nombre de la ciudad" name="name" value="{{ old('name') }}">
                                    @if($errors->has('name'))
                                    <span class="text-danger">{{ $errors->first('name') }}</span>
                                    @endif
                                </div>
                            </div>
                           
                            <div class="col-md-6 pr-1">
                                <div class="form-group">
                                    <label>Dirección de Correo/email<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" placeholder="Dirección de Correo/email" name="email" value="{{ old('email') }}">
                                    @if($errors->has('email'))
                                    <span class="text-danger">{{ $errors->first('email') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6 pr-1">
                                <div class="form-group">
                                    <label>Telefono no.<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" placeholder="Telefono no." name="phone_no" value="{{ old('phone_no') }}">
                                    @if($errors->has('phone_no'))
                                    <span class="text-danger">{{ $errors->first('phone_no') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6 pr-1">
                                <div class="form-group">
                                    <label>Nombre de la país <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" placeholder="Nombre de la país " name="country" value="{{ old('country') }}">
                                    @if($errors->has('country'))
                                    <span class="text-danger">{{ $errors->first('country') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6 pr-1">
                                <div class="form-group">
                                    <label>Nombre de la estado <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" placeholder="Nombre de la estado " name="state" value="{{ old('state') }}">
                                    @if($errors->has('state'))
                                    <span class="text-danger">{{ $errors->first('state') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6 pr-1">
                                <div class="form-group">
                                    <label>Nombre de la parroquia <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" placeholder="Nombre de la parroquia " name="county" value="{{ old('county') }}">
                                    @if($errors->has('county'))
                                    <span class="text-danger">{{ $errors->first('county') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6 pr-1">
                                <div class="form-group">
                                    <label >Ciudad ubicada<span class="text-danger">*</span></label>
                                    <select class="form-control" name="city_id" id="selectcity1">
                                        <option value="">Seleccione Ciudad</option>
                                        @foreach($city as $cities)
                                        <option value="{{$cities->id}}" {{ old('city_id')==$cities->id ? 'selected' : ''
                                            }} data-lat ="{{ $cities->latitude }}" data-lng="{{ $cities->longitude }}" data-address="{{ $cities->name }}">{{$cities->name}}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('city_id'))
                                    <span class="text-danger">{{ $errors->first('city_id') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6 pr-1">
                                <div class="form-group">
                                    <label >Tipo de plantación<span class="text-danger">*</span></label>
                                    <select class="form-control" name="type_of_plant_id" id="selectcity1">
                                        <option value="">Seleccione Tipo de plantación</option>
                                        @foreach($typeofplant as $cities)
                                        <option value="{{$cities->id}}" {{ old('type_of_plant_id')==$cities->id ? 'selected' : ''
                                            }} data-lat ="{{ $cities->latitude }}" data-lng="{{ $cities->longitude }}" data-address="{{ $cities->name }}">{{$cities->name}}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('type_of_plant_id'))
                                    <span class="text-danger">{{ $errors->first('type_of_plant_id') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6 pr-1">
                                <div class="form-group">
                                    <label>Número de hectareas de plantación<span class="text-danger">*</span></label>
                                    <input type="number" class="form-control" placeholder="Número de hectareas de plantación" name="count_of_plant" value="{{ old('count_of_plant') }}">
                                    @if($errors->has('count_of_plant'))
                                    <span class="text-danger">{{ $errors->first('count_of_plant') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6 pr-1">
                                <div class="form-group">
                                    <label>Nro. de Cédula<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" placeholder="Nro. de Cédula" name="driving_licence" value="{{ old('driving_licence') }}">
                                    @if($errors->has('driving_licence'))
                                    <span class="text-danger">{{ $errors->first('driving_licence') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <button class="btn btn-success" type="submit"> Agregar nuevo usuario</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
    
@endsection
