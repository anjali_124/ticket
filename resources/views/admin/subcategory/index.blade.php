@extends('layouts.app')



@section('styles')

@endsection

@section('content')

<!-- End Navbar -->


<div class="content-wrapper">
  <div class="row m-0">
    <div class="col-md-12 p-0">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">
            Administrar Subcategorías
            <a href="{{route('subcategory-create')}}" class="btn addbtn btn-sm" >
              Agregar nueva Subcategoría
            </a>
          </h4>
        </div>
        {{-- <div class="card-body"> --}}
          @if(Session()->has('message'))
          <div class="alert alert-{{explode('|',Session()->get('message'))[0]}} alert-dismissable">
            <button type="button" aria-hidden="true" class="close">
              <i class="now-ui-icons ui-1_simple-remove"></i>
            </button>
            <span>{{explode('|',Session()->get('message'))[1]}}</span>
          </div>
          @endif
       
          <div class="table-responsive">
            <table class="table-bordered" id=>
              <thead class="">
                <th >
                  No.
                </th>
                <th >
                  Nombre de la Subcategoría
                </th>
                <th >
                  Nombre de la Categoría
                </th>
                <th > 
                  Estado
                </th>
                <th >
                  Acción
                </th>
              </thead>
              <tbody>
                @foreach($subcategory as $key => $subcategori)
                <tr>
                  <td>
                    {{ $key+1 }}
                  </td>
                  <td>{{ $subcategori->name }}</td>
                  <td>{{ $subcategori->Category->name }}</td>
                  @if( $subcategori->status =='active')
                  <td style="text-transform: capitalize; "><a herf="" style="background: #36A225;
                    border-radius: 15px;    padding: 0.25rem 1rem;color:white;">Activo</a></td>
                  @else
                  <td style="text-transform: capitalize;">
                    <a herf="" style="background-color: #900E0E !important;
                    border-radius: 15px;    padding: 0.25rem 1rem;color:white;">Inactivo</a></td>
                  @endif
                  <td>
                    <a href="{{url('subcategory/edit/'.$subcategori->id)}}" class="m-3">
                      <img src="{{ asset('assets/img/Edit Icon.png') }}" >
                    </a>
                    <a href="{{url('subcategory/delete/'.$subcategori->id)}}" onclick="return confirm('Estas seguro?')">
                      <img src="{{ asset('assets/img/Delete Icon.png') }}" >
                    </a>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
            <div class="pagination-wrapper">
              {{ $subcategory->links('pagination::bootstrap-4') }}
         </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection

@section('scripts')

<script type="text/javascript">
  var oTable = $('#example').dataTable({
      stateSave: true,
  });

</script>

@endsection