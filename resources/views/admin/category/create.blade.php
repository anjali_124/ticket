@extends('layouts.app')

{{--  --}}

@section('styles')

@endsection

@section('content')

<!-- End Navbar -->
{{-- <div class="panel-header panel-header-sm">
    <!-- <canvas id="bigDashboardChart"></canvas> -->
</div> --}}

<div class="content-wrapper">
    <div class="row m-0">
        <div class="col-md-12 p-0">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">
                        Agregar nueva Categorías
                        <a href="{{ route('category') }}" class="backbtn">
                            Volver a Administrar Categorías
                        </a>
                    </h4>
                </div>
                <form action="{{ route('category-store') }}" method="post" enctype="multipart/form-data" class="mt-2">
                    {{csrf_field()}}
                    <div class="card-body">
                        <div class="row">
                            
                            <div class="col-md-6 pr-1">
                                <div class="form-group">
                                    <label for="address">Nombre de la Categorías <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control " placeholder="Nombre de la Categorías" name="name" value="{{ old('name') }}">
                                @if($errors->has('name'))
                                    <span class="text-danger">{{ $errors->first('name') }}</span>
                                    @endif
                            </div>
                            </div>
                           
                            <div class="col-md-6 pr-1">
                                <div class="form-group">
                                    <label>Estado<span class="text-danger">*</span></label>
                                    <div class='row'>
                                        <div class="col-md-4 pr-1">
                                            <span style="display: flex; align-items: center;justify-content: flex-start;">
                                                <input type="radio" class="mr-1" placeholder="Enter Address"
                                                    name="status" value="active" checked>Activo
                                            </span>
                                        </div>
                                        <div class="col-md-4 pr-1">
                                            <span style="display: flex;align-items: center;justify-content: flex-start;">
                                                <input type="radio" class="mr-1" placeholder="Enter Address"
                                                    name="status" value="inactive">Inactivo
                                            </span>
                                        </div>
                                    </div>
                                   
                                    @if($errors->has('status'))
                                    <span class="text-danger">{{ $errors->first('status') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <button class="btn btn-success" type="submit">Agregar nueva Categorías</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
    
@endsection
