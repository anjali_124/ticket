@extends('layouts.app')



@section('styles')

@endsection

@section('content')

<!-- End Navbar -->


<div class="content-wrapper">
  <div class="row m-0">
    <div class="col-md-12 p-0">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">
            Administrar Técnico
            <a href="{{route('technicians-create')}}" class="btn addbtn btn-sm" >
              Agregar nuevos técnico
            </a>
          </h4>
        </div>
        <div class="card-body">
          @if(Session()->has('message'))
          <div class="alert alert-{{explode('|',Session()->get('message'))[0]}} alert-dismissable">
            <button type="button" aria-hidden="true" class="close">
              <i class="now-ui-icons ui-1_simple-remove"></i>
            </button>
            <span>{{explode('|',Session()->get('message'))[1]}}</span>
          </div>
          @endif
          <div class="table-responsive">
            <table class="table-bordered" id=>
              <thead class="text-success">
                <th>
                  S.No.
                </th>
                <th>
                Nombre de usuario
                </th>
                <th>
                  Telefono
                </th>
                
                <th>
                  Acción
                </th>
              </thead>
              <tbody>
                @foreach($customers as $key => $customer)
                 <tr>
                   <td>
                    {{$key+1}}
                   </td>
                   <td>{{ $customer->name }}</td>

                   <td>{{ $customer->phone_no}}</td>
                  
                   <td>
                    <a href="{{ route('edit-technicians',$customer->id) }}" >
                        <img src="{{ asset('assets/img/image 49.png') }}" >
                       </a>
                     <a href="{{ route('delete-technicians',$customer->id) }}" >
                      <img src="{{ asset('assets/img/image 66.png') }}" >
                     </a>
                    
                   </td>
                 </tr>
                @endforeach
              </tbody>
            </table>
            <div class="pagination-wrapper">
              {{ $customers->links('pagination::bootstrap-4') }}
         </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection

@section('scripts')

<script type="text/javascript">

  var oTable = $('#example').dataTable({
      stateSave: true,
  });

</script>

@endsection