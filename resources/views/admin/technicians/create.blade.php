@extends('layouts.app')



@section('styles')

@endsection

@section('content')

<!-- End Navbar -->
{{-- <div class="panel-header panel-header-sm"> --}}
    <!-- <canvas id="bigDashboardChart"></canvas> -->
{{-- </div> --}}

<div class="content-wrapper">
    <div class="row m-0">
        <div class="col-md-12 p-0">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">
                        Agregar nuevo técnico
                        <a href="{{ route('list-technicians') }}" class="backbtn" >
                            Volver a Administrar técnico
                        </a>
                    </h4>
                </div>
                <form action="{{ route('technicians-store') }}" method="post" class="mt-2" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6 pr-1">
                                <div class="form-group">
                                    <label>Nombres Completos <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" placeholder="Nombres Completos " name="name" value="{{ old('name') }}">
                                    @if($errors->has('name'))
                                    <span class="text-danger">{{ $errors->first('name') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6 pr-1">
                                <div class="form-group">
                                    <label>Nombre de usuario<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" placeholder="nombre de usuario" name="username" value="{{ old('username') }}">
                                    @if($errors->has('username'))
                                    <span class="text-danger">{{ $errors->first('username') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6 pr-1">
                                <div class="form-group">
                                    <label>Dirección de Correo/email<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" placeholder="Dirección de Correo/email" name="email" value="{{ old('email') }}">
                                    @if($errors->has('email'))
                                    <span class="text-danger">{{ $errors->first('email') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6 pr-1">
                                <div class="form-group">
                                    <label>Teléfono<span class="text-danger">*</span></label>
                                    <input type="number" class="form-control" placeholder="Teléfono" name="phone_no" value="{{ old('phone_no') }}">
                                    @if($errors->has('phone_no'))
                                    <span class="text-danger">{{ $errors->first('phone_no') }}</span>
                                    @endif
                                </div>
                            </div>
                           
                            <div class="col-md-6 pr-1">
                                <div class="form-group">
                                    <label>Contraseña <span class="text-danger">*</span></label>
                                    <input type="password" class="form-control" placeholder="contraseña" name="password" value="{{ old('password') }}">
                                    @if($errors->has('password'))
                                    <span class="text-danger">{{ $errors->first('password') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6 pr-1">
                                <div class="form-group">
                                    <label>Confirmar Contraseña<span class="text-danger">*</span></label>
                                    <input type="password" class="form-control" placeholder="confirmar Contraseña" name="confirm_password" value="{{ old('confirm_password') }}">
                                    @if($errors->has('confirm_password'))
                                    <span class="text-danger">{{ $errors->first('confirm_password') }}</span>
                                    @endif
                                </div>
                            </div>

                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <button class="btn btn-success" type="submit">Agregar nuevo técnico</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
    
@endsection
