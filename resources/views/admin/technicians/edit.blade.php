@extends('layouts.app')



@section('styles')

@endsection

@section('content')

<!-- End Navbar -->
{{-- <div class="panel-header panel-header-sm"> --}}
    <!-- <canvas id="bigDashboardChart"></canvas> -->
{{-- </div> --}}

<div class="content-wrapper">
    <div class="row m-0">
        <div class="col-md-12 p-0">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">
                        Editar Técnico
                        <a href="{{ route('list-technicians') }}" class="backbtn" >
                            Volver a Administrar Técnico
                        </a>
                    </h4>
                </div>
                <form action="{{ route('technicians-update') }}" method="post" class="mt-2" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <input type="hidden" name="id" value="{{$customer->id}}">

                    <div class="card-body">

                        <div class="row">
                           
                            <div class="col-md-6 pr-1">
                                <div class="form-group">
                                    <label>Nombres Completos <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" placeholder="Enter Nombre de la ciudad" name="name" value="{{$customer->name }}">
                                    @if($errors->has('name'))
                                    <span class="text-danger">{{ $errors->first('name') }}</span>
                                    @endif
                                </div>
                            </div>
                            
                            <div class="col-md-6 pr-1">
                                <div class="form-group">
                                    <label>Dirección de Correo/email<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" placeholder="Enter Nombre de la ciudad" name="email" value="{{$customer->email }}" >
                                    @if($errors->has('email'))
                                    <span class="text-danger">{{ $errors->first('email') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6 pr-1">
                                <div class="form-group">
                                    <label>Teléfono<span class="text-danger">*</span></label>
                                    <input type="number" class="form-control" placeholder="Teléfono" name="phone_no" value="{{$customer->phone_no }}">
                                    @if($errors->has('phone_no'))
                                    <span class="text-danger">{{ $errors->first('phone_no') }}</span>
                                    @endif
                                </div>
                            </div>
                           
                            <div class="col-md-6 pr-1">
                                <div class="form-group">
                                    <label>Nombre de usuario <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" placeholder="nombre de usuario" name="username" value="{{$customer->username }}">
                                    @if($errors->has('username'))
                                    <span class="text-danger">{{ $errors->first('username') }}</span>
                                    @endif
                                </div>
                            </div>
                         
                           
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <button class="btn btn-success" type="submit">actualizar Técnico</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
    
@endsection
