@extends('layouts.app')



@section('styles')

@endsection

@section('content')

<div class="content-wrapper">

  <form action="{{ route('filter-chart') }}" method="GET" style="width:99%">
    <div class="filterrow-box">
      <div class="row filterrow" style="height: 135px;">
        <div class="p-3 filterby">
          <label class="mb-0">Filtrado por</label>
        </div>
        
        {{-- <input type="hidden" name="is_filter" value="yes"> --}}
          <div class="col-3">
          <input type="text" onfocus="(this.type='date')" placeholder="Partir de la fecha" name="from_date" class="form-control "
            value="{{ request()->from_date }}">
        </div>
       
        <div class="col-3 pl-0">
          <input type="text" onfocus="(this.type='date')" name="to_date" placeholder="Hasta la fecha" class="form-control "
            value="{{ request()->to_date }}">
        </div>
        <div class="col-3 pl-0">
          <select class="form-control" name="city">
            <option value="">Seleccione Ciudad</option>
            @foreach($cities as $key => $city)
            <option value="{{ $city->name }}" {{ (request()->city==$city->name)?'selected':'' }}>{{ $city->name  }}</option>
            @endforeach
          </select>
        </div>
        <div class="col-3 pl-5">
          <select class="form-control" name="category_id">
            <option value="">Seleccione Categoría</option>
            @foreach($category as $key => $city)
            <option value="{{ $city->id }}" {{ (request()->category_id==$city->id)?'selected':'' }}>{{ $city->name  }}</option>
            @endforeach
          </select>
        </div>
        <div class="col-3 pl-0">
          <select class="form-control" name="subcategory_id">
            <option value="">Seleccione SubCategoría</option>
            @foreach($subcategory as $key => $city)
            <option value="{{$city->id }}" {{ (request()->subcategory_id==$city->id)?'selected':'' }}>{{ $city->name  }}</option>
            @endforeach
          </select>
        </div>
        <div class="col-1 p-0 mr-2">
          <button type="submit" class="btn filterbtn btn-sm m-0">Filtrar</button>
        </div>
      </div>
    </div>
  </form>

    <div class="row">
        
        <div class="col-lg-12 grid-margin grid-margin-lg-0 stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Gráfico de estado de Ticket basado en</h4>
                    <canvas id="pieChart"></canvas>
                </div>
            </div>
        </div>
        @foreach($users as $key => $user)

        <div class="col-lg-6 mt-3 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">{{ $user->name }} (Abierto) Ticket</h4>
                    <canvas id="opendata{{ $key  }}"></canvas>
                </div>
            </div>
        </div>
        <div class="col-lg-6 mt-3 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">{{ $user->name }} (Esperando) Ticket</h4>
                    <canvas id="waitingdata{{ $key  }}"></canvas>
                </div>
            </div>
        </div>
        <div class="col-lg-6 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">{{ $user->name }} (Visita) Ticket</h4>
                    <canvas id="visitdata{{ $key  }}"></canvas>
                </div>
            </div>
        </div>
        <div class="col-lg-6 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">{{ $user->name }} (Revisita) Ticket</h4>
                    <canvas id="revisitdata{{ $key  }}"></canvas>
                </div>
            </div>
        </div>
        <div class="col-lg-6 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">{{ $user->name }} (Cerrada) Ticket</h4>
                    <canvas id="closedata{{ $key  }}"></canvas>
                </div>
            </div>
        </div>
        <div class="col-lg-6 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">{{ $user->name }} (Reabrir) Ticket</h4>
                    <canvas id="reopendata{{ $key  }}"></canvas>
                </div>
            </div>
        </div>
        <hr>
        @endforeach
      
    </div>




</div>

@endsection

@section('scripts')
<script src="{{ asset('vendors/js/vendor.bundle.base.js') }}"></script>
<script src="{{ asset('vendors/chart.js/Chart.min.js') }}"></script>
<script src="{{ asset('js/off-canvas.js') }}"></script>
<script src="{{ asset('js/hoverable-collapse.js') }}"></script>
<script src="{{ asset('js/template.js') }}"></script>
<script src="{{ asset('js/settings.js') }}"></script>
<script src="{{ asset('js/todolist.js') }}"></script>
<script>
    $(function() {
    var booking = @json($data2);
    console.log(booking);
    booking.forEach(function(item,i) {
     console.log(item);
     console.log(i);
    var opendata = {
      labels: ["mon", "tue", "wed", "thur", "fri", "sat","sun"],
      datasets: [{
        label: 'Tickets',
        data:[
                    item.openmonday,item.opentuesday,item.openthurday , item.openfriday ,item.opensatday
                  , item.opensunday ,
                ],
        backgroundColor: [
          'rgba(255, 99, 132, 0.2)',
          'rgba(54, 162, 235, 0.2)',
          'rgba(255, 206, 86, 0.2)',
          'rgba(75, 192, 192, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(255, 159, 64, 0.2)',
          'rgba(153, 153, 153, 0.36)'
        ],
        borderColor: [
          'rgba(255,99,132,1)',
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)',
          'rgba(75, 192, 192, 1)',
          'rgba(153, 102, 255, 1)',
          'rgba(255, 159, 64, 1)',
          '#000000'
        ],
        borderWidth: 1,
        fill: false
      }]
    };
    var waitingdata = {
      labels: ["mon", "tue", "wed", "thur", "fri", "sat","sun"],
      datasets: [{
        label: 'Tickets',
        data:[
                    item.waitingmonday,
                    item.waitingtuesday, 
                    item.waitingwedday,
                    item.waitingthurday ,
                    item.waitingfriday ,
                    item.waitingsatday ,
                    item.waitingsunday ,
                ],
        backgroundColor: [
          'rgba(255, 99, 132, 0.2)',
          'rgba(54, 162, 235, 0.2)',
          'rgba(255, 206, 86, 0.2)',
          'rgba(75, 192, 192, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(255, 159, 64, 0.2)',
          'rgba(153, 153, 153, 0.36)'
        ],
        borderColor: [
          'rgba(255,99,132,1)',
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)',
          'rgba(75, 192, 192, 1)',
          'rgba(153, 102, 255, 1)',
          'rgba(255, 159, 64, 1)',
          '#000000'
        ],
        borderWidth: 1,
        fill: false
      }]
    };
 
    var visitdata = {
      labels: ["mon", "tue", "wed", "thur", "fri", "sat","sun"],
      datasets: [{
        label: 'Tickets',
        data:[
                    item.visitmonday,
                    item.visittuesday, 
                    item.visitwedday,
                    item.visitthurday ,
                    item.visitfriday ,
                    item.visitsatday ,
                    item.visitsunday ,
                ],
        backgroundColor: [
          'rgba(255, 99, 132, 0.2)',
          'rgba(54, 162, 235, 0.2)',
          'rgba(255, 206, 86, 0.2)',
          'rgba(75, 192, 192, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(255, 159, 64, 0.2)',
          'rgba(153, 153, 153, 0.36)'
        ],
        borderColor: [
          'rgba(255,99,132,1)',
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)',
          'rgba(75, 192, 192, 1)',
          'rgba(153, 102, 255, 1)',
          'rgba(255, 159, 64, 1)',
          '#000000'
        ],
        borderWidth: 1,
        fill: false
      }]
    };

    var revisitdata = {
      labels: ["mon", "tue", "wed", "thur", "fri", "sat","sun"],
      datasets: [{
        label: 'Tickets',
        data:[
                    item.revisitmonday,
                    item.revisittuesday, 
                    item.revisitwedday,
                    item.revisitthurday ,
                    item.revisitfriday ,
                    item.revisitsatday ,
                    item.revisitsunday ,
                ],
        backgroundColor: [
          'rgba(255, 99, 132, 0.2)',
          'rgba(54, 162, 235, 0.2)',
          'rgba(255, 206, 86, 0.2)',
          'rgba(75, 192, 192, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(255, 159, 64, 0.2)',
          'rgba(153, 153, 153, 0.36)'
        ],
        borderColor: [
          'rgba(255,99,132,1)',
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)',
          'rgba(75, 192, 192, 1)',
          'rgba(153, 102, 255, 1)',
          'rgba(255, 159, 64, 1)',
          '#000000'
        ],
        borderWidth: 1,
        fill: false
      }]
    };


    var closedata = {
      labels: ["mon", "tue", "wed", "thur", "fri", "sat","sun"],
      datasets: [{
        label: 'Tickets',
        data:[
                    item.closemonday,
                    item.closetuesday, 
                    item.closewedday,
                    item.closethurday ,
                    item.closefriday ,
                    item.closesatday ,
                    item.closesunday ,
                ],
        backgroundColor: [
          'rgba(255, 99, 132, 0.2)',
          'rgba(54, 162, 235, 0.2)',
          'rgba(255, 206, 86, 0.2)',
          'rgba(75, 192, 192, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(255, 159, 64, 0.2)',
          'rgba(153, 153, 153, 0.36)'
        ],
        borderColor: [
          'rgba(255,99,132,1)',
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)',
          'rgba(75, 192, 192, 1)',
          'rgba(153, 102, 255, 1)',
          'rgba(255, 159, 64, 1)',
          '#000000'
        ],
        borderWidth: 1,
        fill: false
      }]
    };


    var reopendata = {
      labels: ["mon", "tue", "wed", "thur", "fri", "sat","sun"],
      datasets: [{
        label: 'Tickets',
        data:[
                    item.reopenmonday,
                    item.reopentuesday, 
                    item.reopenwedday,
                    item.reopenthurday ,
                    item.reopenfriday ,
                    item.reopensatday ,
                    item.reopensunday ,
                ],
        backgroundColor: [
          'rgba(255, 99, 132, 0.2)',
          'rgba(54, 162, 235, 0.2)',
          'rgba(255, 206, 86, 0.2)',
          'rgba(75, 192, 192, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(255, 159, 64, 0.2)',
          'rgba(153, 153, 153, 0.36)'
        ],
        borderColor: [
          'rgba(255,99,132,1)',
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)',
          'rgba(75, 192, 192, 1)',
          'rgba(153, 102, 255, 1)',
          'rgba(255, 159, 64, 1)',
          '#000000'
        ],
        borderWidth: 1,
        fill: false
      }]
    };

    var options = {
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true
            
          }
        }]
      },
      legend: {
        display: false
      },
      elements: {
        point: {
          radius: 0
        }
      }
  
    };
    var doughnutPieData = {
      datasets: [{
        data:[
                    item.open,
                    item.waiting, 
                    item.visit,
                    item.revisit ,
                    item.close ,
                    item.reopen ,
                ],
        backgroundColor: [
          'rgba(255, 99, 132, 0.5)',
          'rgba(54, 162, 235, 0.5)',
          'rgba(255, 206, 86, 0.5)',
          'rgba(75, 192, 192, 0.5)',
          'rgba(153, 102, 255, 0.5)',
          'rgba(255, 159, 64, 0.5)'
        ],
        borderColor: [
          'rgba(255,99,132,1)',
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)',
          'rgba(75, 192, 192, 1)',
          'rgba(153, 102, 255, 1)',
          'rgba(255, 159, 64, 1)'
        ],
      }],
  
      labels: ["Abierto", "Esperando", "Visita", "Revisita", "Cerrada", "Reabrir"],
    };
    var doughnutPieOptions = {
      responsive: true,
      animation: {
        animateScale: true,
        animateRotate: true
      }
    };
    if ($("#opendata"+i).length) {
      var barChartCanvas = $("#opendata"+i).get(0).getContext("2d");
      var barChart = new Chart(barChartCanvas, {
        type: 'bar',
        data: opendata,
        options: options
      });
    }
  
    if ($("#waitingdata"+i).length) {
      var barChartCanvas = $("#waitingdata"+i).get(0).getContext("2d");
      var barChart = new Chart(barChartCanvas, {
        type: 'bar',
        data: waitingdata,
        options: options
      });
    }
    if ($("#visitdata"+i).length) {
      var barChartCanvas = $("#visitdata"+i).get(0).getContext("2d");
      var barChart = new Chart(barChartCanvas, {
        type: 'bar',
        data: visitdata,
        options: options
      });
    }
    if ($("#revisitdata"+i).length) {
      var barChartCanvas = $("#revisitdata"+i).get(0).getContext("2d");
      var barChart = new Chart(barChartCanvas, {
        type: 'bar',
        data: revisitdata,
        options: options
      });
    }
    if ($("#closedata"+i).length) {
      var barChartCanvas = $("#closedata"+i).get(0).getContext("2d");
      var barChart = new Chart(barChartCanvas, {
        type: 'bar',
        data: closedata,
        options: options
      });
    }
    if ($("#reopendata"+i).length) {
      var barChartCanvas = $("#reopendata"+i).get(0).getContext("2d");
      var barChart = new Chart(barChartCanvas, {
        type: 'bar',
        data: reopendata,
        options: options
      });
    }

 
    if ($("#doughnutChart").length) {
      var doughnutChartCanvas = $("#doughnutChart").get(0).getContext("2d");
      var doughnutChart = new Chart(doughnutChartCanvas, {
        type: 'doughnut',
        data: doughnutPieData,
        options: doughnutPieOptions
      });
    }
  
    if ($("#pieChart").length) {
      var pieChartCanvas = $("#pieChart").get(0).getContext("2d");
      var pieChart = new Chart(pieChartCanvas, {
        type: 'pie',
        data: doughnutPieData,
        options: doughnutPieOptions
      });
    }
  });
  });
</script>

@endsection