<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>@yield('title','Ticket Project - Login')</title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
  <!-- CSS Files -->
  <link href="{{asset('assets/css/now-ui-dashboard.css?v=1.3.0')}}" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link rel="preconnect" href="https://fonts.googleapis.com"><link rel="preconnect" href="https://fonts.gstatic.com" crossorigin><link href="https://fonts.googleapis.com/css2?family=Inter&display=swap" rel="stylesheet">
  <link href="{{asset('assets/demo/demo.css')}}" rel="stylesheet" />
<style>
  .addbtn{
    background: #44AB2E;
    border-radius: 8px !important;
    color: #FFFFFF !important;
    padding: 13px 36px;
    font-size: 14px !important;
}
.font{
  font-family: 'Inter';
font-style: normal;
font-weight: 400;
font-size: 14px;
line-height: 17px;
color: #3E3E3E;

}
.form{
  background: #FFFFFF !important;
border: 1px solid #919191 !important;
border-radius: 8px !important;
}
</style>
</head>

<body class="">

  <div class="wrapper" style="background-color: #F4F5FA;display: flex;align-content: center;
  justify-content: center;
  align-items: center;">
        <div style="width: 1000px;
        height: 500px;
        background: #FFFFFF;
        box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
        border-radius: 15px;">
        <div class="row m-0">
        <div class="col-md-6 col-sm-6 p-0" style="background: #132B5E;
        border-radius: 15px 0px 0px 15px;
        display: flex;
        align-content: center;
        justify-content: center;
        align-items: center;
        height: 500px;">
         <img src="{{ asset('assets/img/gradient fingerprint unlock login computer_5044947 1.png') }}" width="300" height="300">        </div>
        <div class="col-md-6 col-sm-6 p-0" >
          <img src="{{ asset('assets/img/elora 1 (1).png') }}"  width="250" height="160" style="margin: auto;
          display: block;"
      >
      @if(Session()->has('message'))
      <div class=" col-md-6 alert alert-{{explode('|',Session()->get('message'))[0]}} alert-dismissable m-auto">
        <button type="button" aria-hidden="true" class="close">
          <i class="now-ui-icons ui-1_simple-remove"></i>
        </button>
        <span>{{explode('|',Session()->get('message'))[1]}}</span>
      </div>
      @endif
           <form method="POST" action="{{ route('adminlogin-post') }}" style="margin: auto;display: block;">
            {{csrf_field()}}
            @if(Session()->has('msg'))
            <div class="alert alert-{{Session()->get('alert')}}">
              {{Session()->get('msg')}}
            </div>
            @endif
            {{-- <div >  --}}
              <div class="form-group">
                <label class="font" style="margin-left: 3rem;">Dirección de Correo/email</label>
                <input type="text" name="email" class="form-control form col-md-10" style="margin: auto;display: block;" required>
                @if ($errors->has('email'))
                   <span class="text-danger">
                       <strong>{{ $errors->first('email') }}</strong>
                   </span>
               @endif
            </div>
            <div class="form-group">
              <label class="font" style="margin-left: 3rem;">Contraseña</label>
              <input type="password" name="password" class="form-control form col-md-10" style="margin: auto;display: block;" required>
              @if ($errors->has('password'))
                   <span class="text-danger">
                       <strong>{{ $errors->first('password') }}</strong>
                   </span>
               @endif
          </div>
              <br>
              <button class="btn addbtn" style="margin: auto;display: block;">Iniciar sesión </button>
           </form>
        
          </div>
        </div>
        </div>
  </div>
  </div>
  <!--   Core JS Files   -->
  <!--  Notifications Plugin    -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>

  <script>
    
    $(document).ready(function(){

      window.setTimeout(function() {
          $(".alert").fadeTo(1000, 0).slideUp(1000, function(){
              $(this).remove(); 
          });
      }, 3000);

    });
  </script>

</body>

</html>