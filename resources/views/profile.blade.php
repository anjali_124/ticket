@extends('layouts.app')

@section('title','Dashboard')

@section('styles')

@endsection

@section('content')

<!-- End Navbar -->
{{-- <div class="panel-header panel-header-sm">
    <!-- <canvas id="bigDashboardChart"></canvas> -->
</div> --}}

<div class="content-wrapper">
    <div class="row m-0">
        <div class="col-md-12 p-0">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">
                        Editar perfil
                    </h4>
                </div>
                @if(Session()->has('message'))
                <div class="alert alert-{{explode('|',Session()->get('message'))[0]}} alert-dismissable">
                  <button type="button" aria-hidden="true" class="close">
                    <i class="now-ui-icons ui-1_simple-remove"></i>
                  </button>
                  <span>{{explode('|',Session()->get('message'))[1]}}</span>
                </div>
                @endif
                <form action="{{ route('profile-update') }}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <input type="hidden" name="id" value="{{ $user->id }}">

                    <div class="card-body">
                        <div class="row m-0">
                            <div class="col-md-6 pl-0">
                                <div class="form-group">
                                    <label>Nombres<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" placeholder="Enter Hotel Name" name="fname"
                                        value="{{ $user->fname }}">
                                    @if($errors->has('fname'))
                                    <span class="text-danger">{{ $errors->first('fname') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6 pr-1">
                                <div class="form-group">
                                    <label>Apellidos<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" placeholder="Enter Hotel Name" name="lname"
                                        value="{{ $user->lname }}">
                                    @if($errors->has('fname'))
                                    <span class="text-danger">{{ $errors->first('fname') }}</span>
                                    @endif 
                                </div>
                            </div>
                            <div class="col-md-6 pl-0">
                                <div class="form-group">
                                    <label>Usuario<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" placeholder="Enter Hotel Name" disabled name="fname"
                                        value="{{ $user->username }}">
                                    @if($errors->has('fname'))
                                    <span class="text-danger">{{ $errors->first('fname') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6 pr-1">
                                <div class="form-group">
                                    <label>Dirección de correo electrónico <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" placeholder="email"
                                        name="email" value="{{ $user->email }}">
                                    @if($errors->has('email'))
                                    <span class="text-danger">{{ $errors->first('email') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6 pl-0">
                                <div class="form-group">
                                    <label>Foto de perfil<span class="text-danger">*</span></label>
                                    <input type="file" class="form-control" placeholder="Enter Phone / Cell Number"
                                        name="image" >
                                    @if($errors->has('phone_no'))
                                    <span class="text-danger">{{ $errors->first('phone_no') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6 pr-1">
                                <div class="form-group">
                                    <label>Número de teléfono<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" placeholder="Enter Phone / Cell Number"
                                        name="phone_no" value="{{ $user->phone_no }}">
                                    @if($errors->has('phone_no'))
                                    <span class="text-danger">{{ $errors->first('phone_no') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="row m-0">
                            <div class="col-md-12 text-center">
                                <button class="btn btn-success" type="submit">Actualizar perfil</button>
                            </div>
                        </div>
                    </div>
                </form>
                <hr class="ml-3 mr-3 mb-5 mt-4">
                <div class="card-header">
                    <h4 class="card-title">
                        Cambia la contraseña
                    </h4>
                </div>
                <form action="{{ route('password-update') }}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <input type="hidden" name="id" value="{{ $user->id }}">

                    <div class="card-body">
                        <div class="row m-0">
                            <div class="col-md-6 pl-0">
                                <div class="form-group">
                                    <label>Contraseña<span class="text-danger">*</span></label>
                                    <input type="password" name="new_password" class="form-control map-input">
                                    @if($errors->has('new_password'))
                                    <span class="text-danger">{{ $errors->first('new_password') }}</span>
                                    @endif
                                </div>
                                <div id="address-map"></div>
                            </div>

                            <div class="col-md-6 pr-1">
                                <div class="form-group">
                                    <label>Confirmar contraseña<span class="text-danger">*</span></label>
                                    <input type="password" class="form-control" name="confirm_password">
                                    @if($errors->has('confirm_password'))
                                    <span class="text-danger">{{ $errors->first('confirm_password') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <button class="btn btn-success" type="submit">Actualizar contraseña</button>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')

@endsection