@extends('layouts.app')

{{-- @section('title','Dashboard') --}}

@section('styles')
<style>
	.resumen_main {
		display: flex;
		width: 100%;
	}

	.space_col {
		display: inline-block;
		max-width: 300px;
		width: 100%;
	}

	.resumen_col {
		display: inline-block;
		width: 100%;
	}

	.resumen_wrp {
		display: inline-block;
		width: 100%;
	}

	.resumen_wrp ul {
		display: flex;
		width: 100%;
		padding: 0;
		margin: 0;
	}

	.resumen_wrp ul li {
		/*		margin-right: 36px;*/
		list-style-type: none;
		/*		width: 25%;*/
	}

	.resumen_wrp ul li .main-bg {
		border-top-left-radius: 8px;
		border-top-right-radius: 8px;
	}

	.resumen_wrp ul li:last-child {
		margin-right: 0;
	}

	.resumen_wrp ul .resumen_top_detail {
		display: flex;
		width: auto;
		padding: 20px;
		align-items: center;
	}

	.resumen_wrp ul li .resumen_detail_left {
		display: flex;
		width: 100%;
		flex-wrap: wrap;
		justify-content: center;
		text-align: center;
	}

	.resumen_wrp ul li .resumen_detail_left h4 {
		font-size: 31px;
		display: flex;
		width: 100%;
		margin: 0;
		color: #fff;
		justify-content: center;
	}

	.resumen_wrp ul li .resumen_detail_left span {
		font-size: 16px;
		color: #fff;
	}

	.resumen_wrp ul li.Esperando .main-bg {
		background: #C3BC00;
	}

	.resumen_btn_text {
		display: inline-block;
		width: 100%;
		text-align: center;
		color: #fff;
		background: #0000002e;
	}

	.resumen_btn_text a {
		display: flex;
		align-items: center;
		justify-content: center;
		text-decoration: none;
		color: #fff;
	}

	.resumen_btn_text img {
		padding-top: 8px;
	}

	.Cerca .main-bg {
		background: #7E0000;
	}

	.Visitar .main-bg {
		background: #00828A;
	}

	.Abierto .main-bg {
		background: #306D00;
	}
</style>

@endsection

@section('content')


<div class="resumen_main">
	<div class="resumen_col">
		<div class="resumen_wrp">
			<ul class="row">
				@if(Auth::user()->role == 'admin'||Auth::user()->role == 'customer')
				<li class="col-lg-2 col-md-6 col-sm-6 Abierto mb-4 p-0 mr-3 ml-3">
					<div class="main-bg"  style="height: 180px;">
						<div class="resumen_top_detail" style="height: 154px;">
							<div class="resumen_detail_left">
								<h4>{{ $open }}</h4>
								<span>Abierto <br> Ticket </span>
							</div>
							<div class="resumen_detail_right">
								<img src="{{ asset('images/image 33.png') }}" alt="image 33">
							</div>
						</div>
						<div class="resumen_btn_text" style="height: 26px;">
							@if($open != 0)
								<a href="{{ url('alltickets/open') }}">Más info <img
									src="{{ asset('images/image 25.png')}}"> </a>
							@endif

						</div>
					</div>
				</li>
				<li class="col-lg-2 col-md-6 col-sm-6 Esperando mb-4 p-0 mr-3 ml-3">
					<div class="main-bg"  style="height: 180px;">
						<div class="resumen_top_detail" style="height: 154px;">
							<div class="resumen_detail_left">
								<h4>{{ $waiting }}</h4>
								<span>Esperando <br> Técnico </span>
							</div>
							<div class="resumen_detail_right">
								<img src="{{ asset('images/image 34.png')}}" alt="image 33">
							</div>
						</div>
						@if($waiting != 0)
						<div class="resumen_btn_text" style="height: 26px;">
							<a href="{{ url('alltickets/waiting') }}">Más info <img
									src="{{ asset('images/image 25.png')}}"> </a>
						</div>
						@endif
					</div>
				</li>
				<li class="col-lg-2 col-md-6 col-sm-6 Cerca mb-4 p-0 mr-3 ml-3">
					<div class="main-bg"  style="height: 180px;">
						<div class="resumen_top_detail" style="height: 154px;">
							<div class="resumen_detail_left">
								<h4>{{ $visit }}</h4>
								<span>Visitar <br> Requerido</span>
							</div>
							<div class="resumen_detail_right" >
								<img src="{{ asset('images/image 35.png') }}" alt="image 33">
							</div>
						</div>
						<div class="resumen_btn_text" style="height: 26px;">


							@if($visit != 0)<a href="{{ url('alltickets/visit') }}">Más info <img
									src="{{ asset('images/image 25.png') }}"></a>@endif 


						</div>
					</div>
				</li>
				<li class="col-lg-2 col-md-6 col-sm-6 Visitar mb-4 p-0 mr-3 ml-3">
					<div class="main-bg"  style="height: 180px;">
						<div class="resumen_top_detail" style="height: 154px;">
							<div class="resumen_detail_left">
								<h4>{{ $revisit }}</h4>
								<span>Revisitar <br> Requerido</span>
							</div>
							<div class="resumen_detail_right">
								<img src="{{ asset('images/image 36.png') }}" alt="image 33">
							</div>
						</div>
						<div class="resumen_btn_text" style="height: 26px;"> 
							@if($revisit != 0)

							<a href="{{ url('alltickets/revisit') }}">Más info <img
									src="{{ asset('images/image 25.png') }}"> </a>
							@endif

						</div>
					</div>
				</li>
				<li class="col-lg-2 col-md-6 col-sm-6 Abierto mb-4 p-0 mr-3 ml-2">
					<div class="main-bg"  style="height: 180px;">
						<div class="resumen_top_detail" style="height: 154px;">
							<div class="resumen_detail_left">
								<h4>{{ $close }}</h4>
								<span>Cerrado <br> Ticket </span>
							</div>
							<div class="resumen_detail_right">
								<img src="{{ asset('images/image 33.png') }}" alt="image 33">
							</div>
						</div>
						<div class="resumen_btn_text" style="height: 26px;">
							@if($close != 0)

							<a href="{{ url('alltickets/closed') }}">Más info <img
									src="{{ asset('images/image 25.png')}}"> </a>

							@endif
						</div>
					</div>
				</li>
				@endif
				@if(Auth::user()->role == 'technician')

				<li class="col-lg-3 col-md-6 col-sm-6 Esperando mb-4">
					<div class="main-bg"  style="height: 180px;">
						<div class="resumen_top_detail" style="height: 154px;">
							<div class="resumen_detail_left" >
								<h4>{{ $waiting }}</h4>
								<span>Esperando <br> Técnico </span>
							</div>
							<div class="resumen_detail_right">
								<img src="{{ asset('images/image 34.png')}}" alt="image 33">
							</div>
						</div>
						<div class="resumen_btn_text"  style="height: 26px;">
							@if($waiting != 0)
							<a href="{{ url('alltickets/waiting') }}">Más info <img
									src="{{ asset('images/image 25.png')}}"> </a>
									@endif
						</div>
					</div>
				</li>
				<li class="col-lg-3 col-md-6 col-sm-6 Cerca mb-4">
					<div class="main-bg"  style="height: 180px;">
						<div class="resumen_top_detail" style="height: 154px;">
							<div class="resumen_detail_left"  >
								<h4>{{ $visit }}</h4>
								<span>Visitar <br> Requerido</span>
							</div>
							<div class="resumen_detail_right"  style="height: 26px;">
								<img src="{{ asset('images/image 35.png') }}" alt="image 33">
							</div>
						</div>
						<div class="resumen_btn_text">
							@if($visit != 0)
							<a href="{{ url('alltickets/visit') }}">Más info <img
									src="{{ asset('images/image 25.png') }}"> </a>
									@endif
						</div>
					</div>
				</li>
				<li class="col-lg-3 col-md-6 col-sm-6 Visitar mb-4">
					<div class="main-bg"  style="height: 180px;">
						<div class="resumen_top_detail"style="height:154px">
							<div class="resumen_detail_left">
								<h4>{{ $revisit }}</h4>
								<span>Revisitar <br> Requerido</span>
							</div>
							<div class="resumen_detail_right">
								<img src="{{ asset('images/image 36.png') }}" alt="image 33">
							</div>
						</div>
						<div class="resumen_btn_text"style="height:26px">
							@if($revisit != 0)
							<a href="{{ url('alltickets/revisit') }}">Más info <img
									src="{{ asset('images/image 25.png') }}"> </a>
									@endif
						</div>
					</div>
				</li>
				<li class="col-lg-3 col-md-6 col-sm-6 Abierto mb-4">
					<div class="main-bg"  style="height: 180px;">
						<div class="resumen_top_detail"style="height:154px">
							<div class="resumen_detail_left">
								<h4>{{ $close }}</h4>
								<span>Cerrado <br> Ticket </span>
							</div>
							<div class="resumen_detail_right">
								<img src="{{ asset('images/image 33.png') }}" alt="image 33">
							</div>
						</div>
						<div class="resumen_btn_text"style="height:26px">
							@if($close != 0)
							<a href="{{ url('alltickets/closed') }}">Más info <img
									src="{{ asset('images/image 25.png')}}"> </a>
									@endif
						</div>
					</div>
				</li>
				@endif
			</ul>
		</div>
	</div>
</div>
<br>
<br>
<div class="container-fluid">
	<div class="row m-0" style="    display: flex;
    justify-content: space-between;">
		<h4>Ticket reciente</h4>
		<a href="{{route('list-ticket')}}" class="backbtn" style="float: right">
			Ver todos los Tickets
		</a>
	</div>
	<div class="table-responsive mt-3">
		<table class="table-bordered" id=>
			<thead class="text-success">
				<th>
					S.No.
				</th>
				<th>
					Título del Ticket
				</th>
				<th>
					Ciudad
				</th>
				<th>
					Estado
				</th>
				<th>
					Fecha de creación
				</th>
			</thead>
			<tbody>
				@foreach($ticket as $key => $customer)
				<tr>
					<td>
						{{$key+1}}
					</td>
					<td>{{ $customer->title }}</td>
					<td>{{ $customer->city }}</td>

					{{-- <td>{{ $customer->user_id?$customer->User-> }}</td> --}}
					@if( $customer->status =='open')
					<td><a herf="javascript:void();" style="background: #FFB904;
            border-radius: 15px;    padding: 0.55rem 1rem;color:white;">Abierto</a></td>
					@endif
					@if( $customer->status =='closed')
					<td><a herf="javascript:void();" style="background: rgba(18, 136, 7, 0.2);
            border-radius: 15px;    padding: 0.55rem 1rem;color:white;">Cerrado</a></td>
					@endif
					@if( $customer->status =='waiting')
					<td><a herf="javascript:void();" style="background: rgba(233, 22, 22, 0.42);
           border-radius: 15px;    padding: 0.55rem 1rem;color:#E91616;">Esperando Técnico</a></td>
					@endif
					@if( $customer->status =='visit')
					<td><a herf="javascript:void();" style="background:rgba(11, 29, 77, 0.38);
          border-radius: 15px;    padding: 0.55rem 1rem;color:#0B1D4D;">Visita Requerida</a></td>
					@endif
					@if( $customer->status =='revisit')
					<td><a herf="javascript:void();" style="background: rgba(153, 153, 153, 0.36);
         border-radius: 15px;    padding: 0.55rem 1rem;color:white;">ReVisita Requerida</a></td>
					@endif
					@if( $customer->status =='reopen')
					<td><a herf="javascript:void();" style="background: #5a87c9;;
         border-radius: 15px;    padding: 0.55rem 1rem;color:white;">Reabrir</a></td>
					@endif
					<td>
						{{ $customer->created}}
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
		<div class="pagination-wrapper">
			{{ $ticket->links('pagination::bootstrap-4') }}
		</div>
	</div>

</div>
@endsection

@section('script')

@endsection