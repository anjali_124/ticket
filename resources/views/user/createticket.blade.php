@extends('layouts.app')

@section('title','Dashboard')

@section('styles')

@endsection

@section('content')

<!-- End Navbar -->
{{-- <div class="panel-header panel-header-sm">
    <!-- <canvas id="bigDashboardChart"></canvas> -->
</div> --}}

<div class="content-wrapper">
    <div class="row m-0">
        <div class="col-md-12 p-0">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">
                        Crear Ticket
                    </h4>
                </div>
                @if(Session()->has('message'))
                <div class="alert alert-{{explode('|',Session()->get('message'))[0]}} alert-dismissable">
                  <button type="button" aria-hidden="true" class="close">
                    <i class="now-ui-icons ui-1_simple-remove"></i>
                  </button>
                  <span>{{explode('|',Session()->get('message'))[1]}}</span>
                </div>
                @endif
                <form action="{{ route('store-ticket') }}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                    <input type="hidden" name="created_by" value="{{ Auth::user()->driving_licence }}">
                    <div class="card-body">
                        <div class="row m-0">
                            <div class="col-md-6 pl-0">
                                <div class="form-group">
                                    <label>Título del Ticket<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" placeholder="Título del Ticket" name="title" value="{{ old('title') }}">
                                    @if($errors->has('title'))
                                    <span class="text-danger">{{ $errors->first('title') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6 pr-1">
                                <div class="form-group">
                                    <label> Archivos adjuntos</label>
                                    <input type="file" class="form-control" placeholder="" name="image[]" multiple id="images">
                                    <div class="row m-0 p-1" id="preview_img">
                                    </div>
                                    @if($errors->has('count_of_plant'))
                                    <span class="text-danger">{{ $errors->first('count_of_plant') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-12 pl-0">
                                <div class="form-group">
                                    <label>Descripción<span class="text-danger">*</span></label>
                                    <textarea name="editor">{{ old('editor') }}</textarea>
                                    @if($errors->has('editor'))
                                    <span class="text-danger">{{ $errors->first('editor') }}</span>
                                    @endif 
                                </div>
                            </div>
                           
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="row m-0">
                            <div class="col-md-12 text-center">
                                <button class="btn subbtn" type="submit">Crear Ticket</button>
                            </div>
                        </div>
                    </div>
                </form>
                
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')

@endsection