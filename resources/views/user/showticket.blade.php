@extends('layouts.app')



@section('styles')

@endsection

@section('content')

<!-- End Navbar -->


<div class="content-wrapper">
    <div class="row m-0">
        <div class="col-md-12 p-0">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">
                        Asignar Técnico
                        {{-- <a href="{{route('technicians-create')}}" class="btn addbtn btn-sm" >
                        Agregar nuevos
                        </a> --}}
                    </h4>
                </div>
                <div class="card-body">
                    @if(Session()->has('message'))
                    <div class="alert alert-{{explode('|',Session()->get('message'))[0]}} alert-dismissable">
                        <button type="button" aria-hidden="true" class="close">
                            <i class="now-ui-icons ui-1_simple-remove"></i>
                        </button>
                        <span>{{explode('|',Session()->get('message'))[1]}}</span>
                    </div>
                    @endif
                    <div class="table-responsive">
                        <table class="table-bordered" id=>
                            <thead class="text-success">
                                <th>
                                    S.No.
                                </th>
                                <th>
                                    Ticket
                                </th>
                                <th>
                                    Ciudad
                                </th>
                                <th>
                                    Estado
                                </th>
                                <th>
                                    Acción
                                </th>
                            </thead>
                            <tbody>
                                @foreach($tickets as $key => $customer)
                                
                                <tr>
                                    <td>
                                        {{$key+1}}
                                    </td>
                                    <td style="font-family: 'Lato';
                                        font-style: normal;
                                        font-weight: 700;
                                        font-size: 17px;
                                        line-height: 20px;
                                        
                                        color: #0D0C0C;">{{ $customer->title }} <br>
                                        <p style="font-family: 'Lato';
                                            font-style: normal;
                                            font-weight: 700;
                                            font-size: 15px;
                                            line-height: 18px;
                                            
                                            color: #8D8C8C;"> #{{ $customer->ticket_number }}</p>
                                    </td>
                                    <td>
                                        {{$customer->city}}
                                    </td>
                                    @if( $customer->status =='open')
                                    <td><a herf="javascript:void();" style="background: #FFB904;
                                    border-radius: 15px;    padding: 0.55rem 1rem;color:white;">Abierto</a></td>
                                    @endif
                                    @if( $customer->status =='closed')
                                    <td><a herf="javascript:void();" style="background: rgba(18, 136, 7, 0.2);
                                        border-radius: 15px;    padding: 0.55rem 1rem;color:white;">Cerrado</a></td>
                                    @endif
                                    @if( $customer->status =='waiting')
                                    <td><a herf="javascript:void();" style="background: rgba(233, 22, 22, 0.42);
                                        border-radius: 15px;    padding: 0.55rem 1rem;color:#E91616;">Esperando Técnico</a></td>
                                    @endif
                                    @if( $customer->status =='visit')
                                    <td><a herf="javascript:void();" style="background:rgba(11, 29, 77, 0.38);
                                            border-radius: 15px;    padding: 0.55rem 1rem;color:#0B1D4D;">Visita Requerida</a></td>
                                    @endif
                                    @if( $customer->status =='revisit')
                                    <td><a herf="javascript:void();" style="background: rgba(153, 153, 153, 0.36);
                                        border-radius: 15px;    padding: 0.55rem 1rem;color:white;">ReVisita Requerida</a></td>
                                    @endif
                                    @if( $customer->status =='reopen')
                                    <td><a herf="javascript:void();" style="background: #5a87c9;;
                                        border-radius: 15px;    padding: 0.55rem 1rem;color:white;">Reabrir</a></td>
                                    @endif

                                    <td>
                                       
                                        <a  data-bs-toggle="modal" data-bs-target="#exampleModal" id="editForm" data-id="{{ $customer->id }}" data-userId='{{ $customer->user_id }}'>
                                            <img src="{{ asset('images/Group 3.png') }}">
                                        </a>
                                        <a href="{{ route('show-ticket',$customer->id) }}">
                                            <img src="{{ asset('assets/img/eyeicon.png') }}" width="25" height="25">
                                        </a>

                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="pagination-wrapper">
                            {{ $tickets->links('pagination::bootstrap-4') }}
                       </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"> Asignar Ticket</h5>
                <button type="button" class="mdi mdi-close" style="background: white;
                border: none;
                font-size: 1.5rem;" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body" style="padding: 5px 26px;">
             <form action="{{ route('assigntech') }}" class="mt-2" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="ticket_id" id="ticketId" value="">
                    {{ csrf_field() }}

                    <div class="align-items-center mt-3">

                        @if (Auth::user()->role == 'admin')
                        <input type="hidden" name="user_id" id="userId" value="">

                        <input type="hidden" name="status" value="waiting">
                        <div class="row">
                            <div class="col-md-12 pl-3">
                                <div class="form-group">
                                    <label>Asignar a la tecnica <span class="text-danger">*</span></label>
                                    <select class="form-control" name="assign_to">
                                        <option value="">Select technician</option>
                                        @foreach ($user as $cities)
                                        <option value="{{ $cities->id }}" {{ old('assign_to') == $cities->id ? 'selected' : '' }}>
                                            {{ $cities->name }}</option>

                                        @endforeach
                                    </select>
                                    
                                    @if ($errors->has('assign_to'))
                                    <span class="text-danger">{{ $errors->first('assign_to') }}</span>
                                    @endif
                                </div>
                                <div class="mt-lg-2 mt-1 mb-3"     style="text-align: center;">
                                    <button class="submit-btn button py-2 text-white border-0 px-5"
                                        style="font-size: 18px;font-weight: 500;">Responder Ticket</button>
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
</form>
            </div>
        </div>
    </div>
    @endsection

    @section('scripts')

    <script>
        $('body').on('click', '#editForm', function(event) {

            event.preventDefault();
            console.log($(this).data('id'));
            $('#ticketId').attr('value', $(this).data('id'));
            $('#userId').attr('value', $(this).data('userId'));

            $('#imgcity').text($(this).data('city'));
        });

    </script>
    @endsection
