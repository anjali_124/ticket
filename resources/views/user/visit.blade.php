<!DOCTYPE html>
<html lang="en">

<head>

    <title>@yield('title','Admin - Dashboard')</title>


    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Ticket Project</title>
    <link rel="stylesheet" href="{{asset('vendors/ti-icons/css/themify-icons.css')}}">
    <link rel="stylesheet" href="{{asset('vendors/mdi/css/materialdesignicons.min.css')}}">

    <link rel="stylesheet" href="{{asset('css/vertical-layout-light/style.css')}}">
    <link rel="shortcut icon" href="{{asset('images/favicon.png')}}" />

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lato&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/solid.min.css"
        integrity="sha512-6mc0R607di/biCutMUtU9K7NtNewiGQzrvWX4bWTeqmljZdJrwYvKJtnhgR+Ryvj+NRJ8+NnnCM/biGqMe/iRA=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/fontawesome.min.css"
        integrity="sha512-giQeaPns4lQTBMRpOOHsYnGw1tGVzbAIHUyHRgn7+6FmiEgGGjaG0T2LZJmAPMzRCl+Cug0ItQ2xDZpTmEc+CQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Inter&display=swap" rel="stylesheet">
    <script src="https://cdn.ckeditor.com/4.13.0/full/ckeditor.js"></script>


    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Full Calendar js</title>
    <meta name="csrf-token" content="{{ csrf_token() }}" />


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.min.js"></script>
    <style>
        .wrd-th-heading {
            padding: 0px !important;
            font-size: 25px;
            background-color: black;
            color: white !important;
            text-align: center;
        }

        .subbtn {
            border-radius: 8px !important;
            color: #FFFFFF !important;
            padding: 14px 18px;
            background: #0B1D4D;
            border: 1px solid #FFFFFF;
            font-size: 14px !important;
        }

        .filterrow {
            height: 50px;
            background: #F2F2F2;
            border-radius: 15px;
            display: flex;
            align-content: center;
            align-items: center;
            margin: 22px 0px;
            height: 56px;
        }

        .filterby {
            display: flex;
            align-items: center;
            justify-content: flex-end;
            font-family: 'Inter';
            font-style: normal;
            font-weight: 400;
            font-size: 15px;
            line-height: 18px;
            color: #555555;
        }

        .btn-success {
            background: #44AB2E;
            border-radius: 8px;
        }


        .backbtn {
            float: right;
            text-decoration-line: underline;
            color: #1D3874;
            font-size: 14px;
        }

        .addbtn {
            float: right;
            border-radius: 8px !important;
            color: #FFFFFF !important;
            padding: 14px 18px;
            background: #0B1D4D;
            border: 1px solid #FFFFFF;

            font-size: 14px !important;
        }

        .filterbtn {
            background: #555555;
            border-radius: 8px !important;
            padding: 7px 39px !important;
            font-family: 'Inter';
            font-style: normal;
            font-weight: 700;
            font-size: 13px !important;
            line-height: 16px;
            color: #FFFFFF !important;
        }
    </style>
</head>

<body>


    <div class="container-scroller">

        <div class="container-fluid page-body-wrapper">

            @include('layouts.sidebar')


            <div class="main-panel">
                <div style="height: 70px">
                    <nav class="navbar navbar-expand-lg navbar-absolute" style="box-shadow: none;
                            }">
                        <div class="container-fluid">

                            <div class="collapse navbar-collapse justify-content-end" id="navigation">
                                <ul class="navbar-nav">
                                    @if(Auth::user()->role == 'admin')

                                    <li class="nav-item ">
                                        <a class="nav-link " style="position: relative"> <img
                                                src="{{ asset('assets/img/Notification Icon.png') }}">
                                            <p style="    position: absolute;
                                            top: 2px;
                                            background: #4fb232;
                                            border-radius: 50%;
                                            padding: 3px;
                                            font-size: .75rem;
                                            font-weight: bold;
                                            color: #f4f5fa;
                                            left: 1.2rem;
                                            width: 1.5rem;
                                            text-align: center;
                                            margin: auto;
                                            height: 1.5rem;">{{ count(user()->notifications) }}
                                            </p>
                                        </a>

                                    </li>

                                    @endif
                                    <li class="nav-item dropdown">
                                        <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <img src="{{ asset('assets/img/User Icon.png') }}">

                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right"
                                            aria-labelledby="navbarDropdownMenuLink">

                                            <a class="dropdown-item"
                                                href="{{ route('editprofile', Auth::user()->id) }}">Editar Perfil</a>

                                            <a class="dropdown-item" href="{{ route('logout') }}">Cerrar sesión</a>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>
                <div class="content-wrapper">
                    <div class="row m-0">
                        <div class="col-md-12 p-0">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">
                                        Asignar Ticket

                                    </h4>
                                </div>
                                <div class="card-body">
                                    @if(Auth::user()->role == 'admin')
                                    <form action="{{ route('visitFilter') }}" method="GET" style="width:99%">
                                        <div class="row filterrow">
                                            <div class="col-2">
                                                <label class="mb-0">Filtrado por</label>
                                            </div>
                                            
                                            <div class="col-3  pr-2">
                                                <select class="form-control" name="id">
                                                    <option value="">User</option>
                                                    @foreach($users as $user)
                                                    <option value="{{ $user->id }}" {{ (request()->id==$user->id
                                                        )?'selected':'' }}>{{ $user->name }}
                                                    </option>
                                                    @endforeach
                                                </select>

                                            </div>
                                            

                                            <div class="col-1 p-0 mr-2">
                                                <button type="submit" class="btn filterbtn btn-sm m-0"
                                                    style="background-color:rgba(11, 29, 77, 0.83) !important;">Filtrar</button>
                                            </div>
                                        </div>
                                    </form>
                                    @endif
                                    <div class="wrapper">
                                        <div id="calendar">

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"
        integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous">
    </script>
    <script>
        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var booking = @json($events);
            console.log(booking);
            $('#calendar').fullCalendar({
                
                header: {
                    left: 'prev, next today',
                    center: 'title',
                    right: 'month, agendaWeek, agendaDay',
                },
                // $.each( booking, function( i, val ) {
                // $('.fc-event').attr('href', location.origin +'/show-ticket/'+ val.id);
                    
                //     })
                events: booking,
                eventClick: function(events) {
                    console.log(events);
                 $('.fc-event').attr('href', location.origin+'/public/ticket-show/'+events.id);
            }
            }),
           
            $("#bookingModal").on("hidden.bs.modal", function () {
                $('#saveBtn').unbind();
            });
            $('.fc-event').css('font-size', '13px');
            $('.fc-event').css('width', '95px');
            $('.fc-time-grid-event').css('height', '28px');
            $('.fc-time-grid-event').css('margin-top', '-20px');
        });
    </script>

</body>

</html>