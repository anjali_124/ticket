<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>@yield('title','Ticket Project')</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
        name='viewport' />
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <!-- CSS Files -->
    <link href="{{asset('assets/css/now-ui-dashboard.css?v=1.3.0')}}" rel="stylesheet" />
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Inter&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <!-- CSS Files -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Mulish&display=swap" rel="stylesheet">
    <link href="{{asset('assets/demo/demo.css')}}" rel="stylesheet" />
    <style>
        .addbtn {
            background: #0B1D4D;
            border: 1px solid #FFFFFF;
            border-radius: 11px;
            padding: 13px 85px;
            font-size: 14px !important;
        }

        .font {
            font-family: 'Inter';
            font-style: normal;
            font-weight: 400;
            font-size: 14px;
            line-height: 17px;
            color: #3E3E3E;

        }
.form-group{
    margin-bottom: 10px;
}
        .form {
            background: #FFFFFF !important;
            border: 1px solid #919191 !important;
            border-radius: 8px !important;
        }
    </style>
</head>

<body class="">
    <div class="wrapper" style="background-color: #F4F5FA;display: flex;align-content: center;
  justify-content: center;
  align-items: center;">
        <div style="width: 900px;
        height: 600px;
        background: #FFFFFF;
        box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
        border-radius: 15px;">
            <div class="row m-0">

                <div class="col-md-10 col-sm-10 p-0 m-auto">
                    <img src="{{ asset('images/MARCA TRANSPARENTE HORIZONTAL 2.png') }}" style="margin: auto; margin-top:15px;
          display: block;">
                    <div class="card-title text-center" style="font-family: 'Mulish';
      font-style: normal;
      font-weight: 700;
      font-size: 31px;
      line-height: 39px;
      /* identical to box height */
      
      text-align: center;
      letter-spacing: 0.3px;
      
      /* grayscale / black */
      
      color: #252733;">registro de usuario</div>
                    <p class="text-center" style="font-family: 'Mulish';
      font-style: normal;
      font-weight: 700;
      font-size: 16px;
      line-height: 7px;
      text-align: center;
      letter-spacing: 0.3px;
      color: #8D8C8C;">Registro para continuar</p>
                    <form method="POST" action="{{ route('user-store') }}" style="margin: auto;display: block;">
                        {{csrf_field()}}
                        @if(Session()->has('msg'))
                        <div class="alert alert-{{Session()->get('alert')}}">
                            {{Session()->get('msg')}}
                        </div>
                        @endif
                        {{-- <div> --}}
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="font" style="margin-left: 2rem;">Nombres Completos</label>
                                        <input type="text" name="name" class="form-control form col-md-10"
                                            style="margin: auto;display: block;" value="{{ old('name') }}" required>
                                        @if ($errors->has('name'))
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                               
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="font" style="margin-left: 2rem;">Dirección de Correo/email</label>
                                        <input type="text" name="email" class="form-control form col-md-10"
                                            style="margin: auto;display: block;" value="{{ old('email') }}" required>
                                        @if ($errors->has('email'))
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="font" style="margin-left: 2rem;">Telefono no.</label>
                                        <input type="text" name="phone_no" class="form-control form col-md-10"
                                            style="margin: auto;display: block;" value="{{ old('phone_no') }}" required>
                                        @if ($errors->has('phone_no'))
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('phone_no') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="font" style="margin-left: 2rem;">Nombre de la país </label>
                                        <input type="text" name="country" class="form-control form col-md-10"
                                            style="margin: auto;display: block;" value="{{ old('country') }}" required>
                                        @if ($errors->has('country'))
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('country') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="font" style="margin-left: 2rem;">Nombre de la estado </label>
                                        <input type="text" name="state" class="form-control form col-md-10"
                                            style="margin: auto;display: block;" value="{{ old('state') }}" required>
                                        @if ($errors->has('state'))
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('state') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="font" style="margin-left: 2rem;">Nombre de la parroquia </label>
                                        <input type="text" name="county" class="form-control form col-md-10"
                                            style="margin: auto;display: block;" value="{{ old('county') }}" required>
                                        @if ($errors->has('county'))
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('county') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6 ">
                                    <div class="form-group">
                                        <label class="font" style="margin-left: 2rem;">Ciudad ubicada<span class="text-danger">*</span></label>
                                        <select style="margin: auto;display: block;" class="form-control  form col-md-10" name="city_id" id="selectcity1">
                                            <option value="">Seleccione Ciudad</option>
                                            @foreach($city as $cities)
                                            <option value="{{$cities->id}}" {{ old('city_id')==$cities->id ? 'selected' : ''
                                                }} data-lat ="{{ $cities->latitude }}" data-lng="{{ $cities->longitude }}" data-address="{{ $cities->name }}">{{$cities->name}}</option>
                                            @endforeach
                                        </select>
                                        @if($errors->has('city_id'))
                                        <span class="text-danger">{{ $errors->first('city_id') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6 ">
                                    <div class="form-group">
                                        <label class="font" style="margin-left: 2rem;">Tipo de plantación<span class="text-danger">*</span></label>
                                        <select style="margin: auto;display: block;" class="form-control form col-md-10" name="type_of_plant_id" id="selectcity1">
                                            <option value="">Seleccione Tipo de plantación</option>
                                            @foreach($typeofplant as $cities)
                                            <option value="{{$cities->id}}" {{ old('type_of_plant_id')==$cities->id ? 'selected' : ''
                                                }} data-lat ="{{ $cities->latitude }}" data-lng="{{ $cities->longitude }}" data-address="{{ $cities->name }}">{{$cities->name}}</option>
                                            @endforeach
                                        </select>
                                        @if($errors->has('type_of_plant_id'))
                                        <span class="text-danger">{{ $errors->first('type_of_plant_id') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="font" style="margin-left: 2rem;">Número de hectareas de plantación</label>
                                        <input type="number" name="count_of_plant" class="form-control form col-md-10"
                                            style="margin: auto;display: block;" value="{{ old('count_of_plant') }}" required>
                                        @if ($errors->has('count_of_plant'))
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('count_of_plant') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="font" style="margin-left: 2rem;">Nro. de Cédula</label>
                                        <input type="text" name="driving_licence" class="form-control form col-md-10"
                                            style="margin: auto;display: block;" value="{{ old('driving_licence') }}" required>
                                        @if ($errors->has('driving_licence'))
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('driving_licence') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>


                            </div>
                            <br>
                            <button class="btn addbtn" style="margin: auto;display: block;">Registro</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
    </div>
    <!--   Core JS Files   -->
    <!--  Notifications Plugin    -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>

    <script>
        $(document).ready(function(){

      window.setTimeout(function() {
          $(".alert").fadeTo(1000, 0).slideUp(1000, function(){
              $(this).remove(); 
          });
      }, 3000);

    });
    </script>

</body>

</html>