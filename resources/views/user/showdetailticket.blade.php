@extends('layouts.app')



@section('styles')

@endsection

@section('content')

<!-- End Navbar -->
{{-- <div class="panel-header panel-header-sm">
    <!-- <canvas id="bigDashboardChart"></canvas> -->
</div> --}}

<div class="content-wrapper">
    <div class="row m-0">
        <div class="col-md-12 p-0">
            <div style="display: flex;    justify-content: flex-end;">
                <a href="{{ URL::previous() }}" class="backbtn" >
                    Atrás
                  </a>
                </div>
            <div class="card">
                <div class="main-detail">
                    <div class="row main-detail_top align-items-center">
                        <div class="col-lg-8 col-md-8 col-8">
                            <div class="row">
                                <div class="col-lg-2 col-md-2">
                                    <img src="{{ asset('images/ticket.png') }}" alt="img">
                                </div>
                                <div class="col-lg-10 col-md-10 main-detail-top-title p-0">
                                    <div class="row m-0">
                                        <div class="align-items-center">
                                            <h4 style="color: #fbd571;font-weight: 700;margin-right: 10px;">{{ $place->title }}</h4>
                                            <span class="" style="font-size: 14px;color: #8D8C8C;line-height: 16px;font-weight: 700;">#{{ $place->ticket_number }}</span>
                                        </div>
                                    </div>
                                    <span style="color: #615F5F;font-size: 16px;line-height: 19px;"><b>Creado en : </b> {{ $place->created }}</span><br>
                                    <span style="color: #615F5F;font-size: 16px;line-height: 19px;"><b>Creado Por : </b>{{ $place->created_by }}</span><br>
                                    <span style="color: #615F5F;font-size: 16px;line-height: 19px;"><b>usuario : </b>{{ $place->user_id?$place->User->name:'' }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-4 detail-btn">
                            @if($place->status == "open")
                            <button class="detail_btn button py-2 text-white border-0 px-lg-5 px-2" style="background: #128807;color: #fff !important;font-size: 18px;font-weight: 500;border-radius: 27px;" data-status="{{ $place->status }}">Abierto</button>
                            @endif
                            @if($place->status == "waiting")
                            <button class="detail_btn button py-2 text-white border-0 px-lg-5 px-2" style="background: rgba(233, 22, 22, 0.42);
                            border-radius: 27px; border:none;color: #E91616;font-size:11px;" data-status="{{ $place->status }}">Esperandoo Técnico</button>
                            @endif
                            @if($place->status == "visit")
                            <button class="detail_btn button py-2 text-white border-0 px-lg-5 px-2" style="background: rgba(11, 29, 77, 0.38);
                            border-radius: 27px; border:none;color: #0B1D4D;font-size:11px;" data-status="{{ $place->status }}">Visita Requerida</button>
                            @endif
                            @if($place->status == "revisit")
                            <button class="detail_btn button py-2 text-white border-0 px-lg-5 px-2" style="background: rgba(153, 153, 153, 0.36);
                            border-radius: 27px; border:none;color: #000000;font-size:11px;" data-status="{{ $place->status }}">Revisita Requerida</button>
                            @endif
                            @if($place->status == "closed")
                            <button class="detail_btn button py-2 text-white border-0 px-lg-5 px-2" style="background: #128807;color: #fff !important;font-size: 18px;font-weight: 500;border-radius: 27px;" data-status="{{ $place->status }}">Cerrada</button>
                            @endif
                            @if($place->status == "reopen")
                            <button class="detail_btn button py-2 text-white border-0 px-lg-5 px-2" style="background: #0b5ed7;color: #fff !important;font-size: 18px;font-weight: 500;border-radius: 27px;" data-status="{{ $place->status }}">Reabrir</button>
                            @endif

                        </div>
                    </div>
                    <div class="row main-detail_mid">
                        <div class="col-lg-12 col-md-12 col-12">
                            <span>{!! $place->description !!}</span>
                        </div>
                    </div>
                    <div class="main-detail-btm ">
                        <div class="btm-detail-wrp">
                            <ul class="row p-0">
                                @foreach($placeimgs as $placeimg)
                                <li class="col-lg-3 col-md-4 col-12 mt-lg-0 mt-3">
                                    <div class="">
                                        <img src="{{ asset($placeimg->image) }}" class="mr-2" width="242" height="200">
                                    </div>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                <hr>
                @if($place->status == "open" || $place->status == "waiting" || $place->status == "visit" ||$place->status == "reopen" || $place->status == "revisit" ||$place->status == "closed" )


                @if(count($placeconversation) != '')

                @foreach($placeconversation as $key => $value)
                @if($value->User->role == 'technician')
                <h3 class="mb-3 mt-2" style="font-weight: 700;">Respuesta del técnico</h3>
                <div class="main-detail" style="background: lightgrey; padding: 1rem; border-radius: 12px;">
                    @endif
                    @if($value->User->role == 'admin')
                    <h3 class="mb-3 mt-2" style="font-weight: 700;">Respuesta del administrador</h3>
                    <div class="main-detail" style="background: lightgrey; padding: 1rem; border-radius: 12px;">
                    @endif
                    @if($value->User->role == 'customer')
                    <h3 class="mb-3 mt-2" style="font-weight: 700;">Respuesta del usuario</h3>
                    <div class="main-detail">
                        @endif
                        <div class="row main-detail_top align-items-center">
                          
                            <div class="col-lg-10 col-md-10 main-detail-top-title">
                                <div style="display: flex; justify-content: space-between;align-items: center;" class="mt-4">
                                    <div style="color: #615F5F;font-size: 16px;line-height: 19px;"><b>Asignar : </b>{{ $value->assign_to?$value->AssignUser->name:'' }}</div>
                                    <div style="color: #615F5F;font-size: 16px;line-height: 19px; float:right"><b>Categoria : </b>@if(isset($value->Category->name)){{ $value->category?$value->Category->name:''}}@endif</div>
                                </div>
                                <div style="display: flex;justify-content: space-between;align-items: center;" class="mt-2">
                                    <div style="color: #615F5F;font-size: 16px;line-height: 19px;" class="mt-1"><b>Sub Categoria : </b>@if(isset($value->SubCategory->name)){{ $value->subcategory?$value->SubCategory->name:''}}@endif</div>
                                    <div style="color: #615F5F;font-size: 16px;line-height: 19px; float:right" class="mt-1"><b>Etiquetas : </b>

                                        @if(count($placetag) != 0)
                                        @foreach($placetag as $key1 => $value1)

                                        @if($key1 == $key)
                                        {{ $value1->name}}
                                        @endif
                                        @endforeach
                                        @endif
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="row main-detail_mid">
                            <div class="col-lg-12 col-md-12 col-12">
                                <span>{!! $value->description !!}</span>
                            </div>
                        </div>
                        <div class="main-detail-btm">
                            <div class="btm-detail-wrp">
                                <ul class="row p-0">
                                    @foreach($placetechimgs as $key2 => $placeimg)
                                    @if($placeimg->ticket_conversation_id == $value->id )
                                    <li class="col-lg-3 col-md-4 col-12 mt-lg-0 mt-3">
                                        <div class="">
                                            <img src="{{ asset($placeimg->image) }}" class="mr-2" width="242" height="200">
                                        </div>
                                    </li>
                                    @endif
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                    <hr>
                    
                    @endforeach

                    @endif

                    @endif
                    @if($place->status == "visit" || $place->status == "revisit")
                <div class="table-title">
                    <div class="row">
                        <div class="col">
                            <h3 style="font-size: 26px;font-weight: 700;margin-top: 20px;margin-bottom: 30px;">Customer Detail</h3>
                        </div>
                    </div>
                </div>
                <table class="table-fill">
                    <tbody class="table-hover">
                        <tr>
                            <td class="text-left">Nombre</td>
                            <td class="col-right">{{ $user->name }}</td>
                        </tr>
                        <tr>
                            <td class="text-left">Número de teléfono</td>
                            <td class="col-right">{{ $user->phone_no }}</td>
                        </tr>
                        
                        <tr>
                            <td class="text-left">Tipo de plantación</td>
                            <td class="col-right">{{ $user->type_of_plant_id?$user->TypeOfPlant->name:'' }}</td>
                        </tr>
                        <tr>
                            <td class="text-left">No. de Hectáreas</td>
                            <td class="col-right">{{ $user->count_of_plant }}</td>
                        </tr>
                        <tr>
                            <td class="text-left">ciudad</td>
                            <td class="col-right">{{ $user->city_id?$user->City->name:'' }}</td>
                        </tr>
                <hr>
                <tr>
                    <td class="text-left">Visit DateFecha de visita
                    </td>
                    <td class="col-right">{{ $place->visit_date }}</td>
                </tr>
                <tr>
                    <td class="text-left">Tiempo de visita                    </td>
                    <td class="col-right">{{ $place->visit_time }}</td>
                </tr>
                    </tbody>
                </table>
                
                @endif
                    @if(Auth::user()->role == 'customer')
                    <div class="reopen">
                        <form action="{{ route('reopen-ticket') }}" class="mt-2" method="post" enctype="multipart/form-data">
                            {{csrf_field()}}

                            <input type="hidden" name="assign_to" value="{{ $place->assign_to }}">
                            <input type="hidden" name="user_id" value="{{ $place->user_id }}">
                            <input type="hidden" name="ticket_id" value="{{ $place->id }}">
                            <input type="hidden" name="status" value="reopen">
                            <div class="row">
                                <div class="col-md-6 pl-3">
                                    <div class="form-group">
                                        <label>Añade tu Mensaje <span class="text-danger">*</span></label>
                                        <textarea class="form-control" name="description" style="height: auto!important">{{ old('description') }}</textarea>
                                        @if($errors->has('description'))
                                        <span class="text-danger">{{ $errors->first('description') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6 pl-3">
                                    <div class="form-group">
                                        <label>Images</label>
                                        <input type="file" class="form-control" placeholder="" name="image[]" multiple id="images">

                                        <div class="row m-0 p-1" id="preview_img">
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="mt-lg-5 mt-3">
                                <button class="submit-btn button py-2 text-white border-0 px-5" style="font-size: 18px;font-weight: 500;">Responder Ticket</button>
                            </div>
                        </form>
                    </div>
                    @endif
                </div>
                
            </div>
        </div>
    </div>

    @endsection

    @section('scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

    <script>
        $(document).ready(function() {
            jQuery(window).on("load", function() {
                if ($('.detail_btn').data("status") == 'closed') {
                    console.log('kkkk');
                    $('.reopen').show();
                } else {
                    console.log('bye');
                    $('.reopen').hide();
                }
            });
        });

    </script>

    @endsection
