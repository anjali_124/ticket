@extends('layouts.app')

@section('title','Dashboard')

@section('styles')

@endsection

@section('content')

<!-- End Navbar -->
{{-- <div class="panel-header panel-header-sm">
    <!-- <canvas id="bigDashboardChart"></canvas> -->
</div> --}}

<div class="content-wrapper">
    <div class="row m-0">
        <div class="col-md-12 p-0">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">
                        Editar perfil
                    </h4>
                </div>
                @if(Session()->has('message'))
                <div class="alert alert-{{explode('|',Session()->get('message'))[0]}} alert-dismissable">
                  <button type="button" aria-hidden="true" class="close">
                    <i class="now-ui-icons ui-1_simple-remove"></i>
                  </button>
                  <span>{{explode('|',Session()->get('message'))[1]}}</span>
                </div>
                @endif
                <form action="{{ route('profile-update') }}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <input type="hidden" name="id" value="{{ $user->id }}">

                    <div class="card-body">
                        <div class="row m-0">
                            <div class="col-md-6 pl-0">
                                <div class="form-group">
                                    <label>Nombres Completos<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" placeholder="Nombres Completos" name="name"
                                        value="{{ $user->name }}">
                                    @if($errors->has('name'))
                                    <span class="text-danger">{{ $errors->first('name') }}</span>
                                    @endif
                                </div>
                            </div>
                          
                            <div class="col-md-6 pr-1">
                                <div class="form-group">
                                    <label>Dirección de Correo/email<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" placeholder="Dirección de Correo/email" name="email"
                                        value="{{ $user->email }}">
                                    @if($errors->has('email'))
                                    <span class="text-danger">{{ $errors->first('email') }}</span>
                                    @endif 
                                </div>
                            </div>
                            <div class="col-md-6 pl-0">
                                <div class="form-group">
                                    <label>Telefono no.<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" placeholder="Telefono no."  name="phone_no"
                                        value="{{ $user->phone_no }}">
                                    @if($errors->has('phone_no'))
                                    <span class="text-danger">{{ $errors->first('phone_no') }}</span>
                                    @endif
                                </div>
                            </div>
                            @if(Auth::user()->role == 'customer')
                            <div class="col-md-6 pr-1">
                                <div class="form-group">
                                    <label>Ciudad ubicada<span class="text-danger">*</span></label>
                                    <select class="form-control" name="city_id" id="selectcity1">
                                        <option value="">Seleccione Ciudad</option>
                                        @foreach($city as $cities)
                                        <option value="{{$cities->id}}" {{ $user->city_id ==$cities->id ? 'selected' : ''
                                            }} data-lat ="{{ $cities->latitude }}" data-lng="{{ $cities->longitude }}" data-address="{{ $cities->name }}">{{$cities->name}}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('city_id'))
                                    <span class="text-danger">{{ $errors->first('city_id') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6 pl-0">
                                <div class="form-group">
                                    <label >Tipo de plantación<span class="text-danger">*</span></label>
                                    <select class="form-control " name="type_of_plant_id" id="selectcity1">
                                        <option value="">Seleccione Tipo de plantación</option>
                                        @foreach($typeofplant as $cities)
                                        <option value="{{$cities->id}}" {{ $user->type_of_plant_id ==$cities->id ? 'selected' : ''
                                            }} data-lat ="{{ $cities->latitude }}" data-lng="{{ $cities->longitude }}" data-address="{{ $cities->name }}">{{$cities->name}}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('type_of_plant_id'))
                                    <span class="text-danger">{{ $errors->first('type_of_plant_id') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6 pr-1">
                                <div class="form-group">
                                    <label>Número de hectareas de plantación<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" placeholder="Enter Nombre de la ciudad" name="count_of_plant" value="{{$user->count_of_plant }}">
                                    @if($errors->has('count_of_plant'))
                                    <span class="text-danger">{{ $errors->first('count_of_plant') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6 pl-0">
                                <div class="form-group">
                                    <label>Nro. de Cédula<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" placeholder="Nro. de Cédula"
                                        name="driving_licence" value={{ $user->driving_licence }}>
                                    @if($errors->has('driving_licence'))
                                    <span class="text-danger">{{ $errors->first('driving_licence') }}</span>
                                    @endif
                                </div>
                            </div>
                            @endif
                            @if(Auth::user()->role == 'technician' || Auth::user()->role == 'admin')
                            <div class="col-md-6 pr-1">
                                <div class="form-group">
                                    <label>Usuario<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" placeholder="Usuario"
                                        name="username" value="{{ $user->username }}">
                                    @if($errors->has('username'))
                                    <span class="text-danger">{{ $errors->first('username') }}</span>
                                    @endif
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="row m-0">
                            <div class="col-md-12 text-center">
                                <button class="btn subbtn" type="submit">Actualizar perfil</button>
                            </div>
                        </div>
                    </div>
                </form>
                <hr>
                @if(Auth::user()->role == "admin" || Auth::user()->role == "technician")
                <form action="{{ route('password-update') }}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <input type="hidden" name="id" value="{{ $user->id }}">

                    <div class="card-body">
                        <div class="row m-0">
                            <div class="col-md-6 pl-0">
                                <div class="form-group">
                                    <label>Contraseña<span class="text-danger">*</span></label>
                                    <input type="password" name="new_password" class="form-control map-input">
                                    @if($errors->has('new_password'))
                                    <span class="text-danger">{{ $errors->first('new_password') }}</span>
                                    @endif
                                </div>
                                <div id="address-map"></div>
                            </div>

                            <div class="col-md-6 pr-1">
                                <div class="form-group">
                                    <label>Confirmar contraseña<span class="text-danger">*</span></label>
                                    <input type="password" class="form-control" name="confirm_password">
                                    @if($errors->has('confirm_password'))
                                    <span class="text-danger">{{ $errors->first('confirm_password') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <button class="btn  subbtn" type="submit">Actualizar contraseña</button>
                            </div>
                        </div>
                    </div>

                </form>
                @endif
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')

@endsection