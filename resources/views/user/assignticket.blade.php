@extends('layouts.app')



@section('styles')

@endsection

@section('content')

<!-- End Navbar -->


<div class="content-wrapper">
  <div class="row m-0">
    <div class="col-md-12 p-0">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title">
            Asignar Ticket
            {{-- <a href="{{route('technicians-create')}}" class="btn addbtn btn-sm">
              Agregar nuevos
            </a> --}}
          </h4>
        </div>
        <div class="card-body">
          @if(Session()->has('message'))
          <div class="alert alert-{{explode('|',Session()->get('message'))[0]}} alert-dismissable">
            <button type="button" aria-hidden="true" class="close">
              <i class="now-ui-icons ui-1_simple-remove"></i>
            </button>
            <span>{{explode('|',Session()->get('message'))[1]}}</span>
          </div>
          @endif
          <div class="table-responsive">
            <table class="table-bordered" id=>
              <thead class="text-success">
                <th>
                  S.No.
                </th>
                <th>
                  Ticket
                </th>
                <th>
                  Técnico
                </th>
                <th>
                  Asignar fecha
                </th>
                <th>
                  Acción
                </th>
              </thead>
              <tbody>
                @foreach($tickets as $key => $customer)
                <tr>
                  <td>
                    {{$key+1}}
                  </td>
                  <td style="font-family: 'Lato';
                   font-style: normal;
                   font-weight: 700;
                   font-size: 17px;
                   line-height: 20px;
                   
                   color: #0D0C0C;">{{ $customer->title }} <br>
                    <p style="font-family: 'Lato';
                    font-style: normal;
                    font-weight: 700;
                    font-size: 15px;
                    line-height: 18px;
                    
                    color: #8D8C8C;"> #{{ $customer->ticket_number }}</p>
                  </td>

                  <td style="font-family: 'Lato';
                   font-style: normal;
                   font-weight: 700;
                   font-size: 17px;
                   line-height: 20px;
                   
                   color: #0D0C0C;">{{ $customer->assign_to?$customer->Assignuser->name:''}}</td>
                  <td>{{ date('d/m/Y', strtotime($customer->assign_date) )}}</td>

                  <td>

                    <a data-bs-toggle="modal" data-bs-target="#exampleModal1" id="editForm1"
                      data-id="{{ $customer->id }}" data-assignto='{{ $customer->assign_to }}'>
                      <img src="{{ asset('images/Group 3.png') }}">
                    </a>
                    <a  href="{{ route('edit-ticket',$customer->id) }}">
                      <img src="{{ asset('assets/img/image 49.png') }}">
                  </a>
                    <a href="{{ route('assign-show-ticket',$customer->id) }}">
                      <img src="{{ asset('assets/img/eyeicon.png') }}" width="25" height="25">
                    </a>
                    <a class="ml-1 mr-1" data-bs-toggle="modal" data-bs-target="#exampleModal" id="editForm"
                      data-id="{{ $customer->id }}" data-assign='{{ $customer->assign_to }}'>
                      <img src={{ asset('images/download.png') }} width="20" height="25">
                    </a>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
            <div class="pagination-wrapper">
              {{ $tickets->links('pagination::bootstrap-4') }}
         </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"> Cambiar Estado</h5>
        <button type="button" class="mdi mdi-close" style="background: white;
              border: none;
              font-size: 1.5rem;" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body" style="padding: 5px 26px;">
        <form action="{{ route('allticketsstatus') }}" class="mt-2" method="post" enctype="multipart/form-data">
          <input type="hidden" name="ticket_id" id="ticketId" value="">
          <input type="hidden" name="assign_to" id="userId" value="">
          {{ csrf_field() }}
          <div class="row">
            <div class="col-md-12 pl-3">
              <div class="form-group">
                <label>Cambiar Estado <span class="text-danger">*</span></label>
                <select class="form-control" name="status" id="visit">
                  <option value="">Estado</option>

                  <option value="visit" {{ old('status')=='visit' ? 'selected' : '' }} data-status="visit">Visita
                    Requerida
                  </option>
                  <option value="revisit" {{ old('status')=='revisit' ? 'selected' : '' }} data-status="revisit">
                    Revisita Requerida</option>
                  <option value="closed" {{ old('status')=='closed' ? 'selected' : '' }} data-status="closed">Cerrada
                  </option>
                </select>
                @if ($errors->has('status'))
                <span class="text-danger">{{ $errors->first('status') }}</span>
                @endif
              </div>
            </div>
            <div class="col-md-12 pl-3 visittime">
              <div class="form-group">
                <label>Fecha de visita <span class="text-danger">*</span></label>
                <input type="date" class="form-control" name="visit_date" value="{{ old('visit_date') }}">
                @if ($errors->has('visit_date'))
                <span class="text-danger">{{ $errors->first('visit_date') }}</span>
                @endif
              </div>
            </div>
            <div class="col-md-12 pl-3 visittime">
              <div class="form-group">
                <label>Tiempo de visita<span class="text-danger">*</span></label>
                <input type="time" class="form-control" name="time" value="{{  old('time') }}">
                @if ($errors->has('time'))
                <span class="text-danger">{{ $errors->first('time') }}</span>
                @endif
              </div>
            </div>
            <div class="mt-lg-3 mt-3 mb-3 m-auto">
              <button class="submit-btn button py-2 text-white border-0 px-5"
                style="font-size: 18px;font-weight: 500;">Responder Ticket</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
  <div class="modal fade" id="exampleModal1" tabindex="-1" aria-labelledby="exampleModalLabel1" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel"> Asignar Ticket</h5>
          <button type="button" class="mdi mdi-close" style="background: white;
              border: none;
              font-size: 1.5rem;" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body" style="padding: 5px 26px;">
          <form action="{{ route('assigntech') }}" class="mt-2" method="post" enctype="multipart/form-data">
            <input type="hidden" name="ticket_id" id="ticketId1" value="">
            {{ csrf_field() }}

            <div class="align-items-center mt-3">

              @if (Auth::user()->role == 'admin')
              <input type="hidden" name="user_id" id="userId1" value="">

              <input type="hidden" name="status" value="waiting">
              <div class="row">
                <div class="col-md-12 pl-3">
                  <div class="form-group">
                    <label>Asignar a la tecnica <span class="text-danger">*</span></label>
                    <select class="form-control" name="assign_to">
                      <option value="">Seleccionar Técnico</option>
                      @foreach ($user as $cities)
                      <option value="{{ $cities->id }}" {{ old('assign_to')==$cities->id ? 'selected' : '' }}>
                        {{ $cities->name }}</option>

                      @endforeach
                    </select>

                    @if ($errors->has('assign_to'))
                    <span class="text-danger">{{ $errors->first('assign_to') }}</span>
                    @endif
                  </div>
                  <div class="mt-lg-2 mt-1 mb-3" style="text-align: center;">
                    <button class="submit-btn button py-2 text-white border-0 px-5"
                      style="font-size: 18px;font-weight: 500;">Responder Ticket</button>
                  </div>
                </div>
              </div>
              @endif
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
    @endsection

    @section('scripts')

    <script>
      $(function() {
      $('#visit').change(function() {
          if ($(this).find(':selected').data("status") == 'visit' || $(this).find(':selected').data(
                  "status") == 'revisit') {
              $('.visittime').show();
          } else {
              $('.visittime').hide();
          }

      });
  });
  $(document).ready(function() {
      if ($(this).find(':selected').data("status") == 'visit' || $(this).find(':selected').data("status") ==
          'revisit') {
          $('.visittime').show();
      } else {

          $('.visittime').hide();
      }
  });
    </script>
    <script>
      $('body').on('click', '#editForm', function(event) {

      event.preventDefault();
      console.log($(this).data('id'));
      $('#ticketId').attr('value', $(this).data('id'));
      $('#userId').attr('value', $(this).data('assign'));
console.log($(this).data('assign'));
      $('#imgcity').text($(this).data('city'));
  });

    </script>
    <script>
      $('body').on('click', '#editForm1', function(event) {

      event.preventDefault();
      console.log($(this).data('id'));
      $('#ticketId1').attr('value', $(this).data('id'));
      $('#userId1').attr('value', $(this).data('assignto'));

      console.log($(this).data('assignto'));
      $('#imgcity').text($(this).data('city'));
  });

    </script>
    @endsection