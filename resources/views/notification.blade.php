@extends('layouts.app')



@section('styles')

@endsection

@section('content')

<!-- End Navbar -->


<div class="content-wrapper">
    <div class="row m-0">
        <div class="col-md-12 p-0">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">
                        Notificación
                        {{-- <a href="{{route('technicians-create')}}" class="btn addbtn btn-sm" >
                        Agregar nuevos
                        </a> --}}
                    </h4>
                </div>
                <div class="card-body">
                    @if(Session()->has('message'))
                    <div class="alert alert-{{explode('|',Session()->get('message'))[0]}} alert-dismissable">
                        <button type="button" aria-hidden="true" class="close">
                            <i class="now-ui-icons ui-1_simple-remove"></i>
                        </button>
                        <span>{{explode('|',Session()->get('message'))[1]}}</span>
                    </div>
                    @endif
                    <div class="container">
                        @foreach(user()->notifications as $notification)
                        @if($notification->data['status'] == 'open' && user()->role == 'admin')
                        <a href="{{ route('show-ticket',$notification->data['id']) }}">
                        <div class="row mt-3" style="box-shadow: 1px 1px 2px 1px rgb(0 0 0 / 20%);
                          border-radius: 15px;
                          padding: 0.5rem;">

                            <div class="col-md-1">
                                <img src="{{ asset('images/avatar.png') }}" style="    width: 4rem;
                                  height: 4rem;
                                  border-radius: 50%;">
                            </div>
                            <div class="col-md-10" style="margin: auto;margin-left: 0;padding:0">
                                <p class="m-0" style="font-size: 1rem;font-weight:600">Un nuevo ticket ha sido creado por el usuario : {{ $notification->data['username'] }}</p>
                            </div>
                        </div>
                        </a>
                        @endif
                        @if($notification->data['status'] == 'waiting' && user()->role == 'technician')
                        <a href="{{ route('show-ticket',$notification->data['id']) }}">
                        <div class="row mt-3" style="box-shadow: 1px 1px 2px 1px rgb(0 0 0 / 20%);
                            border-radius: 15px;
                            padding: 0.5rem;">

                            <div class="col-md-1">
                                <img src="{{ asset('images/avatar.png') }}" style="    width: 4rem;
                                height: 4rem;
                                border-radius: 50%;">
                            </div>
                            <div class="col-md-10" style="margin: auto;margin-left: 0;padding:0">
                                <p class="m-0" style="font-size: 1rem;font-weight:600">Hay un nuevo ticket asignado a ti por el admin, haz click para ver más información</p>
                                <p class="m-0" style="font-size: .7rem;">created by : {{ $notification->data['username'] }}
                            </div>
                        </div>
                        </a>
                        @endif
                        @if($notification->data['status'] == 'waiting' && user()->role == 'customer')
                        <a href="{{ route('show-ticket',$notification->data['id']) }}">
                        <div class="row mt-3" style="box-shadow: 1px 1px 2px 1px rgb(0 0 0 / 20%);
                            border-radius: 15px;
                            padding: 0.5rem;">

                            <div class="col-md-1">
                                <img src="{{ asset('images/avatar.png') }}" style="    width: 4rem;
                                height: 4rem;
                                border-radius: 50%;">
                            </div>
                            <div class="col-md-10" style="margin: auto;margin-left: 0;padding:0">
                                <p class="m-0" style="font-size: 1rem;font-weight:600">{{$notification->data['title']}} Ticket asignar a {{ $notification->data['assignto'] }} !</p>
                                <p class="m-0" style="font-size: .7rem;">created by : {{ $notification->data['username'] }}
                            </div>
                        </div>
                        </a>
                        @endif
                        @if(($notification->data['status'] == 'visit' || $notification->data['status'] == 'revisit') && user()->role == 'customer')
                        <a href="{{ route('show-ticket',$notification->data['id']) }}">
                        <div class="row mt-3" style="box-shadow: 1px 1px 2px 1px rgb(0 0 0 / 20%);
                            border-radius: 15px;
                            padding: 0.5rem;">

                            <div class="col-md-1">
                                <img src="{{ asset('images/avatar.png') }}" style="    width: 4rem;
                                height: 4rem;
                                border-radius: 50%;">
                            </div>
                            <div class="col-md-10" style="margin: auto;margin-left: 0;padding:0">
                                <p class="m-0" style="font-size: 1rem;font-weight:600">Técnico quiere visitar su lugar!</p>
                                <p class="m-0" style="font-size: .7rem;">created by : {{ $notification->data['username'] }}
                            </div>
                        </div>
                        </a>
                        @endif
                        @if($notification->data['status'] == 'reopen' && user()->role == 'technician')
                        <a href="{{ route('show-ticket',$notification->data['id']) }}">
                        <div class="row mt-3" style="box-shadow: 1px 1px 2px 1px rgb(0 0 0 / 20%);
                            border-radius: 15px;
                            padding: 0.5rem;">

                            <div class="col-md-1">
                                <img src="{{ asset('images/avatar.png') }}" style="    width: 4rem;
                                height: 4rem;
                                border-radius: 50%;">
                            </div>
                            <div class="col-md-10" style="margin: auto;margin-left: 0;padding:0">
                                <p class="m-0" style="font-size: 1rem;font-weight:600">El cliente respondió al ticket nro:  {{ $notification->data['title']  }}   !</p>
                                <p class="m-0" style="font-size: .7rem;">created by : {{ $notification->data['username'] }}
                            </div>
                        </div>
                        </a>
                        @endif
                        @if($notification->data['status'] == 'closed' && user()->role == 'customer')
                        <a href="{{ route('show-ticket',$notification->data['id']) }}">
                        <div class="row mt-3" style="box-shadow: 1px 1px 2px 1px rgb(0 0 0 / 20%);
                            border-radius: 15px;
                            padding: 0.5rem;">

                            <div class="col-md-1">
                                <img src="{{ asset('images/avatar.png') }}" style="    width: 4rem;
                                height: 4rem;
                                border-radius: 50%;">
                            </div>
                            <div class="col-md-10" style="margin: auto;margin-left: 0;padding:0">
                                <p class="m-0" style="font-size: 1rem;font-weight:600">Respuesta del {{ $notification->data['assignto'] }} en : {{ $notification->data['title']  }}   !</p>
                                <p class="m-0" style="font-size: .7rem;">created by : {{ $notification->data['username'] }}
                            </div>
                        </div>
                        </a>
                        @endif
                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')

<script type="text/javascript">
</script>

@endsection
