<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->id()->index();
            $table->string('title');
            $table->bigInteger('user_id')->index()->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->ondelete('cascade');
            $table->text('description');
            $table->string('ticket_number')->nullable();
            $table->string('created');
            $table->string('created_by');
            $table->enum('status',['open','waiting','visit','revisit','closed']);
            $table->bigInteger('assign_to')->index()->unsigned()->nullable();
            $table->foreign('assign_to')->references('id')->on('users')->ondelete('cascade');
            $table->string('assign_date')->nullable();
            $table->string('category_id')->nullable();
            $table->string('subcategory_id')->nullable();
            $table->string('ticket_status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
};
