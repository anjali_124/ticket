<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_conversations', function (Blueprint $table) {
            $table->id()->index();
            $table->bigInteger('user_id')->index()->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->ondelete('cascade');
            $table->bigInteger('assign_to')->index()->unsigned();
            $table->foreign('assign_to')->references('id')->on('users')->ondelete('cascade');
            $table->bigInteger('ticket_id')->index()->unsigned();
            $table->foreign('ticket_id')->references('id')->on('tickets')->ondelete('cascade');
            $table->string('description');
            $table->string('visit_date')->nullable();
            $table->string('revisit_date')->nullable();
            $table->string('time')->nullable();
            $table->string('category')->nullable();
            $table->string('subcategory')->nullable();
            $table->string('user_role')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket_conversations');
    }
};
