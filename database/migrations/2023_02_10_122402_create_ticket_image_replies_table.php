<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_image_replies', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->index()->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->ondelete('cascade');
            $table->bigInteger('assign_to')->index()->unsigned();
            $table->foreign('assign_to')->references('id')->on('users')->ondelete('cascade');
            $table->bigInteger('ticket_id')->index()->unsigned();
            $table->foreign('ticket_id')->references('id')->on('tickets')->ondelete('cascade');
            $table->bigInteger('ticket_conversation_id')->index()->unsigned();
            $table->foreign('ticket_conversation_id')->references('id')->on('ticket_conversations')->ondelete('cascade');
            $table->string('image');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket_image_replies');
    }
};
