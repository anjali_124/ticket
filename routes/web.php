<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//City
Route::get('/','App\Http\Controllers\MasterController@login')->name('login');
Route::post('/login-post','App\Http\Controllers\MasterController@loginpost')->name('login-post');
Route::get('/view','App\Http\Controllers\MasterController@view')->name('view');
Route::get('/admin/login','App\Http\Controllers\MasterController@adminlogin')->name('adminlogin');
Route::post('/admin/login-post','App\Http\Controllers\MasterController@adminloginpost')->name('adminlogin-post');
Route::get('/register','App\Http\Controllers\CustomerController@create')->name('user-create');
Route::post('/user/store','App\Http\Controllers\CustomerController@store')->name('user-store');


Route::middleware('auth-route')->group(function () {
    Route::get('/notification','App\Http\Controllers\MasterController@notification')->name('notification');
Route::get('/dashboard','App\Http\Controllers\MasterController@dashborad')->name('dashborad');
Route::get('/editprofile/{id}','App\Http\Controllers\CustomerController@edit')->name('editprofile');
Route::post('/user/update','App\Http\Controllers\CustomerController@update')->name('user-update');
Route::get('logout', 'App\Http\Controllers\MasterController@logout')->name('logout');
Route::post('/password/update','App\Http\Controllers\MasterController@updatepassword')->name('password-update');
Route::post('/profile/update','App\Http\Controllers\CustomerController@profileupdate')->name('profile-update');
Route::get('fullcalender','App\Http\Controllers\MasterController@index1')->name('visitcalender');
Route::get('/fullcalender/filter','App\Http\Controllers\MasterController@visitFilter')->name('visitFilter');
Route::get('/ticket-create', 'App\Http\Controllers\TicketContorller@create')->name('create-ticket');
Route::post('store/ticket','App\Http\Controllers\TicketContorller@store')->name('store-ticket');
Route::get('ticket-list','App\Http\Controllers\TicketContorller@index')->name('list-ticket');
Route::get('ticket-show/{id}','App\Http\Controllers\TicketContorller@show')->name('show-ticket');
Route::get('ticket-edit/{id}','App\Http\Controllers\TicketContorller@edit')->name('edit-ticket');
Route::post('update/ticket','App\Http\Controllers\TicketContorller@update')->name('update-ticket');
Route::get('assignticket','App\Http\Controllers\TicketContorller@assignindex')->name('assign-ticket');
Route::get('/assign-ticket-edit/{id}','App\Http\Controllers\TicketContorller@edit')->name('assign-edit-ticket');
Route::get('assign-ticket-show/{id}','App\Http\Controllers\TicketContorller@show')->name('assign-show-ticket');
Route::post('reopen/ticket','App\Http\Controllers\TicketContorller@reopenTicket')->name('reopen-ticket');
Route::get('alltickets','App\Http\Controllers\TicketContorller@alltickets')->name('alltickets');
Route::get('/filter/ticket','App\Http\Controllers\TicketContorller@filtertickets')->name('filter-tickets');
Route::get('alltickets/{status}','App\Http\Controllers\TicketContorller@allticketStatus')->name('allticketStatus');
Route::post('allticketsstatus','App\Http\Controllers\TicketContorller@statusChange')->name('allticketsstatus');
Route::post('assigntech','App\Http\Controllers\TicketContorller@updateTechnician')->name('assigntech');
Route::post('api/fetch-states', 'App\Http\Controllers\MasterController@fetchState');

Route::get('/ticket/filter','App\Http\Controllers\TicketContorller@filter')->name('filter-ticket');
Route::get('list/user','App\Http\Controllers\CustomerController@index')->name('list-user');
Route::get('edit/user/{id}','App\Http\Controllers\CustomerController@edituser')->name('edit-user');
Route::get('delete/user/{id}','App\Http\Controllers\CustomerController@destroy')->name('delete-user');
Route::get('/create/user','App\Http\Controllers\CustomerController@adminuser')->name('adminuser-create');


Route::get('list/technicians','App\Http\Controllers\TechniciansController@index')->name('list-technicians');
Route::get('edit/technicians/{id}','App\Http\Controllers\TechniciansController@edit')->name('edit-technicians');
Route::get('delete/technicians/{id}','App\Http\Controllers\TechniciansController@destroy')->name('delete-technicians');
Route::get('/create/technicians','App\Http\Controllers\TechniciansController@create')->name('technicians-create');
Route::post('/technicians/store','App\Http\Controllers\TechniciansController@store')->name('technicians-store');
Route::post('/technicians/update','App\Http\Controllers\TechniciansController@update')->name('technicians-update');


Route::get('/chart','App\Http\Controllers\MasterController@chart')->name('chart');
Route::get('/filter/chart','App\Http\Controllers\MasterController@chartFilter')->name('filter-chart');

 //City
 Route::get('/city','App\Http\Controllers\CityController@index')->name('city');
//  Route::get('/city/filter','App\Http\Controllers\CityController@filter')->name('filter-city');
 Route::get('/city/create','App\Http\Controllers\CityController@create')->name('city-create');
 Route::post('/city/store','App\Http\Controllers\CityController@store')->name('city-store');
 Route::get('/city/edit/{id}','App\Http\Controllers\CityController@edit')->name('city-edit');
 Route::post('/city/update','App\Http\Controllers\CityController@update')->name('city-update');
 Route::get('/city/delete/{id}', 'App\Http\Controllers\CityController@destroy')->name('city-delete');


  //City
  Route::get('/category','App\Http\Controllers\CategoryController@index')->name('category');
  //  Route::get('/city/filter','App\Http\Controllers\CityController@filter')->name('filter-city');
   Route::get('/category/create','App\Http\Controllers\CategoryController@create')->name('category-create');
   Route::post('/category/store','App\Http\Controllers\CategoryController@store')->name('category-store');
   Route::get('/category/edit/{id}','App\Http\Controllers\CategoryController@edit')->name('category-edit');
   Route::post('/category/update','App\Http\Controllers\CategoryController@update')->name('category-update');
   Route::get('/category/delete/{id}', 'App\Http\Controllers\CategoryController@destroy')->name('category-delete');

     //Subcategory
  Route::get('/subcategory','App\Http\Controllers\SubCategoryController@index')->name('subcategory');
  //  Route::get('/city/filter','App\Http\Controllers\CityController@filter')->name('filter-city');
   Route::get('/subcategory/create','App\Http\Controllers\SubCategoryController@create')->name('subcategory-create');
   Route::post('/subcategory/store','App\Http\Controllers\SubCategoryController@store')->name('subcategory-store');
   Route::get('/subcategory/edit/{id}','App\Http\Controllers\SubCategoryController@edit')->name('subcategory-edit');
   Route::post('/subcategory/update','App\Http\Controllers\SubCategoryController@update')->name('subcategory-update');
   Route::get('/csubategory/delete/{id}', 'App\Http\Controllers\SubCategoryController@destroy')->name('subcategory-delete');

    //TypeofPlant
 Route::get('/typeofplant','App\Http\Controllers\TypeOfPlantController@index')->name('typeofplant');
 //  Route::get('/city/filter','App\Http\Controllers\CityController@filter')->name('filter-city');
  Route::get('/typeofplant/create','App\Http\Controllers\TypeOfPlantController@create')->name('typeofplant-create');
  Route::post('/typeofplant/store','App\Http\Controllers\TypeOfPlantController@store')->name('typeofplant-store');
  Route::get('/typeofplant/edit/{id}','App\Http\Controllers\TypeOfPlantController@edit')->name('typeofplant-edit');
  Route::post('/typeofplant/update','App\Http\Controllers\TypeOfPlantController@update')->name('typeofplant-update');
  Route::get('/typeofplant/delete/{id}', 'App\Http\Controllers\TypeOfPlantController@destroy')->name('typeofplant-delete');
 
});