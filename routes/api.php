<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('login','App\Http\Controllers\API\MasterController@userLogin');          
Route::post('userRegister','App\Http\Controllers\API\MasterController@register');  
Route::post('loginTechnician','App\Http\Controllers\API\MasterController@technicianloginpost'); 
Route::get('city','App\Http\Controllers\API\MasterController@cityShow'); 
Route::get('typeofplant','App\Http\Controllers\API\MasterController@PlantShow'); 
Route::get('category','App\Http\Controllers\API\MasterController@categoryShow'); 
Route::get('subcategory','App\Http\Controllers\API\MasterController@subCategoryShow'); 

Route::group(['middleware'=>'auth:api'], function(){
    Route::post('ticket-list','App\Http\Controllers\API\TicketController@index'); 
    Route::post('ticket-create','App\Http\Controllers\API\TicketController@create'); 
    Route::post('updateTechnicianDetail','App\Http\Controllers\API\MasterController@updateTechnicianDetail');
    Route::post('getUserDetail','App\Http\Controllers\API\MasterController@getUserDetail'); 
    Route::post('getTechnicianDetail','App\Http\Controllers\API\MasterController@getTechnicianDetail'); 
    Route::post('updateUserDetail','App\Http\Controllers\API\MasterController@updateUserDetail');
    Route::post('countofdashorad','App\Http\Controllers\API\MasterController@countofdashorad');

});